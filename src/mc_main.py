#!/usr/bin/env python

from mc_system.mc_config import *
from config import *

from multi_obj_track import MultiTracker

from tracker_manager import TrackerManager

from detectors.cascade_human_detection import CascadeHumanDetection
from detectors.hog_human_detection import HOGHumanDetection
from detectors.mobile_net_ssd import MobileNetSSD
from detectors.yolo_v3 import YOLODetectionV3

import os
import getpass

# Multi Camera Distribution Libraries
from mc_system.mc_ros_pub import PublishROS
from mc_system.mc_ros_sub import SubscribeROS

#ROS Libraries
import rospy
from sensor_msgs.msg import Image

USERNAME = getpass.getuser()

def mc_system_init(cam=-1):
    '''
    This function plays an important role at startup. 
    1) It parses the arguments that were inserted with the keyboard. 
    2) It initialises the objects of the detectors that were chosen
    3) It initialises the objects of the multi_trackers that were chosen
    4) It initialises the particle filters list
    
    TODO Add more ways to utilize PARTICLE FILTERS. MORE COMBINATIONS WITH TRACKERS, etc.

    If a multi_tracker is set to 'None' from the input then the multi_tracker_obj is set to None 

    e.g. --haar None  --hog 'GOTURN  --yolo None
        Here the system will use 3 detectors and 1 tracker that corresponds to hog detector.
        The other objs are set to None and then they are appended to the track_list.

    @return track_list: a list of lists that contains:  * name of the detector
                                                        * detector_obj
                                                        * name of the tracker
                                                        * multi_tracker obj
    '''

    if (CAMERAS_NUM) != len(CAM_DT_PAIRS)  or  (cam) >= len(CAM_DT_PAIRS):
        ValueError('[ERROR]   Wrong Configuration. Check mc_config.py')

    if cam == -1:
        mc_pairs = CAM_DT_PAIRS
    else:
        mc_pairs = list()
        tmp_list = CAM_DT_PAIRS[cam]
        mc_pairs.append(tmp_list)

    track_list = list()
    for dn, tn in (mc_pairs):
        if dn == 'haar'  and  tn != None:
            haar_obj = CascadeHumanDetection(CASCADE_HAAR_CLASSIFIER_PATH, CASCADE_HAAR_FULL_BODY_FILE)
            multi_haar = None
            if tn != 'None': 
                multi_haar = MultiTracker()
                multi_haar.tracker_select(tn)
                tracker_name = tn
                print 'New Pair Selected:  ' + dn.upper()  + '  &  ' + str(tn)
            else:
                print 'New Detector Selected:  ' + dn.upper()
                tracker_name = None 
            haar_list = [dn, haar_obj, tracker_name, multi_haar]
            track_list.append(haar_list)

        elif dn == 'hog'  and  tn != None:
            hog_obj = HOGHumanDetection()
            multi_hog = None
            if tn != 'None':
                multi_hog = MultiTracker()
                multi_hog.tracker_select(tn)
                tracker_name = tn
                print 'New Pair Selected:  ' + dn.upper()  + '  &  ' + str(tn)
            else:
                print 'New Detector Selected:  ' + dn.upper() 
                tracker_name = None
            hog_list = [dn, hog_obj, tracker_name, multi_hog]
            track_list.append(hog_list)

        elif dn == 'ssd'  and  tn != None:
            ssd_obj = MobileNetSSD(SSD_PATH)
            multi_ssd = None
            if tn != 'None':
                multi_ssd = MultiTracker()
                multi_ssd.tracker_select(tn)
                tracker_name = tn
                print 'New Pair Selected:  ' + dn.upper()  + '  &  ' + str(tn)
            else:
                print 'New Detector Selected:  ' + dn.upper() 
                tracker_name = None
            ssd_list = [dn, ssd_obj, tracker_name, multi_ssd]
            track_list.append(ssd_list)

        elif dn == 'yolo'  and  tn != None:
            yolo_obj = YOLODetectionV3(YOLO_PATH, YOLO_WEIGHTS, YOLO_CFG)
            multi_yolo = None
            if tn != 'None':
                multi_yolo = MultiTracker()
                multi_yolo.tracker_select(tn)
                print 'New Pair Selected:  ' + dn.upper()  + '  &  ' + str(tn)
                tracker_name = tn
            else:
                print 'New Detector Selected:  ' + dn.upper()
                tracker_name = None 
            yolo_list = [dn, yolo_obj, tracker_name, multi_yolo]
            track_list.append(yolo_list)
        
    # To avoid compatiblity problems later, always need list inside a list    
    if len (track_list) == 0:
        ValueError('None Detector or Tracker selected. Exiting...')
    if len(track_list) < 2:
        track_list = list(track_list)
    

    detectors_params = detectors_parameters_insert(HAAR_PARAMS, HOG_PARAMS, SSD_PARAMS, YOLO_PARAMS)

    print track_list  
    return track_list, detectors_params



''' The Multi Camera Main function. The system will start by 
    executing this function first.
'''
def main_mc ():
    os.chdir('/home/' + USERNAME)
    print os.getcwd() 

    mc_track_list, detectors_params = mc_system_init()
    print mc_track_list
    print detectors_params

    #Create a single Camera track list. Seperate the list using a list for each Camera
    #and create a Tracker Manager Object
    sc_track_list = list()
    tracker_mngrs = list()
    for i in range(CAMERAS_NUM):
        tmp_list = list()
        tmp_list.append(mc_track_list[i])
        tracker_mngr = TrackerManager(tmp_list, detectors_params, PF_PARAMS, 25)  #SURE IT'S CORRECT
        tracker_mngr.max_dist_radius = 40 
        tracker_mngr.set_colors()
        tracker_mngr.track_mode_select('sc_pf') #TODO CHANGE / ADD / FIX / CREATE WHAT IS NEEDED FOR MULTI CAMERA SYSTEM

        tracker_mngrs.append(tracker_mngr)  #Each item in the list represents a tracker manager for a single camera.

    #NODE Names & Assignment of Node Names
    for i in range(CAMERAS_NUM):
        # pub_video = PublishROS(CAM_TOPIC_VIDEO_INPUT[i], CAM_PUBLISH_NODE, Image, 100)
        # sub_video = SubscribeROS(CAM_TOPIC_VIDEO_INPUT[i], CAM_SUBSCRIBE_NODE, Image, callback, 100)
        pass

if __name__ == "__main__":
    main_mc()


    rospy.spin()