#!/usr/bin/env python

import xml.etree.ElementTree as ET
import numpy as np 
import csv

# Create a Table with all the metrics of the evaluation
import plotly.plotly as py
import plotly.graph_objs as go 
import matplotlib.pyplot as plt 
import pandas as pd
import plotly
from utils import Utils

# Change the working directory accordingly
import os
import getpass

#In order to avoid any problems regarding the CWD, it is changed according to the needs
#of the program
USERNAME = getpass.getuser()
os.chdir ('/home/' + USERNAME)
XML_FILE = "Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/TwoLeaveShop2cor.xml"


'''
Use some ground truth pairs to determine the correctness of the algorithm. 
Important since the Project does not work

Hope to correct the mistakes

Pairs:   db = detector boxes 
         tb = tracker boxes
         dv = detector velocity
         tv = tracker velocity

db = [[100, 80, 50, 25], [60, 60, 30, 30]]
dv = [[0, 0], [1, -3]]

tb = [[90, 70, 60, 40], [70, 67, 28, 37]]
tv = [[0.5, -0.8], [6, 2]]

'''



    def correlate_bboxes_from_sources(self, db, tb, dv, tv, pair=0)
       
        utils = Utils()
        p = pair
        alg_name = self.track_list[p][0]  #Tragic but I will live with it in order to finish my thesis.

        
        if len(db) != len(dv):
            ValueError('[ERROR] correlate_bboxes - length of bboxes diff from length of velocities. Exiting...')
            
        if len(tb) != len(tv):
            ValueError('[ERROR] correlate_bboxes - length of bboxes diff from length of velocities. Exiting...')
        
        if type(tb) == np.ndarray:
            tb = tb.tolist()
        
        # Length of the detector boxes and the tracker boxes

        dists_mtx = np.array([])
        
   
        try:
            dl = len(db)
            db = utils.remove_Nones(db)
            db_unused = copy.deepcopy(db)
            
            tl = len(tb)
            tb = utils.remove_Nones(tb)
            tb_unused = copy.deepcopy(tb)
    
        except:
            dl = 0
            db = []
            dv = []

            tl = 0
            tb = []
            tv = []
        
        #The ordered lists that the function will return
        otb = [None for _ in range(len(db))]
        otv = [None for _ in range(len(dv))]
             
        if dl == 0 :
            return tb, tv
        
        elif dl != 0  and  tl != 0:          
            for di, dbbox in enumerate(db):
                if dbbox is None  or  len(dbbox) == 0:
                    continue
                dxc = dbbox[0] + dbbox[2]/2
                dyc = dbbox[1] + dbbox[3]/2
                
                dist_tmp = list()
                
                for ti, tbbox in enumerate(tb):
                    if tbbox is None  or  len(tbbox) == 0:
                        continue
                    txc = tbbox[0] + tbbox[2]/2
                    tyc = tbbox[1] + tbbox[3]/2

                    centers_dist = np.sqrt((dxc - txc)**2 + (dyc - tyc)**2)
                    dist_tmp.append(centers_dist)

                dist_arr = np.asarray(dist_tmp)

                if di == 0:
                    dists_mtx = dist_arr
                elif len(dists_mtx) == 0:
                    continue
                else:
                    dists_mtx = np.vstack((dists_mtx, dist_arr))    
                # dist_list.append(dist_tmp)

            # Find the minimum element of the array and remove(or make 1000) the column in which it was found
            if len(dists_mtx) != 0:
                if len(dists_mtx.shape) > 1:
                    rows, cols = dists_mtx.shape
                else:
                    rows = dists_mtx.shape
                    cols = 1
                    dists_mtx = np.array([dists_mtx])
            else:
                rows = cols = 0
            #deleted_cols = 0
            while cols > 0  and  np.min(dists_mtx) <= self.max_dist_radius:
                
                min_coords = np.where(dists_mtx == np.min(dists_mtx))
                        
                det_index = min_coords[0][0] 
                track_index = min_coords[1][0] #+ deleted_cols  #Add the number of the deleted cols to keep the indices correct only if the
                                            # concept is to delete the rows, cols. Otherwise, it is of no use
                
                row = det_index
                col = track_index

                dbbox = db[det_index]               
                tbbox = tb[track_index]
            
                #Create the lists with the unused previous and current bboxes
                #They are used for the improvement of the code
                # db_unused.remove(dbbox)
                # tb_unused.remove(tbbox)                
                
                otb[det_index] = tbbox
                otv[det_index] = tv[track_index]
                # ids[det_index] = ident

                # dists_mtx = np.delete(dists_mtx, col, axis=1)   # Delete Column
                dists_mtx[:, col] = 1000
                dists_mtx[row, :] = 1000    # Assign a large number to the elements of the row in order to not be selected
                # deleted_cols += 1


        return otb, otv












if __name__ == "__main__":
    

    # print x
    # a  = det_frame_wrapper_test('hog')
    # print a
    # tracker_names = [1,2,None, 3,None, 4,5,None, 6,7,8,None, 9, 10]
    # tracker_names = [tracker_names[i] for i in range(len(tracker_names))  \
    #                                   if tracker_names[i] != None]    
    # print tracker_names
    # a = [True, True, None, None, False, True, False]
    # b = np.asarray(a)
    
    # a = True
    # b = [1,1]
    # d = 4 
    # e = 5
    # if (b==[2,1])   or   (a is True  and  (e-d>=0)):
    #     print 'Yep, that WORKS TOO!'
    # s = a()
    # print s.d
####################################
#  Dataframe and csv with pandas   #
####################################

    # casc_eval = [[1,2,3], [2,3,4], [7,8,9]]
    # metrics_list = ['Empty_Frames', 'Accuracy', 'Precision']
    
    # csv_write('Music/e.csv', casc_eval, metrics_list)

    # # Create Dataframes using the pandas library to read the csv files
    # df_haar = pd.read_csv('Music/e.csv')
    # print(df_haar.values)
    # print(df_haar)


########################
#  Matplotlib figure   #
########################
    # haar_fig = plt.figure()
    # ax1 = haar_fig.add_subplot(111)
    # x_axis = np.linspace(0, 2, 3)
    # hog_dists = [[1,2,3], [2,3,4,5,6,7], [3,5,6,7,8]]
    # for i in range(len(hog_dists)):
    #     x_axis = [i] *len(hog_dists[i])
    #     ax1.scatter(x_axis, hog_dists[i], label=str(len(hog_dists[i])))
    # plt.title('Distrances of the centers of the bboxes for HOG')
    # plt.legend()
    # plt.xlabel('video')
    # plt.ylabel('dist_xc_yc')
    # plt.show()



#########################
#  csv write function   #
#########################

    # hog_eval = [[1,2,3,4,5,6], [1,2,3,4,5,6], [5,5,5,5,5,5], [1,2,3,4,5,6], [1,2,3,4,5,6], [5,5,5,5,5,5]]
    # a = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'dxcdyc_dist']
    # with open('Documents/Project_INSIGHT/data/evaluations/eval_1_hog.csv', 'w') as hog_csv:
    #     writer = csv.writer(hog_csv)
    #     writer.writerow(a)
    #     writer.writerows(hog_eval)

    # df_hog = pd.read_csv('Documents/Project_INSIGHT/data/evaluations/eval_1_hog.csv')
    
    # print(np.asarray(df_hog).T)
    # trace_haar = go.Table( header=dict( values=list(df_hog.columns),
    #                                     fill = dict(color='#C2D4FF'),
    #                                     align = ['center'] * 5),
    #                        cells=dict(values=np.asarray(df_hog).T,
    #                                   fill = dict(color='#F5F8FF'),
    #                                   align = ['center'] * 5))
    # data = [trace_haar]
    # # print(data)
    # py.plot(data, filename = 'pandas_table')

        # writer.writerows(hog_eval)    
    # gt_bboxes = [ [1, 2, 1, 2], [2,4,2,4]]
    # print(gt_bboxes)
    # for bbox in gt_bboxes:
    #     x = ((bbox[0]) - (bbox[2])/2.0)
    #     print(x)
    #     y = ((bbox[1]) - (bbox[3])/2.0)           
    #     x = int(round(x))
    #     print(x)
    #     y = int(y)
    #     bbox[0] = x
    #     bbox[1] = y
    
    # print(gt_bboxes)
    # xml_parse_bbox2(XML_FILE)

    # a = csv_read('Documents/Project_INSIGHT/data/evaluations/tests/', None)
    # for df in a:
    #     draw_scatter_dists(np.asarray(df),'gggg','video','gg')
    # parameters_list = [ [(4,4), (8,8), 1.0], [(4,4), (8,8), 1.03], [(4,4), (8,8), 1.05], [(4,4), (8,8), 1.1],           \
    #                 [(4,4), (8,8), 1.2], [(4,4), (8,8), 1.3], [(4,4), (8,8), 1.4], [(4,4), (8,8), 1.5],            \
    #                 [(8,8), (8,8), 1.0], [(8,8), (8,8), 1.03], [(8,8), (8,8), 1.05], [(8,8), (8,8), 1.1],          \
    #                 [(8,8), (8,8), 1.2], [(8,8), (8,8), 1.3], [(8,8), (8,8), 1.4], [(8,8), (8,8), 1.5],            \
    #                 [(8,8), (16,16), 1.0], [(8,8), (16,16), 1.03], [(8,8), (16,16), 1.05], [(8,8), (16,16), 1.1],  \
    #                 [(8,8), (16,16), 1.2], [(8,8), (16,16), 1.3], [(8,8), (16,16), 1.4], [(8,8), (16,16), 1.5] ]  

    # print(parameters_list)
    # CSV_DATA_PATH = 'Documents/Project_INSIGHT/data/evaluations/eval1/acc_015/'
    # metrics_list = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure']
    # utils = Utils()
    # csv_eval_list, csv_name_list = utils.csv_read(CSV_DATA_PATH, 'normal_csv', 0)

    # csv_haar_indices = utils.str_search(csv_name_list, 'haar_param')
    # csv_hog_indices = utils.str_search(csv_name_list, 'hog_param')
    # csv_haar_dists_indices = utils.str_search(csv_name_list, 'haar_dists')
    # csv_hog_dists_indices = utils.str_search(csv_name_list, 'hog_dists')
    # plotly.tools.set_credentials_file(username='protesla', api_key='7XqiRT8jffPzHn8xWq2v')

    # for data in csv_eval_list:
    #     if csv_haar_indices.count(csv_eval_list.index(data)) > 0: 
    #         for e in data[1:]:
    #             for f in e:
    #                 data[data.index(e)][e.index(f)] = np.round(float(data[data.index(e)][e.index(f)]), 3)

    #         # temp_data = np.asarray(data[1:])
    #         # temp_data = temp_data.astype(np.float)
    #         # temp_data = np.round(temp_data)
    #         # data[1:] = temp_data
    #         # data[1:] = np.str(data[1:])
    #         # print(data[1:])
    #     elif csv_hog_indices.count(csv_eval_list.index(data)) > 0:
    #         pass
    # for data in csv_eval_list:
    #     i = csv_eval_list.index(data)
    #     if csv_haar_indices.count(csv_eval_list.index(data)) > 0: 
    #         for arr in data[1:]:
    #             for f in arr:
    #                 data[data.index(arr)][arr.index(f)] = np.round(float(data[data.index(arr)][arr.index(f)]), 3)
    #         vanilla_haar = 'vanilla_haar' + '_' + csv_name_list[i][-5]
    #         haar_data = utils.draw_table_plotly(vanilla_haar , data, metrics_list, 'left', '#C2D4FF', '#F5F8FF', 'normal_csv')
    #     elif csv_hog_indices.count(csv_eval_list.index(data)) > 0:
    #         for arr in data[1:]:
    #             for f in arr:
    #                 data[data.index(arr)][arr.index(f)] = np.round(float(data[data.index(arr)][arr.index(f)]), 3)
    #         vanilla_hog = 'vanilla_hog' + '_' + csv_name_list[i][-5]
    #         hog_data = utils.draw_table_plotly(vanilla_hog , data, metrics_list, 'left', '#C2D4FF', '#F5F8FF', 'normal_csv') 
           
        # elif csv_haar_dists_indices.count(csv_eval_list.index(data)) > 0:
        #     utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'haar_' + csv_name_list[i][-5]          \
        #                        , 'video', 'dist_xc_yc', CSV_DATA_PATH, True)  
        # elif csv_hog_dists_indices.count(csv_eval_list.index(data)) > 0:
        #     utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_ ' + 'hog_' +  csv_name_list[i][-5]  \
        #                        , 'video', 'dist_xc_yc', CSV_DATA_PATH, True)  
        # else:
        #     pass
            # ValueError('The index is not inside the folder')
    # for df_dists in csv_dists_list:
    #     if csv_haar_indices.count(csv_metrics_list.index(df)) > 0: 
    #         temp = 'haar '
    #     else:
    #         temp = 'hog '
    #     draw_scatter_dists(np.asarray(df_dists),'Distances of the centers of the bboxes ' + temp + \
    #                        str(len(csv_dists_list.index(df_dists))), 'video', 'dist_xc_yc')

