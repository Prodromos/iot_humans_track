#!/usr/bin/env python

import cv2
import numpy as np 
import sys
import copy 

from multi_obj_track import MultiTracker
from datasets_eval import CaviarDataset, DatasetEvaluation

from utils import Utils
from fast_nms import non_max_suppression_fast


from config import CAVIAR_VIDEOS_PATH, CAVIAR_VIDEOS_ENCODING, XML_PATH, \
                   CSV_DATA_PATH, CSV_DATA_HAAR, CSV_DATA_HOG, CSV_DATA_SSD, CSV_DATA_YOLO, CSV_END,\
                   CHECK_FLAG_THRESHOLD

# Particle Filters 
from particle_filters.pf_deepgaze import DeepgazePF
from particle_filters.pf_huang import HuangPF
from particle_filters.particle_filter import ParticleFilter
# from detectors.cascade_human_detection import CascadeHumanDetection
# from detectors.hog_human_detection import HOGHumanDetection
# from detectors.mobile_net_ssd import MobileNetSSD
# from detectors.yolo_v3 import YOLODetectionV3

class TrackerManager():
    '''
    One of the most important classes of the application. The
    management of the detectors and trackers that are going to
    to be used in this app is achieved here. Considering the two
    lists for detectors and trackers some ROS topics will be created
    accordingly.  TODO
    '''
    def __init__(self, track_list, det_params, pf_params, track_max_frames=25, display=False):
        '''

        @param track_list: a list of lists. This list contains: the name of the detector,
                           the detector_obj, the name of the tracker and the multi_tracker_obj 

        @param det_params: a list of dicts with the parameters of the detectors the application is going to use

        @param pf_params: a list of dicts. It contains a dict with the initialisation parameters of every particle
                        filter that is going to be used. Each dict is of the form: {'name': [params]}. For more
                        information about the origin of the parameters for each filter see the .py files in 
                        the folder particle filters

        @param pf_list: a list like the track_list, just for the particle filters. Each sublist comprises of 
        TODO            the detector_name, det_obj, pf_name, PF_OBJS

        @param track_max_frames: an int that indicates the number of max frames to track before a reinitialisation
                                 of the tracker is done

        @var project_mode: depending on this variable, the system will be adjusted accordingly (e.g. for var='video'
                            it will be used for single camera tracking)
        @var track_mode: depending on this variable the tracking at any camera will be adjusted. 
                        (e.g. if var='particle_filters' the particle filters mode will be used)
        @var frame_counter: potentially it can be a list of N integers, where N stands for the number of cameras we use
        
        @var trackers_frame_init: a list of length equal to the length of the track_list. It contains the name of
                                    the tracker and the number of the frame at 
                                    which the trackers were initialised. Thus, when a certain number of frames is passed,
                                    the tracker can be reinitialised. Default value -1 for every tracker. 
        
        @var trackers_bboxes_num: a list of length equal to the length of the track_list, It contains the names of the 
                                    detectors that have been paired with the tracker and the number of the bboxes that 
                                    each of the trackers are meant to track. These bboxes are assigned to the trackers 
                                    by the detectors.
        '''
        self.project_mode = ''
        self.track_mode = ''
        self.track_list = track_list
        
        self.prev_state = []#[{dn: [], tn: []} for dn,_,tn,_ in self.track_list]    # Previous state
        self.prev_vel = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        # self.pprev_state = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]   # Pre previous state
        # self.pprev_vel = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        
        self.check_current_state = []
        self.check_current_state_flags = [] 

        self.max_dist_radius = 40   # Maximum distance that a human can move at consecutive frames

        # TODO TODO TODO Use these variables for the Multi Camera Track Frame
        self.track_max_frames = track_max_frames
        self.display = display

        self.det_params = det_params


        self.frame_counter = -1     # Increments in track_frame(). Must be initialised before every video 
                                    # in a video list (e.g. CAVIAR).
        if len(self.track_list) != 0:
            self.trackers_frame_init = [[self.track_list[i][0], -1] for i in range(len(self.track_list))]
            self.trackers_bboxes_num = [[self.track_list[i][0] ,-1] for i in range(len(self.track_list))]

            self.tracker_names = zip(*self.track_list)[2]       #Create a tuple with the names of the trackers
            self.tracker_names = [self.tracker_names[i] for i in range(len(self.tracker_names))  \
                                                    if self.tracker_names[i] != None]    
    
        self.pf_params = pf_params
        
        self.ids = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        self.prev_ids = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        
        self.prev_reset_param = 0
        
        self.avail_ids = [x for x in range(0, 20)]
        self.avail_ids.sort(reverse=True)

        self.pf_objs = []

        # TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO DELETE THIS AND FIX THE PROBLEM
        self.pf_list = []
        if len(self.pf_list) != 0:
            self.pf_frame_init = [[self.pf_list[i][0], -1] for i in range(len(self.pf_list))]
            self.pf_bboxes_num = [[self.pf_list[i][0], -1] for i in range(len(self.pf_list))]
            self.pf_names = zip(*self.pf_list)[2]       #Create a tuple with the names of the trackers
            self.pf_names = [self.pf_names[i] for i in range(len(self.pf_names))  \
                                                    if self.pf_names[i] != None]    

        self.colors = []
        self.pf_colors = []

    def _reset_prev_state(self):
        self.prev_state = []    # Previous state
    def _reset_prev_vel(self):
        self.prev_vel = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]    # Previous state
    def _reset_pprev_state(self):
        self.pprev_state = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]    # Pre-previous state
    def _reset_pprev_vel(self):
        self.pprev_vel = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]    # Pre-previous state
    def _reset_avail_ids(self):
        self.avail_ids = [x for x in range(0, 20)]
        self.avail_ids.sort(reverse=True)
    

    def _save_prev_state(self, cur_state):
        '''
        This function stores the bboxes that were detected and tracked
        by detectors and trackers correspondingly. It is called inside 
        the track_frame_sc().

        @param cur_state: a list that contains the current_state for the pairs
            detector-tracker. Each pair comprises of a dict of the form:
                        {'haar': bboxes, 'CSRT': bboxes}. 
        @return None
        '''
        # if self.prev_reset_param == 15:  # TODO Replace the magic number 
        #     self.prev_state = [{dn: [], tn: [], 'id': []} for dn,_,tn,_ in self.track_list]
        #     self.prev_reset_param = 0
        # else:
        self.prev_state = copy.deepcopy(cur_state)
            # self.prev_reset_param += 1
        return None

    def _save_prev_ids(self):
        self.prev_ids = copy.deepcopy(self.ids)

    def _save_prev_vel(self, cur_vel):
        '''
        This function stores the bboxes that were detected and tracked
        by detectors and trackers correspondingly. It is called inside 
        the track_frame_sc().

        @param cur_vel: a list that contains the current_state for the pairs
            detector-tracker. Each pair comprises of a dict of the form:
                        {'haar': vels, 'CSRT': vels}. 
        @return None
        '''
        self.prev_vel = cur_vel
        return None


    def set_colors(self):
    
        num_of_dets = len(self.track_list)
        num_of_dets += len(self.pf_list)
        
        num_of_trackers = 0
        num_of_pfs = len(self.pf_list)
        
        det_names = []
        tracker_names = []
        pf_names = []
        
        for det,_,tracker,_ in self.track_list:
            if tracker is not None:
                num_of_trackers += 1
                tracker_names.append(tracker) 
            det_names.append(det)

        for det,_,pf,_ in self.pf_list:
            pf_names.append(pf) 
            det_names.append(det)


        total_colors = num_of_dets + num_of_trackers + num_of_pfs

        colors =  np.random.uniform(0, 255, size=(total_colors, 3))
        for i, col in enumerate(colors):
            colors[i] = [np.int16(j) for j in col]
        print colors        

        color_dict = {}
        for i in range(num_of_dets):
            temp_dict = {det_names[i]: colors[i]}
            color_dict.update(temp_dict)
        
        for i in range(num_of_trackers):
            index = num_of_dets + i #+num_of_pfs
            temp_dict = {tracker_names[i]: colors[index]}
            color_dict.update(temp_dict)
        
        for i in range(num_of_pfs):
            index = num_of_dets + num_of_trackers + i
            temp_dict = {pf_names[i]: colors[index]}
            color_dict.update(temp_dict)

        self.colors = color_dict
        return color_dict


    def pf_color_set(self):
        total_colors = 30
        colors =  np.random.uniform(0, 255, size=(total_colors, 3))
        for i, col in enumerate(colors):
            colors[i] = [np.int16(j) for j in col]
        return colors


    def mode_select(self, mode='video'):
        '''
        Select the mode for the desired operation. 
        If a dataset evaluation is done or a video are is processed select 'video'. It's the default.
        If a single camera is used as input video select 'camera'.
        If the multi-camera tracking is desired select 'RPis'

        @param mode: if mode == 'video' the tracking will be done in a video. It's default
                     if mode == 'camera' the tracking will be done with the webcam
                     if mode == 'RPis' the tracking will be done with multi-cameras
        '''
        if mode == 'video':
            self.project_mode = 'video'
        elif mode == 'camera':
            self.project_mode = 'camera'
        elif mode == 'RPis':    
            self.project_mode = 'RPis'
        else:
            ValueError('The mode does not correspond with the existing ones. Exiting...')

        return None

    def track_mode_select(self, mode = 'dummy'):
        if mode == 'dummy':
            self.track_mode = 'dummy'
        elif mode == 'opencv_trackers':
            self.track_mode = 'opencv_trackers'
        elif mode == 'particle_filters':
            self.track_mode = 'particle_filters'
        elif mode == 'sc_pf':
            self.track_mode = 'sc_pf'
        elif mode == 'mc_pf':
            self.track_mode = 'mc_pf'
        
        else:
            ValueError('Incorrect tracking mode selected. Exiting...')
        return None

    # def track_caviar(self, frame_size, iou_threshold, skip_frames_num=0, max_frames_reached=False):
    def track_caviar(self, frame_size, iou_threshold, videos_num=0, skip_frames_num=0, \
                     max_frames_reached=False, display=False, output=True):
        '''
        Implementation of an automated process for the examination of the performance
        of our application using the CAVIAR DATASET. In every frame there is a series 
        of detection and tracking mechanisms that takes place. Also, the results are save 
        in csv files, which are comprised of files regarding the xc_yc distances, the 
        metrics we need to assess and the reset log file of every video 

        @param frame_size: the (width, height) of the frame
        @param iou_threshold: the accuracy error that is considered acceptable
        @param skip_frames_num: a uint. Determines the number of frames to skip to improve speed of application.
                                e.g. If skip_frames_num=3 only 1 frame per 4 will be examined
        @param max_frames_reached: bool. If True, an additional chech will be done inside tracker_reset()
        
        @return None
        '''
        
        caviar_dataset = CaviarDataset()

        videos, gt_list = caviar_dataset.eval_files_list(CAVIAR_VIDEOS_PATH, CAVIAR_VIDEOS_ENCODING, XML_PATH)    
        
        utils = Utils()
        metrics = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'Mean_dist', 'TP', 'FP', 'FN']
        for det_name,_,tracker_name,_ in self.track_list:
            if det_name is not None:
                utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'w', [], metrics)
            if tracker_name is not None:
                utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'w', [], metrics)

        for _,_,pf_name,_ in self.pf_list:
            if pf_name is not None:
                utils.csv_write(CSV_DATA_PATH + pf_name + '_metrics' + CSV_END, 'w', [], metrics)

        # utils.csv_write(CSV_DATA_PATH + 'reset_log' + '' + CSV_END, 'a', , tracker_names)    

        # det_total_metrics = list()
        # det_total_dists = list()
        # tracker_total_metrics = list()
        # tracker_total_dists = list()
        
        # self.frame_counter = -1
        for i in range(len(videos)):
            if videos_num != 0:
                if videos_num == i:
                    break

            gt_bboxes = caviar_dataset.xml_parser_gt_bboxes(gt_list[i])
            gt_bboxes = caviar_dataset.coords_convert(gt_bboxes)        
            
            if self.track_mode == 'dummy': 
                # self.dummy_video(videos[i], frame_size, iou_threshold, gt_bboxes=gt_bboxes,         \
                #                 skip_frames_num=skip_frames_num, max_frames_reached=max_frames_reached, display=True)
                self.track_video_dummy(videos[i], frame_size, iou_threshold, gt_bboxes=gt_bboxes,         \
                                skip_frames_num=skip_frames_num, max_frames_reached=max_frames_reached, display=display)

            elif self.track_mode == 'opencv_trackers':
                self.track_video_opencv(videos[i], frame_size, iou_threshold, gt_bboxes=gt_bboxes,         \
                                skip_frames_num=skip_frames_num, max_frames_reached=max_frames_reached, display=display)

            elif self.track_mode == 'particle_filters':
                self.track_video_pf(videos[i], frame_size, iou_threshold, gt_bboxes=gt_bboxes,         \
                                skip_frames_num=skip_frames_num, max_frames_reached=max_frames_reached, display=display)
            
            elif self.track_mode == 'sc_pf':
                self.pf_colors = self.pf_color_set()
                self.track_video_sc_pf(videos[i], frame_size, iou_threshold, gt_bboxes=gt_bboxes,         \
                                skip_frames_num=skip_frames_num, max_frames_reached=max_frames_reached, display=display, output=output)
            
            else:
                pass

        return None


    def trackers_reset(self, frame, detectors_bboxes, trackers_bboxes, max_frames_reached=False):
        '''
        This function is used for reseting the trackers that have been problematic.
        A check of the number of the bboxes is done in order to find out if the bboxes
        that are being tracked need are the same number as the initial. Furthermore, it
        is considered a good practice to update the trackers every a num of frames to ensure
        better tracking results.

        #TODO add further functionality using more parameters, e.g. comparison with the 
        detector_bboxes, etc.

        @param frame: the current frame
       
        @detectors_bboxes:  a list that contains the bboxes of all the detectors that are being 
                            used. Technically, it is a list of dicts.
       
        @trackers_bboxes:   a list that contains the bboxes of all the trackers that are being 
                            used. Technically, it is a list of dicts. However, for every detector
                            obj without a tracker obj, a None is appended to the list
       
        @param max_frames_reached:  a bool variable. If it is set True, the function will make an
                                    additional check, in order to determine if the max_frames using
                                    a tracker are reached and we need to reset it.

        @return reset: a list with bool variables and the frame number. It's initialised as a list of False elements.
                       If a tracker resets, the element at that index is the list is set True.
        '''

        reset = [False for i in range(len(self.track_list))]
        num_dets = len(self.track_list)
        num_trs = len(self.tracker_names)
        
        if self.trackers_bboxes_num == [[self.track_list[i][2] ,-1] for i in range(len(self.track_list))]:
            return reset
        
        tr_index=0
        for i in range(num_dets):
            det_name = self.track_list[i][0]
            tracker_name = self.track_list[i][2]

            if tr_index >= num_trs:
                continue
            if len(trackers_bboxes) == 0:
                continue   
            if self.track_list[i][2] == None  or  self.track_list[i][3] == None:
                continue

            # if trackers_bboxes[tr_index] is None:
            # print self.trackers_bboxes_num
            # print (trackers_bboxes[i][tracker_name])

            elif ((self.trackers_bboxes_num[i][1] != len(trackers_bboxes[tr_index][tracker_name]))   or      \
                 (max_frames_reached is True   and   (self.frame_counter - self.trackers_frame_init[i][1] >= self.track_max_frames))):

                tr_index += 1           
                multi_tracker = MultiTracker()
                multi_tracker.tracker_select(self.track_list[i][2])     #Select the name of the tracker
                self.track_list[i][3] = multi_tracker
                self.track_list[i][3].tracker_add(frame, detectors_bboxes[i][det_name])

                self.trackers_frame_init[i][1] = -1     #Update the parameters for the tracker
                self.trackers_bboxes_num[i][1] = len(detectors_bboxes[i][det_name])
                reset[i] = True   
            else:
                pass
        reset.append(self.frame_counter)
        return reset

    def track_video(self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
                    skip_frames_num=0, max_frames_reached=False, display=False, output=True):
        '''
        Track a video. It can be used in a loop if the evaluation of a dataset is considered advisable.
        
        @param video_input: the video that's to be tracked. Use full PATH. If a video from a webcam 
                            is going to be tracked used integers like 0,1, etc
        
        @param frame_size: the size of each frame
        
        @param iou_threshold: the accuracy error below which the tracking is considered acceptable
        
        @param gt_bboxes: The ground truth bounding boxes. If the video comes from webcam or just 
                          want to evaluate a video optically, insert the default None argument
        
        @param skip_frames_num: a uint. Determines the number of frames to skip to improve speed of application.
                                e.g. If skip_frames_num=3 only 1 frame per 4 will be examined
        
        @param max_frames_reached: bool. If True, an additional chech will be done inside tracker_reset()
        
        @return None
        '''
        if self.track_mode == 'dummy':
            self.track_video_dummy(video_input, frame_size, iou_threshold, gt_bboxes, \
                    skip_frames_num, max_frames_reached, display)

        elif self.track_mode == 'opencv_trackers':
            self.track_video_opencv(video_input, frame_size, iou_threshold, gt_bboxes, \
                    skip_frames_num, max_frames_reached, display)

        elif self.track_mode == 'particle_filters':
            self.track_video_pf(video_input, frame_size, iou_threshold, gt_bboxes, \
                    skip_frames_num, max_frames_reached, display)
        elif self.track_mode == 'sc_pf':
            self.track_video_sc_pf(video_input, frame_size, iou_threshold, gt_bboxes, \
                    skip_frames_num, max_frames_reached, display, output)
        else:
            ValueError('Wrong parameters. Exiting...')

        return None
        
    # TODO PLACE THIS FUNCTION SOMEWHERE ELSE. NAME INDICATES IT'S MEANT FOR DETECTOR CLASS, DOES NOT BELONG TO TRACKER MANAGER
    def det_frame_wrapper(self, frame, frame_size, det_name, det_obj):
        '''
        This is the implementation of a wrapper function. Instead of having to make loops
        inside other functions to use the proper parameters of the detector, it can be done
        with a sense of abstruction. No need to check every time the detector's name or to 
        loop over the det_params. Instead, this function is called. Despite the fact that
        a loop is used to find the desired parameters, the return is immediate.
        
        @param frame: the current frame
        @param frame_size: the (width, height) of the currenc frame
        @param det_name: the name of the detector
        @param det_obj: the object of a detector
        
        @return det_dict: a dictionary of the form: {detector_name: [bboxes]}
        '''
        utils = Utils()

        det_dict = {det_name: list()}
        det_rslt = None       
        for det in self.det_params:

            if det['name'] == det_name == 'haar':
                det_rslt = det_obj.cascade_detection_frame(frame, det['scale'], frame_size)            
                break

            elif det['name'] == det_name == 'hog':
                det_rslt = det_obj.hog_detection_frame(frame, det['winstride'], det['padding'],         \
                                                   det['scale'], det['threshold'], frame_size)
                break
            elif det['name'] == det_name == 'ssd':
                det_rslt = det_obj.ssd_detection_frame(frame, det['confidence'], det['display'])                                        
                break

            elif det['name'] == det_name == 'yolo':
                det_rslt = det_obj.yolo_detection_frame(frame, det['confidence'], det['threshold'],     \
                                                    det['blob_size'], det['display'])
                break

            else:
                ValueError('Unknown Detector --  Check Spelling --  Exiting...')
        
        # Ensure that every empty frame will have the same form
        # The operator <= might never be used for <. Only for =
        # However, some times algorithms make mistakes. They are
        # still human made in majority.
        if det_rslt is not None  and  len(det_rslt) <= 0:
            if det_rslt  == []  or det_rslt == ()  or  det_rslt == {}:
                det_dict[det_name] = []
        elif det_rslt is None:
            det_dict[det_name] = []
        else:
            det_bboxes = utils.remove_Nones(det_rslt)
            det_dict[det_name] = det_bboxes
        
        return det_dict


    # TODO
    def track_munero(self):
        pass


    def track_frame(self, frame, frame_size, max_frames_reached=False, display=False):
       
        if self.track_mode == 'dummy':
            return self.track_frame_dummy(frame, frame_size, max_frames_reached, display)
        elif self.track_mode == 'opencv_trackers':
            return self.track_frame_opencv(frame, frame_size, max_frames_reached, display)
        elif self.track_mode == 'particle_filters':
            return self.track_frame_pf(frame, frame_size, max_frames_reached, display)
        elif self.track_mode == 'sc_pf':
            return self.track_frame_sc(frame, frame_size,max_frames_reached, display)
        else:
            pass

        return None


    def track_frame_opencv(self, frame, frame_size, max_frames_reached=False, display=False):
        '''
        A crucial function for the Project. Inside it the detection and tracking for every input 
        frame is implemented. With on emajor difference from the other track_frame_functions.
        With this function, the project is mainly supported to the Preprogrammed Trackers
        that are provided by OpenCV 3-4-4. The detectors that have been implemented in this
        Project are used only for correction and update of the trackers. This function is
        important since it boosts the speed of the program. 

        @param frame: the current frame
        @param frame_size: the (width, height) of the frame
        @param max_frames_reached: bool. If True, the @tracker_reset() will make an additional check
        @param display: bool. If True, display the current frame. For visualisation purposes

        @return det_frame_bboxes, tracker_frame_bboxes: lists of dicts that contain the bboxes for
                every detector and tracker
        '''
        det_frame_bboxes = dict()
        track_frame_bboxes = dict()

        self.frame_counter += 1
        i = -1
        # det_dict = dict()
        # track_dict = dict()
        for det_name, det_obj, tracker_name, tracker_obj in self.track_list:
            i += 1
            det_dict = dict()
            track_dict = dict()
            if (self.trackers_frame_init[i][1] == -1  or  self.trackers_bboxes_num[i][1] == -1)     \
                and (tracker_obj is not None):        #Tracker without bboxes to track but not None Tracker
                # print self.trackers_frame_init
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                det_bboxes = det_dict[det_name]
                det_frame_bboxes.update(det_dict)
              
                if len(det_bboxes) > 0:
                    tracker_obj.tracker_add(frame, det_bboxes)      #det_dict[det_name] returns the bboxes of the detector with that name
                    temp_cntr = self.frame_counter
                    self.trackers_frame_init[i][1] = temp_cntr
                    self.track_list[i][3] = tracker_obj
                    self.trackers_bboxes_num[i][1] = len(det_bboxes)
                    track_dict = {tracker_name: det_bboxes}
                else:
                    track_dict = {tracker_name: []}
                    self.trackers_bboxes_num[i][1] = -1     # Trigger a reset in order to redetect
                    # print 'DEEP INSIDE INNOVATION! LINE 1140'
                
                track_frame_bboxes.update(track_dict)       #The init bboxes will be the same as the detector bboxes    
                
            elif tracker_obj is None:
                det_dict = {det_name: []}
                det_frame_bboxes.update(det_dict)
                continue
            else:
                det_dict = {det_name: []}
                det_frame_bboxes.update(det_dict)
                _, track_bboxes = tracker_obj.tracker_update(frame)
                # print 'Tracker bboxes {}'.format(track_bboxes)
                track_dict = {tracker_name: track_bboxes}
                track_frame_bboxes.update(track_dict)
                

        if display is True:
            for det_name, bboxes in det_frame_bboxes.iteritems():
                for bbox in bboxes:
                    bbox = [np.int(x) for x in bbox]
                    #               img, (    x  ,    y   ), (   x     +    w    , (   y    +    h   )),
                    #               color                  , thickness
                    cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0] + bbox[2]), (bbox[1] + bbox[3])), \
                                    self.colors[det_name], 2)
                    # cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), \
                    #               self.colors[det_name], 2)
                    text = '{}'.format(det_name)        #Write the name of the detector or tracker
                    cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[det_name], 2)

            for tracker_name, bboxes in track_frame_bboxes.iteritems():
                for bbox in bboxes:
                    # bbox = [int(pix) for pix in bbox]
                    bbox = [np.int(x) for x in bbox]
                    # print 'new bbox {}'.format(bbox)
                    #               img, (  x    ,    y   ), (  x   +   w    , (    y   +   h  )), 
                    #               color,             thickness
                    cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0]+bbox[2]), (bbox[1]+bbox[3])), \
                                self.colors[tracker_name], 2)
                    text = '{}'.format(tracker_name)        #Write the name of the detector or tracker
                    cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[tracker_name], 2)

        reset_log = self.track_reset_dummy(frame, det_frame_bboxes, track_frame_bboxes, max_frames_reached)

        if display is True:  
            text = '{}'.format(self.frame_counter)        #Write the name of the detector or tracker
            cv2.putText(frame, text, (20,30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0), 3)
                                            
            cv2.imshow('Tracking by Detectors and OpenCV Trackers', frame)
            cv2.waitKey(1)
         
        return det_frame_bboxes, track_frame_bboxes, reset_log

    # def dummy_video(self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
    #                 skip_frames_num=0, max_frames_reached=False, display=False):
    def track_video_opencv(self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
                    skip_frames_num=0, max_frames_reached=False, display=False):
        '''
        Track a video. It can be used in a loop if the evaluation of a dataset is considered advisable.
        
        @param video_input: the video that's to be tracked. Use full PATH. If a video from a webcam 
                            is going to be tracked used integers like 0,1, etc
        
        @param frame_size: the size of each frame
        
        @param iou_threshold: the accuracy error below which the tracking is considered acceptable
        
        @param gt_bboxes: The ground truth bounding boxes. If the video comes from webcam or just 
                          want to evaluate a video optically, insert the default None argument
        
        @param skip_frames_num: a uint. Determines the number of frames to skip to improve speed of application.
                                e.g. If skip_frames_num=3 only 1 frame per 4 will be examined
        
        @param max_frames_reached: bool. If True, an additional chech will be done inside tracker_reset()
        
        @return None
        '''
        vc = cv2.VideoCapture(video_input)      # Create obj to use with OpenCV
        utils = Utils()          

        self.frame_counter = -1
        self.trackers_frame_init = [[self.track_list[i][2] ,-1] for i in range(len(self.track_list))]
        self.trackers_bboxes_num = [[self.track_list[i][0] ,-1] for i in range(len(self.track_list))]

        if gt_bboxes is not None:
            dataset_eval = DatasetEvaluation()  
            # metrics = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'Mean_dist', 'True_Positives']
        if skip_frames_num !=0:
            gt_index_bboxes = list()
        if type(video_input) == int:
            print ('Video Streaming from Camera  ' + str(video_input))
        else:
            # Find the name of the video without the path and the encoding
            a = video_input.rfind('/')
            b = video_input.rfind('.')
            
            if a == b == -1:
                ValueError('Not a proper name for video. Probably forgot the encoding!!! Exiting...')
            elif a == -1:
                video_input = video_input[0:b]
            else:
                video_input = video_input[a+1: b]       #a+1 since at index a there is the character we want to avoid
            print('video:  ' + str(video_input))
            
        det_video_bboxes = list()
        tracker_video_bboxes = list()
        reset_log_files = list()


        frame_count = -1
        skip = True        #It's used for the check of the skip_num_frames
        while True:
            if skip == False:
                break 
            ok, frame = vc.read()
            if ok is False:
                break

            frame_count +=1
            
            if skip_frames_num != 0  and  frame_count == 0:
                gt_index_bboxes.append(frame_count)
            
            # NECESSARY SINCE THE CAVIAR DATASET FORGOT 1 FRAME. MIGHT BE USEFUL FOR OTHER IDIOTS IN THE FUTURE
            elif frame_count >= len(gt_bboxes):
                break
            elif skip_frames_num != 0  and  frame_count != 0  and  (frame_count % (skip_frames_num+1) != 0):
                gt_index_bboxes.append(frame_count)
            
            #Skip a number of frames to improve speed
            elif  frame_count != 0  and  skip_frames_num!=0  and  frame_count % (skip_frames_num+1) == 0:
                for _ in range(skip_frames_num - 1):   #It's necessary to subtract 1 because the first frame to skip has already been cancelled
                    skip, frame = vc.read()
                    if skip is False:
                        break
                        # print 'SKIP PROBLEM SUCKS 1262'
                    frame_count += 1
                continue
            
            det_frame_bboxes, tracker_frame_bboxes, reset_log = self.track_frame(frame, frame_size, max_frames_reached, display=display)
    
            if display is True  and  (cv2.waitKey(1) & 0xFF == 27):  # Esc pressed
                vc.release()
                cv2.destroyAllWindows()
                sys.exit('ESC Key pressed. Exiting...')
                
            # Expand the lists with the bboxes from each detector, tracker at each frame
            det_video_bboxes.append(det_frame_bboxes)
            tracker_video_bboxes.append(tracker_frame_bboxes)
            
            # print len(tracker_video_bboxes)
            # print len(det_video_bboxes), len(tracker_video_bboxes)

            if reset_log.count(True) > 0:
                if self.frame_counter == reset_log.count(True) == 1:
                    continue
                else:
                    reset_log_files.append(reset_log)
        

        if skip_frames_num != 0:
            gt_bboxes_skip_frames  = list()
            # print gt_index_bboxes, len(gt_bboxes)
            for index in gt_index_bboxes:
                gt_bboxes_skip_frames.append(gt_bboxes[index])
            gt_bboxes = gt_bboxes_skip_frames


        for det_name, _, tracker_name, _ in self.track_list:
            # det_title = True
            # track_title = True
            #Evaluation of the detectors
            det_eval = list()
            for det_frame in det_video_bboxes:
                # if len(det_frame[det_name]) == 0:
                #     det_eval.append([])
                # else:
                #     det_eval.append(det_frame[det_name])
                det_eval.append(det_frame[det_name])
            
            if gt_bboxes is not None:
                # det_total_metrics.append(det_video_metrics)
                # det_total_dists.append(det_video_dists)
                # print 'gt_bboxes in video {}, detected {}'.format(len(gt_bboxes), len(det_eval))
                if len(det_eval) != 0:
                    # print len(gt_bboxes), len(det_eval)
                    #TODO
                    #det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    # print det_title 
                    # if det_title is True:
                    utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                        # det_title = False
                    # else:
                    #     utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + det_name + '_dists' + CSV_END, 'a', det_video_dists, [])
            else:
                pass

            #Evaluation of the trackers
            if tracker_name is not None:
                tracker_eval = list()
                for tracker_frame in tracker_video_bboxes:
                    tracker_eval.append(tracker_frame[tracker_name])

                if gt_bboxes is not None:
                    # tracker_total_metrics.append(tracker_video_metrics)
                    # tracker_total_dists.append(tracker_total_dists)
                    # print 'gt_bboxes in video {}, tracked {}'.format(len(gt_bboxes), len(tracker_eval))
                    #TODO
                    #tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    
                    # if track_title == True:
                    utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                        # track_title = False
                    # else:
                        # utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + tracker_name + '_dists' + CSV_END, 'a', tracker_video_dists, [])
                else:
                    pass

        if len(reset_log_files) > 0:
            utils.csv_write(CSV_DATA_PATH + 'reset_log_' + 'video_' + video_input + CSV_END, 'a', reset_log_files, self.tracker_names)    
        if display is True:
            vc.release()
            cv2.destroyAllWindows()
        
        return None

    def track_frame_pf(self, frame, frame_size, max_frames_reached=False, display=False):
        '''
        A crucial function for the Project. Inside it the detection and tracking for every input 
        frame is implemented. With one major difference from the other track_frame functions.
        With this function, the project is mainly supported to the Particle Filters that are
        being used in this Project. The detectors that have been implemented in this
        Project are used only for correction and update of the filters. This function is
        important since it boosts the speed of the program.

                                    ***IMPORTANT***
        The deepgaze particle filter does not return a bounding box. Instead, it returns the
        [xc,yc] estimations of the center of the bbox. Furthermore, it does not return any other
        information for the size of the rectangle except for the MaxArearRectangle where it is 
        certain that the object is inside. However, this is not helpful for accurate tracking

        @param frame: the current frame
        @param frame_size: the (width, height) of the frame
        @param max_frames_reached: bool. If True, the @tracker_reset() will make an additional check
        @param display: bool. If True, display the current frame. For visualisation purposes

        @return det_frame_bboxes, tracker_frame_bboxes: lists of dicts that contain the bboxes for
                every detector and tracker
 
        '''
        utils = Utils()
        
        det_frame_bboxes = dict()
        pf_frame_bboxes = dict()
        self.frame_counter += 1
        i = -1
        # det_dict = dict()
        # track_dict = dict()
        for det_name, det_obj, pf_name, pf_objs in self.pf_list:
            i += 1
            det_dict = dict()
            pf_dict = dict()
            if (self.pf_frame_init[i][1] == -1  or  self.pf_bboxes_num[i][1] == -1):#     \
                #and (pf_objs is not None):        #Tracker without bboxes to track but not None Tracker
                # print self.trackers_frame_init
                print 'line 733'
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                det_bboxes = det_dict[det_name]
              
                if len(det_bboxes) > 0:
                    imgs_track = list()
                    for bbox in det_bboxes:
                        img_track = utils.crop_img(frame, bbox)
                        imgs_track.append(img_track)
                    pf_objs = self.pf_init_wrapper(frame_size, pf_name, det_bboxes, imgs_track)
                    
                    temp_cntr = self.frame_counter
                    self.pf_frame_init[i][1] = temp_cntr
                    self.pf_list[i][3] = pf_objs
                    self.pf_bboxes_num[i][1] = len(det_bboxes)
                    pf_dict = {pf_name: det_bboxes}
                else:
                    pf_dict = {pf_name: []}
                    self.pf_frame_init[i][1] = -1
                
            elif pf_objs is None:
                det_dict = {det_name: []}
            else:
                det_dict = {det_name: []}
                pf_bboxes = list()
                for pf_obj in pf_objs:
                    bbox = pf_obj.track_frame(frame, self.pf_params[pf_name], display)
                    pf_bboxes.append(bbox)    
                pf_dict = {pf_name: pf_bboxes}
            
            det_frame_bboxes.update(det_dict)
            pf_frame_bboxes.update(pf_dict)       #The init bboxes will be the same as the detector bboxes    

        if display is True:
            for det_name, bboxes in det_frame_bboxes.iteritems():
                for bbox in bboxes:
                    bbox = [np.int(x) for x in bbox]
                    #               img, (    x  ,    y   ), (   x     +    w    , (   y    +    h   )),
                    #               color                  , thickness
                    cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0] + bbox[2]), (bbox[1] + bbox[3])), \
                                    self.colors[det_name], 2)
                    # cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), \
                    #               self.colors[det_name], 2)
                    text = '{}'.format(det_name)        #Write the name of the detector or tracker
                    cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[det_name], 2)

            for pf_name, bboxes in pf_frame_bboxes.iteritems():
                for bbox in bboxes:
                    bbox = [np.int(x) for x in bbox]
                    
                    if len(bbox) == 2:      # Corresponds to [xc,yc] coords
                        cv2.circle(frame, (bbox[0],bbox[1]), 3, [0,255,0], 5)   # Green Dot
                        text = '{}'.format(pf_name)        #Write the name of the detector or tracker
                        cv2.putText(frame, text, (bbox[0], bbox[1] - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[pf_name], 2)

                    elif len(bbox) == 4:      # Corresponds to [x,y,w,h] coords
                        #               img, (  x    ,    y   ), (  x   +   w    , (    y   +   h  )), 
                        #               color,             thickness
                        cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0]+bbox[2]), (bbox[1]+bbox[3])), \
                                    self.colors[pf_name], 2)
                        text = '{}'.format(pf_name)        #Write the name of the detector or tracker
                        cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[pf_name], 2)

        reset_log = self.pf_reset(frame, frame_size, det_frame_bboxes, pf_frame_bboxes, max_frames_reached)

        if display is True:  
            text = '{}'.format(self.frame_counter)        #Write the name of the detector or tracker
            cv2.putText(frame, text, (20,30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0), 3)
                                            
            cv2.imshow('Tracking by Detectors and OpenCV Trackers', frame)
            cv2.waitKey(1)
         
        return det_frame_bboxes, pf_frame_bboxes, reset_log              

    #iou_threshold
    def track_video_pf (self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
                    skip_frames_num=0, max_frames_reached=False, display=False):
        '''
        Track a video. It can be used in a loop if the evaluation of a dataset is considered advisable.
        
        @param video_input: the video that's to be tracked. Use full PATH. If a video from a webcam 
                            is going to be tracked used integers like 0,1, etc
        
        @param frame_size: the size of each frame
        
        @param iou_threshold: the accuracy error below which the tracking is considered acceptable
        
        @param gt_bboxes: The ground truth bounding boxes. If the video comes from webcam or just 
                          want to evaluate a video optically, insert the default None argument
        
        @param skip_frames_num: a uint. Determines the number of frames to skip to improve speed of application.
                                e.g. If skip_frames_num=3 only 1 frame per 4 will be examined
        
        @param max_frames_reached: bool. If True, an additional chech will be done inside tracker_reset()
        
        @return None
        '''
        vc = cv2.VideoCapture(video_input)      # Create obj to use with OpenCV
        utils = Utils()          

        self.frame_counter = -1
        self.pf_frame_init = [[self.pf_list[i][2] ,-1] for i in range(len(self.pf_list))]
        self.pf_bboxes_num = [[self.pf_list[i][0] ,-1] for i in range(len(self.pf_list))]

        if gt_bboxes is not None:
            dataset_eval = DatasetEvaluation()  
            # metrics = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'Mean_dist', 'True_Positives']
        if skip_frames_num !=0:
            gt_index_bboxes = list()
        if type(video_input) == int:
            print ('Video Streaming from Camera  ' + str(video_input))
        else:
            # Find the name of the video without the path and the encoding
            a = video_input.rfind('/')
            b = video_input.rfind('.')
            
            if a == b == -1:
                ValueError('Not a proper name for video. Probably forgot the encoding!!! Exiting...')
            elif a == -1:
                video_input = video_input[0:b]
            else:
                video_input = video_input[a+1: b]       #a+1 since at index a there is the character we want to avoid
            print('video:  ' + str(video_input))
            
        det_video_bboxes = list()
        pf_video_bboxes = list()
        reset_log_files = list()


        frame_count = -1
        skip = True        #It's used for the check of the skip_num_frames
        while True:
            if skip == False:
                break 
            ok, frame = vc.read()
            if ok is False:
                break

            frame_count +=1
            
            if skip_frames_num != 0  and  frame_count == 0:
                gt_index_bboxes.append(frame_count)
            
            # NECESSARY SINCE THE CAVIAR DATASET FORGOT 1 FRAME. MIGHT BE USEFUL FOR OTHER IDIOTS IN THE FUTURE
            elif frame_count >= len(gt_bboxes):
                break
            elif skip_frames_num != 0  and  frame_count != 0  and  (frame_count % (skip_frames_num+1) != 0):
                gt_index_bboxes.append(frame_count)
            
            #Skip a number of frames to improve speed
            elif  frame_count != 0  and  skip_frames_num!=0  and  frame_count % (skip_frames_num+1) == 0:
                for _ in range(skip_frames_num - 1):   #It's necessary to subtract 1 because the first frame to skip has already been cancelled
                    skip, frame = vc.read()
                    if skip is False:
                        break
                        # print 'SKIP PROBLEM SUCKS 1262'
                    frame_count += 1
                continue
            
            det_frame_bboxes, pf_frame_bboxes, reset_log = self.track_frame(frame, frame_size, max_frames_reached, display=display)
    
            if display is True  and  (cv2.waitKey(1) & 0xFF == 27):  # Esc pressed
                vc.release()
                cv2.destroyAllWindows()
                sys.exit('ESC Key pressed. Exiting...')
                
            # Expand the lists with the bboxes from each detector, tracker at each frame
            det_video_bboxes.append(det_frame_bboxes)
            pf_video_bboxes.append(pf_frame_bboxes)
            
            # print len(tracker_video_bboxes)
            # print len(det_video_bboxes), len(tracker_video_bboxes)

            if reset_log.count(True) > 0:
                if self.frame_counter == reset_log.count(True) == 1:
                    continue
                else:
                    reset_log_files.append(reset_log)
        

        if skip_frames_num != 0:
            gt_bboxes_skip_frames  = list()
            # print gt_index_bboxes, len(gt_bboxes)
            for index in gt_index_bboxes:
                gt_bboxes_skip_frames.append(gt_bboxes[index])
            gt_bboxes = gt_bboxes_skip_frames


        for det_name, _, pf_name, _ in self.pf_list:
            # det_title = True
            # track_title = True
            #Evaluation of the detectors
            det_eval = list()
            for det_frame in det_video_bboxes:
                # if len(det_frame[det_name]) == 0:
                #     det_eval.append([])
                # else:
                #     det_eval.append(det_frame[det_name])
                det_eval.append(det_frame[det_name])
            
            if gt_bboxes is not None:
                # det_total_metrics.append(det_video_metrics)
                # det_total_dists.append(det_video_dists)
                # print 'gt_bboxes in video {}, detected {}'.format(len(gt_bboxes), len(det_eval))
                if len(det_eval) != 0:
                    # print len(gt_bboxes), len(det_eval)
                    #TODO
                    #det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    # print det_title 
                    # if det_title is True:
                    utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                        # det_title = False
                    # else:
                    #     utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + det_name + '_dists' + CSV_END, 'a', det_video_dists, [])
            else:
                pass

            #Evaluation of the trackers
            if pf_name is not None:
                pf_eval = list()
                for pf_frame in pf_video_bboxes:
                    pf_eval.append(pf_frame[pf_name])

                if gt_bboxes is not None:
                    print 'gt_bboxes in video {}, tracked {}'.format(len(gt_bboxes), len(pf_eval))
                    #TODO
                    #tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    pf_video_metrics, pf_video_dists = dataset_eval.eval_video(gt_bboxes, pf_eval, iou_threshold, frame_size)
                    
                    # if track_title == True:
                    utils.csv_write(CSV_DATA_PATH + pf_name + '_metrics' + CSV_END, 'a', pf_video_metrics, [])
                        # track_title = False
                    # else:
                        # utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + pf_name + '_dists' + CSV_END, 'a', pf_video_dists, [])
                else:
                    pass

        if len(reset_log_files) > 0:
            utils.csv_write(CSV_DATA_PATH + 'reset_log_' + 'video_' + video_input + CSV_END, 'a', reset_log_files, self.pf_names)    
        if display is True:
            vc.release()
            cv2.destroyAllWindows()
        
        return None

    def pf_init_wrapper(self, frame_size, pf_name, bboxes, imgs_track):
        '''
        Initialisation of the partile filter objects depending on the 
        bboxes that were given as input. For each bounding box, a particle
        filter object will be created. At the end, the objects will be
        placed at the self.pf_list of the Class

        @param frame_size: the size of the frame
        @param pf_name: the name of the particle filter
        @param bboxes: the bboxes to track

        @return pf_objs: the particle filter objects
        '''
        pf_objs = list()
        if pf_name == 'huang':
            for i, bbox in enumerate(bboxes):
                pf_obj = HuangPF(self.pf_params['huang'])
                pf_obj.init_HuangPF(imgs_track[i], bbox)
                pf_objs.append(pf_obj)

        elif pf_name == 'deepgaze':
            particles = self.pf_params['deepgaze'][0]
            std = self.pf_params['deepgaze'][1]
            noise_prob = self.pf_params['deepgaze'][2]

            for i, bbox in enumerate(bboxes):
                pf_obj = DeepgazePF(particles, frame_size, std, noise_prob)
                pf_obj.img_select(imgs_track[i])
                pf_objs.append(pf_obj)
        
        for i in range(len(self.pf_list)):
            if self.pf_list[i][2] == pf_name:
                self.pf_list[i][3] = pf_objs
        return pf_objs
    

    def pf_reset(self, frame, frame_size, detectors_bboxes, pf_bboxes, max_frames_reached=False):
        '''
        This function is used for reseting the trackers that have been problematic.
        A check of the number of the bboxes is done in order to find out if the bboxes
        that are being tracked need are the same number as the initial. Furthermore, it
        is considered a good practice to update the trackers every a num of frames to ensure
        better tracking results.

        #TODO add further functionality using more parameters, e.g. comparison with the 
        detector_bboxes, etc.

        @param frame: the current frame
        @param frame_size: (width, height)
        @detectors_bboxes:  a list that contains the bboxes of all the detectors that are being 
                            used. Technically, it is a list of dicts.
       
        @pf_bboxes: a list that contains the bboxes of all the pfs that are being 
                    used. Technically, it is a list of dicts. However, for every detector
                    obj without a pf obj, a None is appended to the list
       
        @param max_frames_reached:  a bool variable. If it is set True, the function will make an
                                    additional check, in order to determine if the max_frames using
                                    a tracker are reached and we need to reset it.

        @return reset: a list with bool variables and the frame number. It's initialised as a list of False elements.
                       If a tracker resets, the element at that index is the list is set True.
        '''
        utils = Utils()
        reset = [False for i in range(len(self.pf_list))]
    
        for i in range(len(self.pf_list)):
            det_name = self.pf_list[i][0]
            pf_name = self.pf_list[i][2]

            print (self.frame_counter, self.pf_frame_init[i][1])

            if self.pf_list[i][2] == None  or  self.pf_list[i][3] == None:
                continue
 

            elif max_frames_reached == True  and  (self.frame_counter - self.pf_frame_init[i][1] >= self.track_max_frames):
                print '861 pf reset'
                
                if len(detectors_bboxes[det_name]) != 0:
                    imgs_track = list()
                    for bbox in detectors_bboxes[det_name]:
                        img_track = utils.crop_img(frame, bbox)
                        imgs_track.append(img_track)                
                    
                    self.pf_init_wrapper(frame_size, pf_name, detectors_bboxes[det_name], imgs_track)
            
                self.pf_frame_init[i][1] = -1     #Update the parameters for the tracker
                self.pf_bboxes_num[i][1] = -1     #len(detectors_bboxes[det_name])
                reset[i] = True  

            elif max_frames_reached==False  and  self.pf_bboxes_num[i][1] != len(pf_bboxes[pf_name]):
                # print 'line 1390'
                
                # self.track_list[i][3].tracker_add(frame, detectors_bboxes[det_name])

                self.pf_frame_init[i][1] = -1     #Update the parameters for the tracker
                self.pf_bboxes_num[i][1] = -1     #len(detectors_bboxes[det_name])
                reset[i] = True   

 
            else:
                pass
                # print 'WTF LINE 1418'
                
        reset.append(self.frame_counter)
        
        return reset





    def track_frame_dummy(self, frame, frame_size, max_frames_reached=False, display=False):
        '''
        A crucial function for the Project. Inside it the detection and tracking for every input 
        frame is implemented.
        @param frame: the current frame
        @param frame_size: the (width, height) of the frame
        @param max_frames_reached: bool. If True, the @tracker_reset() will make an additional check
        @param display: bool. If True, display the current frame. For visualisation purposes

        @return det_frame_bboxes, tracker_frame_bboxes: lists of dicts that contain the bboxes for
                every detector and tracker
        '''
   
        det_frame_bboxes = dict()
        track_frame_bboxes = dict()

        self.frame_counter += 1
        i = -1
        # det_dict = dict()
        # track_dict = dict()
        for det_name, det_obj, tracker_name, tracker_obj in self.track_list:
            i += 1
            det_dict = dict()
            track_dict = dict()
            if (self.trackers_frame_init[i][1] == -1  or  self.trackers_bboxes_num[i][1] == -1)     \
                and (tracker_obj is not None):        #Tracker without bboxes to track but not None Tracker
                # print self.trackers_frame_init
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                det_bboxes = det_dict[det_name]
              
                if len(det_bboxes) > 0:
                    tracker_obj.tracker_add(frame, det_bboxes)      #det_dict[det_name] returns the bboxes of the detector with that name
                    temp_cntr = self.frame_counter
                    self.trackers_frame_init[i][1] = temp_cntr
                    self.track_list[i][3] = tracker_obj
                    self.trackers_bboxes_num[i][1] = len(det_bboxes)
                    track_dict = {tracker_name: det_bboxes}
                else:
                    track_dict = {tracker_name: []}
                    self.trackers_bboxes_num[i][1] = -1     # Trigger a reset in order to redetect
                    # print 'DEEP INSIDE INNOVATION! LINE 1140'
                
                # track_frame_bboxes.update(track_dict)       #The init bboxes will be the same as the detector bboxes    
                
            elif tracker_obj is None:
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                # track_frame_bboxes.append(None)
            else:
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                _, track_bboxes = tracker_obj.tracker_update(frame)
                # print 'Tracker bboxes {}'.format(track_bboxes)
                track_dict = {tracker_name: track_bboxes}
            
            track_frame_bboxes.update(track_dict)    
            det_frame_bboxes.update(det_dict)

        if display is True:
            for det_name, bboxes in det_frame_bboxes.iteritems():
                for bbox in bboxes:
                    bbox = [np.int(x) for x in bbox]
                    #               img, (    x  ,    y   ), (   x     +    w    , (   y    +    h   )),
                    #               color                  , thickness
                    cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0] + bbox[2]), (bbox[1] + bbox[3])), \
                                    self.colors[det_name], 2)
                    # cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), \
                    #               self.colors[det_name], 2)
                    text = '{}'.format(det_name)        #Write the name of the detector or tracker
                    cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[det_name], 2)

            for tracker_name, bboxes in track_frame_bboxes.iteritems():
                for bbox in bboxes:
                    # bbox = [int(pix) for pix in bbox]
                    bbox = [np.int(x) for x in bbox]
                    # print 'new bbox {}'.format(bbox)
                    #               img, (  x    ,    y   ), (  x   +   w    , (    y   +   h  )), 
                    #               color,             thickness
                    cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0]+bbox[2]), (bbox[1]+bbox[3])), \
                                self.colors[tracker_name], 2)
                    text = '{}'.format(tracker_name)        #Write the name of the detector or tracker
                    cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[tracker_name], 2)
            cv2.rectangle(frame, (50,50), (90,90), (0,255,0), -1)
        reset_log = self.track_reset_dummy(frame, det_frame_bboxes, track_frame_bboxes, max_frames_reached)

        if display is True:  
            text = '{}'.format(self.frame_counter)        #Write the name of the detector or tracker
            cv2.putText(frame, text, (20,30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0), 3)
                                            
            cv2.imshow('Tracking by Detectors and OpenCV Trackers', frame)
            cv2.waitKey(1)
         
        return det_frame_bboxes, track_frame_bboxes, reset_log

    # def dummy_video(self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
    #                 skip_frames_num=0, max_frames_reached=False, display=False):
    def track_video_dummy(self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
                    skip_frames_num=0, max_frames_reached=False, display=False):
        '''
        Track a video. It can be used in a loop if the evaluation of a dataset is considered advisable.
        
        @param video_input: the video that's to be tracked. Use full PATH. If a video from a webcam 
                            is going to be tracked used integers like 0,1, etc
        
        @param frame_size: the size of each frame
        
        @param iou_threshold: the accuracy error below which the tracking is considered acceptable
        
        @param gt_bboxes: The ground truth bounding boxes. If the video comes from webcam or just 
                          want to evaluate a video optically, insert the default None argument
        
        @param skip_frames_num: a uint. Determines the number of frames to skip to improve speed of application.
                                e.g. If skip_frames_num=3 only 1 frame per 4 will be examined
        
        @param max_frames_reached: bool. If True, an additional chech will be done inside tracker_reset()
        
        @return None
        '''
        vc = cv2.VideoCapture(video_input)      # Create obj to use with OpenCV
        utils = Utils()          

        self.frame_counter = -1
        self.trackers_frame_init = [[self.track_list[i][2] ,-1] for i in range(len(self.track_list))]
        self.trackers_bboxes_num = [[self.track_list[i][0] ,-1] for i in range(len(self.track_list))]

        if gt_bboxes is not None:
            dataset_eval = DatasetEvaluation()  
            # metrics = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'Mean_dist', 'True_Positives']
        if skip_frames_num !=0:
            gt_index_bboxes = list()
        if type(video_input) == int:
            print ('Video Streaming from Camera  ' + str(video_input))
        else:
            # Find the name of the video without the path and the encoding
            a = video_input.rfind('/')
            b = video_input.rfind('.')
            
            if a == b == -1:
                ValueError('Not a proper name for video. Probably forgot the encoding!!! Exiting...')
            elif a == -1:
                video_input = video_input[0:b]
            else:
                video_input = video_input[a+1: b]       #a+1 since at index a there is the character we want to avoid
            print('video:  ' + str(video_input))
            
        det_video_bboxes = list()
        tracker_video_bboxes = list()
        reset_log_files = list()


        frame_count = -1
        skip = True        #It's used for the check of the skip_num_frames
        while True:
            if skip == False:
                break 
            ok, frame = vc.read()
            if ok is False:
                break

            frame_count +=1
            
            if skip_frames_num != 0  and  frame_count == 0:
                gt_index_bboxes.append(frame_count)
            
            # NECESSARY SINCE THE CAVIAR DATASET FORGOT 1 FRAME. MIGHT BE USEFUL FOR OTHER IDIOTS IN THE FUTURE
            elif frame_count >= len(gt_bboxes):
                break
            elif skip_frames_num != 0  and  frame_count != 0  and  (frame_count % (skip_frames_num+1) != 0):
                gt_index_bboxes.append(frame_count)
            
            #Skip a number of frames to improve speed
            elif  frame_count != 0  and  skip_frames_num!=0  and  frame_count % (skip_frames_num+1) == 0:
                for _ in range(skip_frames_num - 1):   #It's necessary to subtract 1 because the first frame to skip has already been cancelled
                    skip, frame = vc.read()
                    if skip is False:
                        break
                        # print 'SKIP PROBLEM SUCKS 1262'
                    frame_count += 1
                continue
            
            det_frame_bboxes, tracker_frame_bboxes, reset_log = self.track_frame(frame, frame_size, max_frames_reached, display=display)
    
            if display is True  and  (cv2.waitKey(1) & 0xFF == 27):  # Esc pressed
                vc.release()
                cv2.destroyAllWindows()
                sys.exit('ESC Key pressed. Exiting...')
                
            # Expand the lists with the bboxes from each detector, tracker at each frame
            det_video_bboxes.append(det_frame_bboxes)
            tracker_video_bboxes.append(tracker_frame_bboxes)
            
            # print len(tracker_video_bboxes)
            # print len(det_video_bboxes), len(tracker_video_bboxes)

            if reset_log.count(True) > 0:
                if self.frame_counter == reset_log.count(True) == 1:
                    continue
                else:
                    reset_log_files.append(reset_log)
        

        if skip_frames_num != 0:
            gt_bboxes_skip_frames  = list()
            # print gt_index_bboxes, len(gt_bboxes)
            for index in gt_index_bboxes:
                gt_bboxes_skip_frames.append(gt_bboxes[index])
            gt_bboxes = gt_bboxes_skip_frames


        for det_name, _, tracker_name, _ in self.track_list:
            # det_title = True
            # track_title = True
            #Evaluation of the detectors
            det_eval = list()
            for det_frame in det_video_bboxes:
                # if len(det_frame[det_name]) == 0:
                #     det_eval.append([])
                # else:
                #     det_eval.append(det_frame[det_name])
                det_eval.append(det_frame[det_name])
            
            if gt_bboxes is not None:
                # det_total_metrics.append(det_video_metrics)
                # det_total_dists.append(det_video_dists)
                # print 'gt_bboxes in video {}, detected {}'.format(len(gt_bboxes), len(det_eval))
                if len(det_eval) != 0:
                    # print len(gt_bboxes), len(det_eval)
                    #TODO
                    #det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    # print det_title 
                    # if det_title is True:
                    utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                        # det_title = False
                    # else:
                    #     utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + det_name + '_dists' + CSV_END, 'a', det_video_dists, [])
            else:
                pass

            #Evaluation of the trackers
            if tracker_name is not None:
                tracker_eval = list()
                for tracker_frame in tracker_video_bboxes:
                    tracker_eval.append(tracker_frame[tracker_name])

                if gt_bboxes is not None:
                    # tracker_total_metrics.append(tracker_video_metrics)
                    # tracker_total_dists.append(tracker_total_dists)
                    print 'gt_bboxes in video {}, tracked {}'.format(len(gt_bboxes), len(tracker_eval))
                    #TODO
                    #tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    
                    # if track_title == True:
                    utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                        # track_title = False
                    # else:
                        # utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + tracker_name + '_dists' + CSV_END, 'a', tracker_video_dists, [])
                else:
                    pass

        if len(reset_log_files) > 0:
            utils.csv_write(CSV_DATA_PATH + 'reset_log_' + 'video_' + video_input + CSV_END, 'a', reset_log_files, self.tracker_names)    
        if display is True:
            vc.release()
            cv2.destroyAllWindows()
        
        return None


    def track_reset_dummy(self, frame, detectors_bboxes, trackers_bboxes, max_frames_reached=False):
        '''
        This function is used for reseting the trackers that have been problematic.
        A check of the number of the bboxes is done in order to find out if the bboxes
        that are being tracked need are the same number as the initial. Furthermore, it
        is considered a good practice to update the trackers every a num of frames to ensure
        better tracking results.

        #TODO add further functionality using more parameters, e.g. comparison with the 
        detector_bboxes, etc.

        @param frame: the current frame
       
        @detectors_bboxes:  a dict that contains the bboxes of all the detectors that were 
                            detected in a frame
       
        @trackers_bboxes:   a dict that contains the bboxes of all the trackers that were
                            detected.
       
        @param max_frames_reached:  a bool variable. If it is set True, the function will make an
                                    additional check, in order to determine if the max_frames using
                                    a tracker are reached and we need to reset it.

        @return reset: a list with bool variables and the frame number. It's initialised as a list of False elements.
                       If a tracker resets, the element at that index is the list is set True.
        '''

        reset = [False for i in range(len(self.track_list))]
        # num_dets = len(self.track_list)
        
        # if self.trackers_bboxes_num == [[self.track_list[i][2] ,-1] for i in range(len(self.track_list))]:
        #     return reset
        # if len(trackers_bboxes) == 0:
        #     return reset
        

        for i in range(len(self.track_list)):
            det_name = self.track_list[i][0]
            tracker_name = self.track_list[i][2]

            # if self.track_list[i][2] is not None:
            #     print (self.frame_counter, self.trackers_frame_init[i][1])

            if self.track_list[i][2] == None  or  self.track_list[i][3] == None:
                continue
 
            # elif len(trackers_bboxes[tracker_name]) == 0:
            #     continue

            elif max_frames_reached == True  and  (self.frame_counter - self.trackers_frame_init[i][1] >= self.track_max_frames):
                # print 'Line 1400'
                multi_tracker = MultiTracker()
                multi_tracker.tracker_select(self.track_list[i][2])     #Select the name of the tracker
                self.track_list[i][3] = multi_tracker
                # self.track_list[i][3].tracker_add(frame, detectors_bboxes[det_name])

                self.trackers_frame_init[i][1] = -1     #Update the parameters for the tracker
                self.trackers_bboxes_num[i][1] = -1#len(detectors_bboxes[det_name])
                reset[i] = True  



            elif max_frames_reached==False  and  self.trackers_bboxes_num[i][1] != len(trackers_bboxes[tracker_name]):
                # print 'line 1390'
                multi_tracker = MultiTracker()
                multi_tracker.tracker_select(self.track_list[i][2])     #Select the name of the tracker
                self.track_list[i][3] = multi_tracker
                # self.track_list[i][3].tracker_add(frame, detectors_bboxes[det_name])

                self.trackers_frame_init[i][1] = -1     #Update the parameters for the tracker
                self.trackers_bboxes_num[i][1] = len(detectors_bboxes[det_name])
                reset[i] = True   

 
            else:
                pass
                # print 'WTF LINE 1418'
                
        reset.append(self.frame_counter)
        
        return reset


    def cur_state_evaluation(self, cur_state, cur_vel, algorithm, frame_size, pair=0, visible_thres=10):
        '''
        This function implements the evaluation of the results of the trackers and the detectors at a single frame. 
        To be more accurate, the model makes use of the knowledge of the previous and pre-previous states, thus 
        determining the velocity of each bbox. The velocity of each bbox is determined by attempting to find the
        bbox from the pre-previous state at the previous with the use of a constant (not the COSMOLOGICAL one, chill)
        that we assume is the maximum speed of an obj that can develop in consecutive frames. For example, if we 
        assume that an obj can move at consecutive frames max 30 pixels radius, then we can narrow the search field.

        However, this is not all. This function implements another crucial action. By determinig correctly the
        velocities of the bboxes, an estimation of the position of each current bbox can be made, sth that 
        makes the algorithm more robust to occlusion. If sth like this works, I will be thrilled! 
        
        @param cur_state: a list of dicts, where each represents a seperate state, it is of the form
            [{'haar': bboxes, 'CSRT': bboxes}, {...}, ...]
        
        @param cur_vel: a list of dicts, exactly like the cur_state, with one difference. Instead of bboxes,
            the [dx,dy] velocities are the arguments
        
        @param algorithm: a string with the name of the detector or the tracker that is to be used. Inside the
            code for simplicity, it is renamed dn (comes from detector_name, for honourary purposes since the code
            was initially created for detectors only)
        
        @param frame_size: the size of the frame
        
        @param pair: the index inside self.track_list for the desired detector-tracker pair

        @param visible thres: the threshold in number of pixels, in order to find if the bbox is located 
            at the boundaries of the frame or not.
        
        @return cur_state, cur_vel, and the state & vel for the single operation as simple lists
        '''
        utils = Utils()

        dn = algorithm
        i = pair

        current_states = {dn: [], 'id': []}
        current_vels = []

        prev_unused_indices = list()

        dists_mtx = np.array([])

        if type(cur_state[i][dn]) == np.ndarray:
            cur_state[i][dn] = cur_state[i][dn].tolist()

        detectors = [x for x,_,_,_ in self.track_list]
        id_assign = False
        if dn in detectors:
            id_assign = True
        
        try:
            csl = len(cur_state[i][dn])
            cur_state[i][dn] = utils.remove_Nones(cur_state[i][dn])
            cur_unused = copy.deepcopy(cur_state[i][dn])

        except:
            csl = 0
            cur_unused = []

        try:
            psl = len(self.prev_state[i][dn])
            self.prev_state[i][dn] = utils.remove_Nones(self.prev_state[i][dn])
            prev_unused = copy.deepcopy(self.prev_state[i][dn])
            
            self.prev_vel[i][dn] = utils.remove_Nones(self.prev_vel[i][dn])

        except:
            psl=0
            prev_unused = []

        cur_v = [[0,0] for _ in range(len(cur_state[i][dn]))]
        
        if csl != 0:
            ids = [-1 for _ in range(len(cur_state[i][dn]))]
        else:
            ids = list()

        
        if csl == 0  and psl == 0:
            return current_states, current_vels, prev_unused_indices
        
        elif  csl == 0  and  psl != 0:
            for j, dp_bbox in enumerate(self.prev_state[i][dn]):
                if dp_bbox is None  or  len(dp_bbox) == 0:
                    continue
                pxc = dp_bbox[0] + dp_bbox[2]/2
                pyc = dp_bbox[1] + dp_bbox[3]/2


                #TODO CHECK IT
                # for pf_indx in range(len(self.pf_objs)):
                #     self.pf_objs[pf_indx].flag_none_bbox += 1



                index = self.prev_state[i][dn].index(dp_bbox)
                if pxc < visible_thres  or  pxc > frame_size[0] - visible_thres:
                    if id_assign is True:
                        # ident = self.prev_state[i]['id'][index]
                        # self.avail_ids.append(ident)
                        
                        # pf_tmp = copy.deepcopy(self.pf_objs)
                        # for pf_indx in range(len(self.pf_objs)):
                        #     if self.pf_objs[pf_indx].id == ident:
                        #         pf_tmp.remove(self.pf_objs[pf_indx])
                        # self.pf_objs = list()
                        # self.pf_objs = pf_tmp
                        pass
                    else:
                        continue

                elif pyc < visible_thres  or  pyc > frame_size[1] - visible_thres:
                    if id_assign is True:
                        # ident = self.prev_state[i]['id'][index]
                        # self.avail_ids.append(ident)
                        
                        # pf_tmp = copy.deepcopy(self.pf_objs)
                        # for pf_indx in range(len(self.pf_objs)):
                        #     if self.pf_objs[pf_indx].id == ident:
                        #         pf_tmp.remove(self.pf_objs[pf_indx])
                        # self.pf_objs = list()
                        # self.pf_objs = pf_tmp
                        pass
                    else:
                        continue

                else:
                    try:
                        dp_bbox[0] += self.prev_vel[i][dn][j][0]  # Add the dx, dy velocities to improve robustness
                        dp_bbox[1] += self.prev_vel[i][dn][j][1]  # Add the dx, dy velocities to improve robustness
                    
                        cur_state[i][dn].append(dp_bbox)     
                        cur_vel[i][dn].append([0,0])    # Make the prev bbox current and move it by old velocity, then make vel=0
                        
                        current_states[dn].append(dp_bbox)
                        try:
                            ident = self.prev_state[i]['id'][j] 
                        except:
                            self._reset_avail_ids()
                            ident = self.avail_ids.pop()

                        current_states['id'].append(ident)
                        current_vels.append([0,0])
                    
                    except:
                        pass

                    # ident = self.prev_ids[i][dn][j] 
                    # ids.append(ident)
            # self.ids[i][dn] = ids

        elif csl != 0  and  psl != 0:          
            for dci, dc_bbox in enumerate(cur_state[i][dn]):
                if dc_bbox is None  or  len(dc_bbox) == 0:
                    continue
                cxc = dc_bbox[0] + dc_bbox[2]/2
                cyc = dc_bbox[1] + dc_bbox[3]/2
                
                dist_tmp = list()
                
                for dpi, dp_bbox in enumerate(self.prev_state[i][dn]):
                    if dp_bbox is None  or  len(dp_bbox) == 0:
                        continue
                    pxc = dp_bbox[0] + dp_bbox[2]/2
                    pyc = dp_bbox[1] + dp_bbox[3]/2

                    centers_dist = np.sqrt((cxc - pxc)**2 + (cyc - pyc)**2)
                    dist_tmp.append(centers_dist)

                dist_arr = np.asarray(dist_tmp)

                if dci == 0:
                    dists_mtx = dist_arr
                elif len(dists_mtx) == 0:
                    continue
                else:
                    dists_mtx = np.vstack((dists_mtx, dist_arr))    
                # dist_list.append(dist_tmp)

            # Find the minimum element of the array and remove(or make 1000) the column in which it was found
            if len(dists_mtx) != 0:
                if len(dists_mtx.shape) > 1:
                    rows, cols = dists_mtx.shape
                else:
                    rows = dists_mtx.shape
                    cols = 1
                    dists_mtx = np.array([dists_mtx])
            else:
                rows = cols = 0
            #deleted_cols = 0
            while cols > 0  and  np.min(dists_mtx) <= self.max_dist_radius:
                
                min_coords = np.where(dists_mtx == np.min(dists_mtx))
                        
                dc_index = min_coords[0][0] 
                dp_index = min_coords[1][0] #+ deleted_cols  #Add the number of the deleted cols to keep the indices correct only if the
                                            # concept is to delete the rows, cols. Otherwise, it is of no use
                
                row = dc_index
                col = dp_index

                dc_bbox = cur_state[i][dn][dc_index]
                cxc = dc_bbox[0] + dc_bbox[2] / 2
                cyc = dc_bbox[1] + dc_bbox[3] / 2

                
                try:
                    dp_bbox = self.prev_state[i][dn][dp_index]
                    # if dp_bbox is None:
                    #     continue
                    pxc = dp_bbox[0] + dp_bbox[2] / 2
                    pyc = dp_bbox[1] + dp_bbox[3] / 2

                    dx = cxc - pxc
                    dy = cyc - pyc
                    
                    cur_v[dc_index] = [dx, dy]

                    ident = self.prev_state[i]['id'][dp_index]
                    
                    current_states[dn].append(dc_bbox)
                    current_states['id'].append(ident)
                    
                    current_vels.append([dx, dy])

                    #Create the lists with the unused previous and current bboxes
                    #They are used for the improvement of the code
                    prev_unused.remove(dp_bbox)                
                    cur_unused.remove(dc_bbox)

                    # ids[dc_index] = ident
                except:
                    # print ('ERROR 1781! ' + str(dp_index))
                    # print self.prev_state[i]['id']
                    # print len(self.prev_state[i]['id'])
                    # print psl
                    pass
                # dists_mtx = np.delete(dists_mtx, col, axis=1)   # Delete Column
                dists_mtx[:, col] = 1000
                dists_mtx[row, :] = 1000    # Assign a large number to the elements of the row in order to not be selected
                # deleted_cols += 1


            for dp_bbox in prev_unused:
                # dp_bbox = prev_bbox
                if dp_bbox is None  or  len(dp_bbox) == 0:
                    continue
                
                index = self.prev_state[i][dn].index(dp_bbox)
                
                pxc = dp_bbox[0] + dp_bbox[2] / 2
                pyc = dp_bbox[1] + dp_bbox[3] / 2

                if pxc < visible_thres  or  pxc > frame_size[0] - visible_thres:
                    if id_assign is True:        
                        # ident = self.prev_state[i]['id'][index]
                        # self.avail_ids.append(ident)
                        
                        # pf_tmp = copy.deepcopy(self.pf_objs)
                        # for pf_indx in range(len(self.pf_objs)):
                        #     if self.pf_objs[pf_indx].id == ident:
                        #         pf_tmp.remove(self.pf_objs[pf_indx])
                        # self.pf_objs = list()
                        # self.pf_objs = pf_tmp
                        pass
                    else:
                        continue
                elif pyc < visible_thres  or  pyc > frame_size[1] - visible_thres:
                    if id_assign is True:
                        # ident = self.prev_state[i]['id'][index]
                        # self.avail_ids.append(ident)
                        
                        # pf_tmp = copy.deepcopy(self.pf_objs)
                        # for pf_indx in range(len(self.pf_objs)):
                        #     if self.pf_objs[pf_indx].id == ident:
                        #         pf_tmp.remove(self.pf_objs[pf_indx])
                        # self.pf_objs = list()
                        # self.pf_objs = pf_tmp
                        pass
                    else:
                        continue
                else:
                    try:
                        current_states[dn].append(dp_bbox)
                        ident = self.prev_state[i]['id'][index]
                        current_states['id'].append(ident)

                        current_vels.append([0, 0])

                        # Use the ID of the particle filter with the prev bbox in order to later delete it
                        prev_unused_indices.append(ident)

                    except:
                        pass

            # if len(dists_mtx) != 0  and  csl == psl:
            #     cur_new = np.where(dists_mtx[:, 0] != 1000)
            #     cur_new = cur_new[0]
            # else
            #     cur_new = range(0, len(cur_state[i][dn]))
            # unused_indices = np.unique(unused_indices)
            if id_assign is True:
                for cur_new in cur_unused:
                    try:
                        ident = self.avail_ids.pop()
                    except:
                        ident = -1
                    current_states[dn].append(cur_new)
                    current_states['id'].append(ident)

                    current_vels.append([0, 0])
            

            
            # self.ids[i][dn] = ids
        else:
            for j, dc_bbox in enumerate(cur_state[i][dn]):
                if dc_bbox is None  or  len(dc_bbox) == 0:
                    continue
                else:
                    current_states[dn].append(dc_bbox)
                    current_vels.append([0, 0])
                    if id_assign is True:
                        try:
                            # self._reset_avail_ids()
                            ident = self.avail_ids.pop()
                        except:
                            ident = -1
                        current_states['id'].append(ident)

        return current_states, current_vels, prev_unused_indices
 

   


    def correlate_bboxes_from_sources(self, db, tb, dv, tv, pair=0):
       
        utils = Utils()
        p = pair
        alg_name = self.track_list[p][0]  #Tragic but I will live with it in order to finish my thesis.

        
        if len(db) != len(dv):
            ValueError('[ERROR] correlate_bboxes - length of bboxes diff from length of velocities. Exiting...')
            
        if len(tb) != len(tv):
            ValueError('[ERROR] correlate_bboxes - length of bboxes diff from length of velocities. Exiting...')
        
        if type(tb) == np.ndarray:
            tb = tb.tolist()
        
        # Length of the detector boxes and the tracker boxes

        dists_mtx = np.array([])
        
   
        try:
            dl = len(db)
            db = utils.remove_Nones(db)
            db_unused = copy.deepcopy(db)
            
            tl = len(tb)
            tb = utils.remove_Nones(tb)
            tb_unused = copy.deepcopy(tb)
    
        except:
            dl = 0
            db = []
            dv = []

            tl = 0
            tb = []
            tv = []
        
        #The ordered lists that the function will return
        if dl != 0:
            otb = [None for _ in range(len(db))]
            otv = [None for _ in range(len(dv))]
        else:
            otb = []
            otv = []

        if dl == 0 :
            return tb, tv
        
        elif dl != 0  and  tl != 0:          
            for di, dbbox in enumerate(db):
                if dbbox is None  or  len(dbbox) == 0:
                    continue
                dxc = dbbox[0] + dbbox[2]/2
                dyc = dbbox[1] + dbbox[3]/2
                
                dist_tmp = list()
                
                for ti, tbbox in enumerate(tb):
                    if tbbox is None  or  len(tbbox) == 0:
                        continue
                    txc = tbbox[0] + tbbox[2]/2
                    tyc = tbbox[1] + tbbox[3]/2

                    centers_dist = np.sqrt((dxc - txc)**2 + (dyc - tyc)**2)
                    dist_tmp.append(centers_dist)

                dist_arr = np.asarray(dist_tmp)

                if di == 0:
                    dists_mtx = dist_arr
                elif len(dists_mtx) == 0:
                    continue
                else:
                    dists_mtx = np.vstack((dists_mtx, dist_arr))    
                # dist_list.append(dist_tmp)

            # Find the minimum element of the array and remove(or make 1000) the column in which it was found
            if len(dists_mtx) != 0:
                if len(dists_mtx.shape) > 1:
                    rows, cols = dists_mtx.shape
                else:
                    rows = dists_mtx.shape
                    cols = 1
                    dists_mtx = np.array([dists_mtx])
            else:
                rows = cols = 0
            #deleted_cols = 0
            while cols > 0  and  np.min(dists_mtx) <= self.max_dist_radius:
                
                min_coords = np.where(dists_mtx == np.min(dists_mtx))
                        
                det_index = min_coords[0][0] 
                track_index = min_coords[1][0] #+ deleted_cols  #Add the number of the deleted cols to keep the indices correct only if the
                                            # concept is to delete the rows, cols. Otherwise, it is of no use
                
                row = det_index
                col = track_index

                dbbox = db[det_index]               
                tbbox = tb[track_index]
            
                #Create the lists with the unused previous and current bboxes
                #They are used for the improvement of the code
                # db_unused.remove(dbbox)
                # tb_unused.remove(tbbox)                
                
                try:
                    otb[det_index] = tbbox
                    otv[det_index] = tv[track_index]
                except:
                    print 'ERRRRRRRRRRRRRRRRRRRRRRRRrrrrrrrrrrrrrrRRRRRRRRRRRRRRRRRRRRRRRRRRRR'
                    print otb
                    print otv
                    print det_index
                    print tv
                    print track_index
                # ids[det_index] = ident

                # dists_mtx = np.delete(dists_mtx, col, axis=1)   # Delete Column
                dists_mtx[:, col] = 1000
                dists_mtx[row, :] = 1000    # Assign a large number to the elements of the row in order to not be selected
                # deleted_cols += 1


        return otb, otv





    def tracking_evaluation(self, cur_state, frame_size, visible_thres=10, mode='all'):
        '''
        This function implements the evaluation of the results of the trackers and the detectors at a single frame. 
        To be more accurate, the model makes use of the knowledge of the previous and pre-previous states, thus 
        determining the velocity of each bbox. The velocity of each bbox is determined by attempting to find the
        bbox from the pre-previous state at the previous with the use of a constant (not the COSMOLOGICAL one, chill)
        that we assume is the maximum speed of an obj that can develop in consecutive frames. For example, if we 
        assume that an obj can move at consecutive frames max 30 pixels radius, then we can narrow the search field.

        However, this is not all. This function implements another crucial action. By determinig correctly the
        velocities of the bboxes, an estimation of the position of each current bbox can be made, sth that 
        makes the algorithm more robust to occlusion. If sth like this works, I will be thrilled! 
        
        e.g. list of dicts e.g. [{'haar':[[], []], 'id': [1,2]}, {'HOG':[[]], 'id': [4]}]
        '''
        # cur_state: list of dicts, each one for a different iteration in track_list
        # self.prev_state: likewise
        cur_vel = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        # unordered_cur_states = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        # unordered_cur_vel = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]
        i = -1
        
        current_states = list()
        current_vels = [{dn: [], tn: []} for dn,_,tn,_ in self.track_list]

        tmp_states_dict = dict() #list of dicts e.g. [{'haar':[[], []], 'id': [1,2]}, {'HOG':[[]], 'id': [4]}]
        det_tmp_vels = list()   #Simple list of lists
        tr_tmp_states = dict()
        tr_tmp_vels = list()
        
        for dn,_,tn,_ in self.track_list:
            i += 1
            #Return the position, id and the velocities of the bboxes
            det_nms_states = list()
            det_tmp_states, det_tmp_vels, prev_unused_indices = self.cur_state_evaluation(cur_state, cur_vel, dn, frame_size, i)

            #TODO Use NMS Suppression for further improvement            
            det_nms_states = non_max_suppression_fast(det_tmp_states[dn], 0.3)
            det_nms_vels = list()
            ids = list()

            for j, nms_box in enumerate(det_nms_states):
                nms_index = det_tmp_states[dn].index(nms_box)
                ids.append(det_tmp_states['id'][nms_index])
                det_nms_vels.append(det_tmp_vels[nms_index])
                
            det_tmp_states[dn] = det_nms_states
            det_tmp_states['id'] = ids
            det_tmp_vels = det_nms_vels

                
                

            tmp_states_dict.update(det_tmp_states)
            
            current_vels[i][dn] = det_tmp_vels

            if tn is not None:
                #No need to return the ID. ID assignment not necessary in trackers
                tr_tmp_states, tr_tmp_vels, prev_unused_indices = self.cur_state_evaluation(cur_state, cur_vel, tn, frame_size, i)
                
                # tmp_states_dict.update(tr_tmp_states)

                #INSERT ONLY THE BBOXES, NOT THE IDs
                otb, otv = self.correlate_bboxes_from_sources(det_tmp_states[dn], tr_tmp_states[tn], det_tmp_vels, tr_tmp_vels)
                
                tmp_states_dict.update({tn: otb})

                current_states.append(tmp_states_dict)
                current_vels[i][tn] = otv

            else:
                #TODO In case you don't use any tracker
                pass

            # else:
            #     tb = None
            #     tv = None
            #     cur_state[i][tn] = tb
            #     cur_vel[i][tn] = tv
            #     unordered_cur_states[i][tn] = tb
            #     unordered_cur_vel[i][tn] = tv
        return current_states, current_vels, prev_unused_indices


    def track_frame_sc (self, frame, frame_size, max_frames_reached=False, display=False):
        '''
        This function performs detection and tracking at a frame using a detector, a tracker
        and a particle filter. Everything that takes place here is used for implementing
        the single camera subproject of Project INSIGHT.
        '''
        
        utils = Utils()

        #General Definitions of Variables for easier handling
        max_vel = self.max_dist_radius 
        move_conf = self.pf_params[0]
        acc_error = self.pf_params[1]
        resample_method = self.pf_params[2]
        N = self.pf_params[3]
        w_thres = self.pf_params[4]


        det_frame_bboxes = dict()
        track_frame_bboxes = dict()
        pf_frame_centers = list()

        self.frame_counter += 1

        #Old time classic, nothing special. Just receive the results from the images       
        i = -1
        # det_dict = dict()
        # track_dict = dict()

        current_states = list() #This Variable contains every bbox from every algorithm from both detectors and trackers that were used
        
        for det_name, det_obj, tracker_name, tracker_obj in self.track_list:
            i += 1
            cur_state_dict = dict()
            det_dict = dict()
            track_dict = dict()
            if (self.trackers_frame_init[i][1] == -1  or  self.trackers_bboxes_num[i][1] == -1)     \
                and (tracker_obj is not None):        #Tracker without bboxes to track but not None Tracker
                # print self.trackers_frame_init
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                det_bboxes = det_dict[det_name]

                if len(det_bboxes) > 0:
                    tracker_obj.tracker_add(frame, det_bboxes)      #det_dict[det_name] returns the bboxes of the detector with that name
                    temp_cntr = self.frame_counter
                    self.trackers_frame_init[i][1] = temp_cntr
                    self.track_list[i][3] = tracker_obj
                    self.trackers_bboxes_num[i][1] = len(det_bboxes)
                    track_dict = {tracker_name: det_bboxes}
                    
                else:
                    track_dict = {tracker_name: []}
                    self.trackers_bboxes_num[i][1] = -1     # Trigger a reset in order to redetect

                # track_frame_bboxes.update(track_dict)       #The init bboxes will be the same as the detector bboxes    
                
            elif tracker_obj is None:
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                

                # track_frame_bboxes.append(None)
            else:
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)     
           
                _, track_bboxes = tracker_obj.tracker_update(frame)
                tracker_bboxes = utils.remove_Nones(track_bboxes)
                # print 'Tracker bboxes {}'.format(track_bboxes)
                track_dict = {tracker_name: tracker_bboxes}
                # print ("Tracker_bboxes = ")
                # print track_bboxes
                # print ('\nWithout Nones = ')
                # print(tracker_bboxes)
                # print('\n\n\n')
        
            det_frame_bboxes.update(det_dict)
            track_frame_bboxes.update(track_dict)    

            id_dict = {'id': [-1 for _ in range(len(det_dict[det_name]))]}

            cur_state_dict.update(det_dict)
            cur_state_dict.update(track_dict)
            cur_state_dict.update(id_dict)

            current_states.append(cur_state_dict)

            # if len(current_states[i][det_name]) > 3:
            #     xxx = 0

            
        # if self.frame_counter == 0:
        #     xxxxx = 0
        
        #HERE I WILL IMPLEMENT THE SPEED CALCULATIONS FOR EVERY DETECTOR/TRACKER    
        #The cur_states & cur_vels represent the whole system. They are the lists
        #in which everything is stored. For every detector, tracker, each bbox, id,velocity
        #TODO IMPORTANT TO KNOW FOR DEBUGGING
        cur_states, cur_vels, prev_unused_indices = self.tracking_evaluation(current_states, frame_size)

        
        # if len(self.pf_objs) > 3:
        #     jgks=0

        #current_states is a list of every dn,tn,etc for each pair
        #I want to use the detector boxes only as input to the filter 
        
        #TODO FILL THIS AREA WITH CODE THAT INCLUDES THE PARTICLE FILTER
        #TODO IMPORTANT TASK AHEAD 
        #TODO HERE LIES AN AREA THAT HAS BEEN COMPLETELY MASSACREDED BY A SUPER VILLAN

        # Create new Particle Filters for the new people that the application has detected
        # and assign to them new IDs.
        dbs = list()
        
        pf_dbs = {}
        pf_vels = []
        
        i=-1
        for dn, do, tn, to in self.track_list:
            i +=1

            pf_dbs = {dn: [], 'id': []}

            dbs = copy.deepcopy(current_states[i][dn])
            # dbs = copy.deepcopy(det_frame_bboxes)

            # pf_dbs = {dn: [], 'id': []}
            # pf_vels = []
            for j, cbox in enumerate(cur_states[i][dn]):
                for k, dbox in enumerate(dbs):
                    if cbox == dbox:
                        pf_dbs[dn].append(cbox)
                        pf_dbs['id'].append(cur_states[i]['id'][j])

                        try:
                            if len(cur_vels[i][tn][j]) == 2:
                                pf_vels.append(cur_vels[i][tn][j])
                            else:
                                pf_vels.append([0,0])
                        except:
                            pf_vels.append([0,0])

        pf_dbs_len = len(pf_dbs['id'])


        # pf_objs_copy = copy.deepcopy(self.pf_objs)
        i=-1
        for dn, do, tn, to in self.track_list:
            i +=1
            
            # tmp_pf_objs = copy.deepcopy(self.pf_objs)
            # cur_state_unused_indices = list()
            for j in range(len(cur_states[i]['id'])):
                pf_flag = 0
                ident = cur_states[i]['id'][j]

                for pf_index, pf_obj in enumerate(self.pf_objs): #Check whether there is a need for the creation of new Instances 
                                                                 #of Particle Filters
                    if ident == pf_obj.id:
                        pf_flag = 1
                        break
                if pf_flag == 0:
                    # if len(self.pf_objs) < len(cur_states[i]['id']) + 3:
                    pf_obj = ParticleFilter(N, frame_size, w_thres, 1)
                
                    if ident == -1:
                        try:
                            pf_obj.id = self.avail_ids.pop()
                        except:
                            self._reset_avail_ids()
                            pf_obj.id = self.avail_ids.pop()
                        
                        cur_states[i]['id'][j] = pf_obj.id
                    else:
                        pf_obj.id = ident 
                        # pf_obj.flag_none_bbox += 1   
                    self.pf_objs.append(pf_obj)
    

            pf_objs_copy = copy.deepcopy(self.pf_objs)
            for rm_index, pf_obj in enumerate(pf_objs_copy):
                pf_flag = 0
                for j, id in enumerate(pf_dbs['id']):
                    if pf_obj.id == id:
                        pf_flag = 1
                        break
                if pf_flag == 0:
                    pf_obj.flag_none_bbox += 1

                if pf_obj.flag_none_bbox >= 2 * CHECK_FLAG_THRESHOLD:
                    try:
                        ident = self.pf_objs[rm_index].id
                        self.avail_ids.append(ident)
                        self.pf_objs[rm_index] = None
                        self.pf_objs.remove(None)
                        # self.pf_objs.remove(rm_obj)
                        # self.pf_objs.remove(pf_obj)
                        print 'FILTER REMOVED'
                    except:
                        pass
        

        cur_states, cur_vels = self.check_cur_state(cur_states, cur_vels)


        # For every Particle Filter, f.y.i. for every person that the application
        # has detected, try to track it using the Particle Filter Algorithm that
        # we have designed. Depict the result using the OpenCV Library for visualization
        i=-1 
        for tt in self.pf_objs:
            print tt.flag_none_bbox
        for dn, do, tn, to in self.track_list:
            i +=1


            display_pfs = []
            for j in range(len(cur_states[i]['id'])):
                bbox = cur_states[i][dn][j]
                ident = cur_states[i]['id'][j]
                
                pf_vel = cur_vels[i][tn][j]
                pf_index=-1
                for pf_obj in self.pf_objs:
                    pf_index += 1
                    if ident == pf_obj.id:

                        ttt = 0
                        for dbbox in dbs: # or pf_dbs[dn]
                            if bbox == dbbox:
                                ttt = 1

                        if ttt == 0:    
                            self.pf_objs[pf_index].flag_none_bbox += 1
                            # if deleted == False:
                                # cur_
                        pf_bbox_center = self.pf_objs[pf_index].bbox_track(bbox, pf_vel, move_conf, max_vel, acc_error, resample_method)
                        pf_frame_centers.append(pf_bbox_center)
                        
                        display_pfs.append(pf_index)

                        if pf_bbox_center[0] >= 0  and  pf_bbox_center[1] >= 0:
                            cv2.circle(frame, (int(pf_bbox_center[0]), int(pf_bbox_center[1])), 6, (0,0,0), -1)                       
                            text = '{}'.format(ident)        #Write the name of the detector or tracker
                            cv2.putText(frame, text, (int(pf_bbox_center[0]), int(pf_bbox_center[1] - 30)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 3)
                            pf_obj.draw_particles(frame, self.pf_colors[ident])
                            break


            for pf_index, pf_obj in enumerate(self.pf_objs):
                ident = pf_obj.id

                d = 0
                for display_pf in display_pfs:
                    if pf_index == display_pf:
                        d=1
                
                if d == 0:
                    bbox = []
                    pf_vel = [0,0]
                    pf_bbox_center = self.pf_objs[pf_index].bbox_track(bbox, pf_vel, move_conf, max_vel, acc_error, resample_method)

                    if pf_bbox_center[0] >= 0  and  pf_bbox_center[1] >= 0:
                        cv2.circle(frame, (int(pf_bbox_center[0]), int(pf_bbox_center[1])), 6, (0,0,0), -1)                       
                        text = '{}'.format(ident)        #Write the name of the detector or tracker
                        cv2.putText(frame, text, (int(pf_bbox_center[0]), int(pf_bbox_center[1] - 30)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 3)
                        pf_obj.draw_particles(frame, self.pf_colors[ident])



            # pf_objs_copy = copy.deepcopy(self.pf_objs)
            # for rm_index, pf_obj in enumerate(pf_objs_copy):
            #     pf_flag = 0
            #     for j, id in enumerate(cur_states[i]['id']):
            #         if pf_obj.id == id:
            #             pf_flag = 1
            #             break
            #     if pf_flag == 0:
            #         self.pf_objs[rm_index].flag_none_bbox += 1






        self._save_prev_state(cur_states)  # Save current state to use in the next frame as previous state
        self._save_prev_vel(cur_vels)
        
        # IMPLEMENTATION OF THE PARTICLE FILTER FOR A SINGLE CAMERA. FOR EVERY PAIR
        # DETECTOR - TRACKER, 1 PARTICLE FILTER WILL BE CREATED, SINCE WE NEED TO
        # ACQUIRE INFORMATION FROM 2 DIFFERENT SOURCES IN ORDER TO MAKE THE SYSTEM MORE ROBUST

        # if display is True:
        for det_name, bboxes in det_frame_bboxes.iteritems():
            for bbox in bboxes:
                bbox = [np.int(x) for x in bbox]
                #               img, (    x  ,    y   ), (   x     +    w    , (   y    +    h   )),
                #               color                  , thickness
                cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0] + bbox[2]), (bbox[1] + bbox[3])), \
                                self.colors[det_name], 2)
                # cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), \
                #               self.colors[det_name], 2)
                text = '{}'.format(det_name)        #Write the name of the detector or tracker
                cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[det_name], 2)

        for tracker_name, bboxes in track_frame_bboxes.iteritems():
            for bbox in bboxes:
                bbox = [np.int(x) for x in bbox]
                # print 'new bbox {}'.format(bbox)
                #               img, (  x    ,    y   ), (  x   +   w    , (    y   +   h  )), 
                #               color,             thickness
                cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0]+bbox[2]), (bbox[1]+bbox[3])), \
                            self.colors[tracker_name], 2)
                text = '{}'.format(tracker_name)        #Write the name of the detector or tracker
                cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[tracker_name], 2)





        reset_log = self.track_reset_dummy(frame, det_frame_bboxes, track_frame_bboxes, max_frames_reached)

        # if display is True:  
        text = '{}'.format(self.frame_counter)        #Write the name of the detector or tracker
        cv2.putText(frame, text, (20,30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0), 3)

        if display is True:           
            cv2.imshow('Tracking by Detectors and OpenCV Trackers', frame)
            cv2.waitKey(1)


        return det_frame_bboxes, track_frame_bboxes, pf_frame_centers, reset_log, frame


    def track_video_sc_pf(self, video_input, frame_size, iou_threshold, gt_bboxes=None, 
                          skip_frames_num=0, max_frames_reached=False, display=False, output=True):
        '''
        Track a video. It can be used in a loop if the evaluation of a dataset is considered advisable.
        
        @param video_input: the video that's to be tracked. Use full PATH. If a video from a webcam 
                            is going to be tracked used integers like 0,1, etc
        
        @param frame_size: the size of each frame
        
        @param iou_threshold: the accuracy error below which the tracking is considered acceptable
        
        @param gt_bboxes: The ground truth bounding boxes. If the video comes from webcam or just 
                          want to evaluate a video optically, insert the default None argument
        
        @param skip_frames_num: a uint. Determines the number of frames to skip to improve speed of application.
                                e.g. If skip_frames_num=3 only 1 frame per 4 will be examined
        
        @param max_frames_reached: bool. If True, an additional check will be done inside tracker_reset()
        
        @return None
        '''
        vc = cv2.VideoCapture(video_input)      # Create obj to use with OpenCV
        utils = Utils()          

        writer = None
        save_video = list()

        self.frame_counter = -1
        self.trackers_frame_init = [[self.track_list[i][2] ,-1] for i in range(len(self.track_list))]
        self.trackers_bboxes_num = [[self.track_list[i][0] ,-1] for i in range(len(self.track_list))]

        self._reset_prev_state()
        self._reset_avail_ids()
        self.pf_objs = []

        if gt_bboxes is not None:
            dataset_eval = DatasetEvaluation()  
            # metrics = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'Mean_dist', 'True_Positives']
        if skip_frames_num !=0:
            gt_index_bboxes = list()
        if type(video_input) == int:
            print ('Video Streaming from Camera  ' + str(video_input))
        else:
            # Find the name of the video without the path and the encoding
            a = video_input.rfind('/')
            b = video_input.rfind('.')
            
            if a == b == -1:
                ValueError('Not a proper name for video. Probably forgot the encoding!!! Exiting...')
            elif a == -1:
                video_input = video_input[0:b]
            else:
                video_input = video_input[a+1: b]       #a+1 since at index a there is the character we want to avoid
            print('video:  ' + str(video_input))
            
        det_video_bboxes = list()
        tracker_video_bboxes = list()
        pf_video_centers = list()
        reset_log_files = list()


        frame_count = -1
        skip = True        #It's used for the check of the skip_num_frames
        while True:
            if skip == False:
                break 
            ok, frame = vc.read()
            if ok is False:
                break

            frame_count +=1
            
            if skip_frames_num != 0  and  frame_count == 0:
                gt_index_bboxes.append(frame_count)
            
            # NECESSARY SINCE THE CAVIAR DATASET FORGOT 1 FRAME. MIGHT BE USEFUL FOR OTHER IDIOTS IN THE FUTURE
            elif frame_count >= len(gt_bboxes):
                break
            elif skip_frames_num != 0  and  frame_count != 0  and  (frame_count % (skip_frames_num+1) != 0):
                gt_index_bboxes.append(frame_count)
            
            #Skip a number of frames to improve speed
            elif  frame_count != 0  and  skip_frames_num!=0  and  frame_count % (skip_frames_num+1) == 0:
                for _ in range(skip_frames_num - 1):   #It's necessary to subtract 1 because the first frame to skip has already been cancelled
                    skip, frame = vc.read()
                    if skip is False:
                        break
                        # print 'SKIP PROBLEM SUCKS 1262'
                    frame_count += 1
                continue
            
            det_frame_bboxes, tracker_frame_bboxes, pf_frame_centers, reset_log, frame = self.track_frame(frame, frame_size, max_frames_reached, display=display)
            save_video.append(frame)

            if display is True  and  (cv2.waitKey(1) & 0xFF == 27):  # Esc pressed
                vc.release()
                cv2.destroyAllWindows()
                sys.exit('ESC Key pressed. Exiting...')
                
            # Expand the lists with the bboxes from each detector, tracker at each frame
            det_video_bboxes.append(det_frame_bboxes)
            tracker_video_bboxes.append(tracker_frame_bboxes)
            pf_video_centers.append(pf_frame_centers)
            
            # print len(tracker_video_bboxes)
            # print len(det_video_bboxes), len(tracker_video_bboxes)

            if reset_log.count(True) > 0:
                if self.frame_counter == reset_log.count(True) == 1:
                    continue
                else:
                    reset_log_files.append(reset_log)
        

        if skip_frames_num != 0:
            gt_bboxes_skip_frames  = list()
            # print gt_index_bboxes, len(gt_bboxes)
            for index in gt_index_bboxes:
                gt_bboxes_skip_frames.append(gt_bboxes[index])
            gt_bboxes = gt_bboxes_skip_frames

        gt_sum = 0
        for row in gt_bboxes:
            gt_sum += len(row)
        print gt_sum

        for det_name, _, tracker_name, _ in self.track_list:
            # det_title = True
            # track_title = True
            #Evaluation of the detectors
            det_eval = list()
            for det_frame in det_video_bboxes:
                # if len(det_frame[det_name]) == 0:
                #     det_eval.append([])
                # else:
                #     det_eval.append(det_frame[det_name])
                det_eval.append(det_frame[det_name])
            
            if gt_bboxes is not None:
                # det_total_metrics.append(det_video_metrics)
                # det_total_dists.append(det_video_dists)
                # print 'gt_bboxes in video {}, detected {}'.format(len(gt_bboxes), len(det_eval))
                if len(det_eval) != 0:
                    # print len(gt_bboxes), len(det_eval)
                    #TODO
                    #det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    det_video_metrics, det_video_dists = dataset_eval.eval_video(gt_bboxes, det_eval, iou_threshold, frame_size)
                    # print det_title 
                    # if det_title is True:
                    utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                        # det_title = False
                    # else:
                    #     utils.csv_write(CSV_DATA_PATH + det_name + '_metrics' + CSV_END, 'a', det_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + det_name + '_dists' + CSV_END, 'a', det_video_dists, [])
            else:
                pass

            #Evaluation of the trackers
            if tracker_name is not None:
                tracker_eval = list()
                for tracker_frame in tracker_video_bboxes:
                    tracker_eval.append(tracker_frame[tracker_name])

                if gt_bboxes is not None:
                    # tracker_total_metrics.append(tracker_video_metrics)
                    # tracker_total_dists.append(tracker_total_dists)
                    # print 'gt_bboxes in video {}, tracked {}'.format(len(gt_bboxes), len(tracker_eval))
                    #TODO
                    #tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    tracker_video_metrics, tracker_video_dists = dataset_eval.eval_video(gt_bboxes, tracker_eval, iou_threshold, frame_size)
                    
                    # if track_title == True:
                    utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                        # track_title = False
                    # else:
                        # utils.csv_write(CSV_DATA_PATH + tracker_name + '_metrics' + CSV_END, 'a', tracker_video_metrics, [])
                    utils.csv_write(CSV_DATA_PATH + tracker_name + '_dists' + CSV_END, 'a', tracker_video_dists, [])
                else:
                    pass

        # Since I use only 1 type of PF and not 2,3 different and thus, there is no need for a dictionary
        # the use of a for loop is redundant
        pf_video_dists_eval = list()
        pf_video_metrics = list()
        pf_mean_dist = list()
        if len(self.pf_params) != 0:#  and  output is True:
            
            pf_metrics = 'TP, FP, FN'
            for index, pf_frame in enumerate(pf_video_centers):
                # pf_tmp_dists = list()
                pf_frame_dists, pf_frame_metrics = dataset_eval.correlate_data(gt_bboxes[index], pf_frame, 40)
            
                # pf_tmp_dists.append(pf_frame_dists)
                pf_video_metrics.append(pf_frame_metrics)

                # if len(pf_frame_dists) > 1:
                for dist in pf_frame_dists:
                    # pf_tmp_dists.append(dist)
                    pf_mean_dist.append(dist)
                    pf_video_dists_eval.append(dist)
                
            # A list with the mean value of the dists and the number of the TPs
            pf_save_data = [np.mean(pf_mean_dist), len(pf_mean_dist)]
            # pf_mean_dist = np.mean(pf_video_dists)
            metrics_header = ['Accuracy', 'Precision', 'Recall', 'F_Measure', 'TP', 'FP', 'FN']

            pf_metrics = dataset_eval.eval_metrics(pf_video_metrics)


            pf_N = self.pf_params[3]
            pf_w = self.pf_params[4]
            pf_resample = self.pf_params[2]

            utils.csv_write(CSV_DATA_PATH + 'pf_N_' + str(pf_N) + '_w_' + str(pf_w) + '_' +     \
                            str(pf_resample) + '_metrics' + CSV_END, 'a', pf_metrics, [])

            utils.csv_write(CSV_DATA_PATH + 'pf_N_' + str(pf_N) + '_w_' + str(pf_w) + '_' +     \
                            str(pf_resample) + '_dists' + CSV_END, 'a', pf_video_dists_eval, [])


            utils.csv_write(CSV_DATA_PATH + 'pf_N_' + str(pf_N) + '_w_' + str(pf_w) + '_' +     \
                            str(pf_resample) + '_mean' + CSV_END, 'a', pf_save_data, [])

            # utils.draw_scatter_dists(np.asarray(pf_video_dists_eval),'pf_dists_N_' + str(pf_N) + '_w_' + str(pf_w) + '_' +     \
            #                 str(pf_resample), 'frame', 'pf_dists', CSV_DATA_PATH, True)  


            if output is True:
                pf_output_video_name = CSV_DATA_PATH + video_input + 'pf_N_' + str(pf_N) + '_w_' + str(pf_w) + '_' + str(pf_resample) + '.avi'
            # check if the video writer is None
            else: 
                pf_output_video_name = None
            if writer is None and output is True:
                # initialize our video writer
                frame = save_video[0]
                fourcc = cv2.VideoWriter_fourcc(*"MJPG")
                writer = cv2.VideoWriter(pf_output_video_name, fourcc, 30, (frame.shape[1], frame.shape[0]), True)
                # write the output frame to disk
            if writer is not None  and  pf_output_video_name is not None:
                for frame in save_video:
                    writer.write(frame)


        # if len(reset_log_files) > 0:
        #     utils.csv_write(CSV_DATA_PATH + 'reset_log_' + 'video_' + video_input + CSV_END, 'a', reset_log_files, self.tracker_names)    
        
        if writer is not None:
            writer.release()
        # if display is True:
        #     vc.release()
        #     cv2.destroyAllWindows()
        
        return None









    def track_frame_mc (self, frame):
        '''
        This function performs detection and tracking at a frame using a detector, a tracker
        and a particle filter. Everything that takes place here is used for implementing
        the single camera subproject of Project INSIGHT.
        '''
        display = self.display
        max_frames_reached = self.track_max_frames
        
        height, width = frame.shape[:2]
        frame_size = (width, height) 

        utils = Utils()

        #General Definitions of Variables for easier handling
        # max_vel = self.max_dist_radius 
        # move_conf = self.pf_params[0]
        # acc_error = self.pf_params[1]
        # resample_method = self.pf_params[2]
        # N = self.pf_params[3]
        # w_thres = self.pf_params[4]


        det_frame_bboxes = dict()
        track_frame_bboxes = dict()
        # pf_frame_centers = list()

        self.frame_counter += 1

        #Old time classic, nothing special. Just receive the results from the images       
        i = -1
        # det_dict = dict()
        # track_dict = dict()

        current_states = list() #This Variable contains every bbox from every algorithm from both detectors and trackers that were used
        
        for det_name, det_obj, tracker_name, tracker_obj in self.track_list:
            i += 1
            cur_state_dict = dict()
            det_dict = dict()
            track_dict = dict()
            if (self.trackers_frame_init[i][1] == -1  or  self.trackers_bboxes_num[i][1] == -1)     \
                and (tracker_obj is not None):        #Tracker without bboxes to track but not None Tracker
                # print self.trackers_frame_init
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                
                det_bboxes = det_dict[det_name]

                if len(det_bboxes) > 0:
                    tracker_obj.tracker_add(frame, det_bboxes)      #det_dict[det_name] returns the bboxes of the detector with that name
                    temp_cntr = self.frame_counter
                    self.trackers_frame_init[i][1] = temp_cntr
                    self.track_list[i][3] = tracker_obj
                    self.trackers_bboxes_num[i][1] = len(det_bboxes)
                    track_dict = {tracker_name: det_bboxes}
                    
                else:
                    track_dict = {tracker_name: []}
                    self.trackers_bboxes_num[i][1] = -1     # Trigger a reset in order to redetect

                # track_frame_bboxes.update(track_dict)       #The init bboxes will be the same as the detector bboxes    
                
            elif tracker_obj is None:
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)                

                # track_frame_bboxes.append(None)
            else:
                det_dict = self.det_frame_wrapper(frame, frame_size, det_name, det_obj)     
           
                _, track_bboxes = tracker_obj.tracker_update(frame)
                tracker_bboxes = utils.remove_Nones(track_bboxes)
                # print 'Tracker bboxes {}'.format(track_bboxes)
                track_dict = {tracker_name: tracker_bboxes}
                # print ("Tracker_bboxes = ")
                # print track_bboxes
                # print ('\nWithout Nones = ')
                # print(tracker_bboxes)
                # print('\n\n\n')
        
            det_frame_bboxes.update(det_dict)
            track_frame_bboxes.update(track_dict)    

            id_dict = {'id': [-1 for _ in range(len(det_dict[det_name]))]}

            cur_state_dict.update(det_dict)
            cur_state_dict.update(track_dict)
            cur_state_dict.update(id_dict)

            current_states.append(cur_state_dict)

            # if len(current_states[i][det_name]) > 3:
            #     xxx = 0

            
        # if self.frame_counter == 0:
        #     xxxxx = 0
        
        #HERE I WILL IMPLEMENT THE SPEED CALCULATIONS FOR EVERY DETECTOR/TRACKER    
        #The cur_states & cur_vels represent the whole system. They are the lists
        #in which everything is stored. For every detector, tracker, each bbox, id,velocity
        #TODO IMPORTANT TO KNOW FOR DEBUGGING
        
        cur_states, cur_vels, prev_unused_indices = self.tracking_evaluation(current_states, frame_size)

        cur_states, cur_vels = self.check_cur_state(cur_states, cur_vels)


        self._save_prev_state(cur_states)  # Save current state to use in the next frame as previous state
        self._save_prev_vel(cur_vels)
        
        # IMPLEMENTATION OF THE PARTICLE FILTER FOR A SINGLE CAMERA. FOR EVERY PAIR
        # DETECTOR - TRACKER, 1 PARTICLE FILTER WILL BE CREATED, SINCE WE NEED TO
        # ACQUIRE INFORMATION FROM 2 DIFFERENT SOURCES IN ORDER TO MAKE THE SYSTEM MORE ROBUST

        # if display is True:
        for det_name, bboxes in det_frame_bboxes.iteritems():
            for bbox in bboxes:
                bbox = [np.int(x) for x in bbox]
                #               img, (    x  ,    y   ), (   x     +    w    , (   y    +    h   )),
                #               color                  , thickness
                cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0] + bbox[2]), (bbox[1] + bbox[3])), \
                                self.colors[det_name], 2)
                # cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), \
                #               self.colors[det_name], 2)
                text = '{}'.format(det_name)        #Write the name of the detector or tracker
                cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[det_name], 2)

        for tracker_name, bboxes in track_frame_bboxes.iteritems():
            for bbox in bboxes:
                bbox = [np.int(x) for x in bbox]
                # print 'new bbox {}'.format(bbox)
                #               img, (  x    ,    y   ), (  x   +   w    , (    y   +   h  )), 
                #               color,             thickness
                cv2.rectangle(frame, (bbox[0], bbox[1]), ((bbox[0]+bbox[2]), (bbox[1]+bbox[3])), \
                            self.colors[tracker_name], 2)
                text = '{}'.format(tracker_name)        #Write the name of the detector or tracker
                cv2.putText(frame, text, (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[tracker_name], 2)





        self.track_reset_dummy(frame, det_frame_bboxes, track_frame_bboxes, max_frames_reached)

        # if display is True:  
        text = '{}'.format(self.frame_counter)        #Write the name of the detector or tracker
        cv2.putText(frame, text, (20,30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0), 3)

        if display is True:           
            cv2.imshow('Tracking by Detectors and OpenCV Trackers', frame)
            cv2.waitKey(1)


        # return det_frame_bboxes, track_frame_bboxes, frame

        # Return the first arguments only, minimum change in order to avoid Butterfly Effect
        #TODO TODO FIX IT IN FUTURE
        return cur_states[0], cur_vels[0], frame









    def check_cur_state(self, cur_state, cur_vel):
        
        i = -1
        for dn, _, tn, _ in self.track_list:
            i += 1
            
            keep_flags = []

            keep_indices_check = []
            keep_indices_cur = []
            
            remove_states_indices = []
            
            for j, cbox in enumerate(cur_state[i][dn]):

                for k, pbox in enumerate(self.check_current_state):

                    if cbox == pbox:
                        self.check_current_state_flags[k] += 1

                        keep_flags.append(self.check_current_state_flags[k])

                        keep_indices_check.append(k)
                        keep_indices_cur.append(j)

                        if self.check_current_state_flags[k] >= CHECK_FLAG_THRESHOLD:
                            remove_states_indices.append(j)

                        break


            self.check_current_state = copy.deepcopy(cur_state[i][dn])
            self.check_current_state_flags = [0 for _ in range(len(cur_state[i][dn]))]

            for index in range(len(keep_flags)):
                self.check_current_state_flags[keep_indices_cur[index]] = keep_flags[index]


            for rm_index in remove_states_indices:
                # if rm_index == 3:
                #     x = 1
                try:
                    cur_state[i][dn][rm_index] = None
                    cur_state[i][dn].remove(None)

                    cur_state[i]['id'][rm_index] = None
                    cur_state[i]['id'].remove(None)

                    cur_state[i][tn][rm_index] = [-100, -100]
                    cur_state[i][tn].remove([-100, -100])

                    cur_vel[i][tn][rm_index] = [-100, -100]
                    cur_vel[i][tn].remove([-100, -100])
                
                except:
                    pass

        return cur_state, cur_vel




if __name__ == "__main__":
    pass
    # counter = 0
    # vc = cv2.VideoCapture(0)
    # while vc.isOpened():
    #     ret, frame = vc.read()
        
    #     if ret == True:
    #         counter += 1
    #         if counter % 3 == 0:
    #             cv2.imshow('frames', frame)
    
    #         if cv2.waitKey(1) & 0xFF == ord("q"):
    #             break    

    # vc.release()
    # cv2.destroyAllWindows()   