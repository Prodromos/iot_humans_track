#!/usr/bin/env python

''' The file contains a class that will be used for the Subscription of
    some nodes to the video input classes. 

    If we want to use Raspberry Pi or another device, the video input
    is configurable, hence, the only thing that needs to be changed
    is the Video Feed.
'''

import sys
sys.path.append(sys.path[0] + "/..")

import cv2
import time 

from mc_ros_sub import SubscribeROS
from mc_ros_pub import PublishROS
 
from mc_config import *

from iot_humans_track.msg import frame_anal, llint16, lint16
from utils import ImgConvOpenCVRos

from sensor_msgs.msg import Image
import rospy

from mc_main import mc_system_init
from tracker_manager import TrackerManager

import copy

class VideoInputAnal ():
    ''' This is the Class where the Frame Analysis for the Video Input of
        a single camera takes place. We use this class to Subscribe to
        the VideoInput topic of a Camera, whether it's a Video Stream or
        a video that we split in frames.

   #     The class inherits 2 classes, each responsible for simple
   #     creation of Publisher and Subscriber nodes and topics.

        - Whenever a new frame is appeared in the appropriate VideoInput
          topic, the callback function of this class is called.

        - Then, the frame is analysed based on the parameters that were
          provided from the user configuration scheme.

        - Finally, the data (bboxes for detector & tracker, velocities,
          the timestamp of the frame and the name of the detector & the
          tracker) are sent to another topic that was specified by the user.
    '''

    def __init__(self, 
                 sub_topic, sub_node, sub_msg_type, sub_queue_size, 
                 pub_topic, pub_node, pub_msg_type, pub_queue_size, 
                 track_mngr, det_name, tr_name):

        ''' The Init Function of the Class. There are 11 arguments plus the self arg.
            It's very important to use the class with arguments rather than using the
            other classes from their objects. The function is huge but we will handle 
            it.

            @param ROS Subscriber Values

            @param ROS Publisher Values

            @param track_mngr: the tracker manager object from the TrackerManager Class

            @param det_name: name of the detector

            @param tr_name: name of the tracker

            @param det_video_bboxes: a list that stores every bbox at each frame of for 
                the detector. It may be used for evaluation later.
        
            @param tr_video_bboxes: a list that stores every bbox at each frame of for 
                the tracker. It may be used for evaluation later.
        '''
        # super (VideoInputAnal, self).__init__ (sub_topic, sub_node, sub_msg_type, self.callback, sub_queue_size,
        #                                        pub_topic, pub_node, pub_msg_type, pub_queue_size)
        
        # Init the Subscriber and Publisher Node of the Class
        # TODO Try to use Inheritance from 2 Base Classes in order to avoid having the Objects inside here.
        self.sub = SubscribeROS(sub_topic, sub_node, sub_msg_type, self.callback, sub_queue_size)   
        self.pub = PublishROS(pub_topic, pub_node, pub_msg_type, pub_queue_size)

        self.track_mngr = track_mngr
 
        self.det_name = det_name
        self.tr_name = tr_name
        self.det_video_bboxes = list()
        self.tr_video_bboxes = list()

        self.framen = -1
        self.ros_msg = frame_anal()

    def callback(self, ros_img):
        ''' The callback function of the Subscriber ROS Node. It receives the ROS Image, converts it to OpenCV
            frame and performs the analysis of the frame using the TrackerManager Class. Finally, the 
            function publishes the results to the appropriate topic as defined by the user.

            @param ros_img: the ROS Image that is received by the topic in which the Subscriber Node is
                listening.
        '''
        timestamp = ros_img.header.stamp    #Get the timestamp of the Frame. Maybe we will use it

        frame = ImgConvOpenCVRos().conv_to_opencv (ros_img)       # Conversion of the ROS img to OpenCV img 

        cur_states, cur_vels, frame = self.track_mngr.track_frame_mc(frame)

        self.framen += 1
        print ('Frame = ' + str(self.framen))
        # cv2.imshow('Camera', frame)
        # cv2.waitKey(1)

        self.det_video_bboxes.append(cur_states[self.det_name])
        self.tr_video_bboxes.append(cur_states[self.tr_name])

        # Publish everything that we have now to the pub_topic
        ros_msg = copy.deepcopy(self.ros_msg)
        ros_msg.timestamp = timestamp
        ros_msg.dn = self.det_name
        ros_msg.tn = self.tr_name
        ros_msg.frame_width = frame.shape[1]
        ros_msg.frame_height = frame.shape[0]
        
        # print cur_states[self.det_name]

        for db in cur_states[self.det_name]:
            if db is None:
                db = [-1, -1, -1, -1]
            ros_msg.db.data.append(db)
        
        for tb in cur_states[self.tr_name]:
            if tb is None:
                tb = [-1, -1, -1, -1]
            ros_msg.tb.data.append(tb)

        for dv in cur_vels[self.det_name]:
            if dv is None:
                dv = [-1, -1, -1, -1]
            ros_msg.dv.data.append(dv)
        
        for tv in cur_vels[self.tr_name]:
            if tv is None:
                tv = [-1, -1, -1, -1]
            ros_msg.tv.data.append(tv)
            
        # print ros_msg
        self.pub.publish(ros_msg)   # Publish the msg
        # print ('Publish OK')
        # print ros_msg
        # rospy.sleep(0.2)


if __name__ == "__main__":
    # ros_msg = frame_anal()
    # ros_msg.db = llint16()
    # ros_msg.db.data.append([1,2,3])
    # ros_msg.db.data.append([1,6,3])    
    # print ros_msg

    cam = sys.argv[1]
    cam = int(cam)

    if cam >= CAMERAS_NUM:
        ValueError('[ERROR] Incorrect Name for The Camera')

    start = time.time()
    while (time.time() - start < 3.0):
        pass
    # os.chdir('/home/' + USERNAME)
    # print(os.getcwd())

    mc_track_list, detectors_params = mc_system_init(cam=cam)
    # print mc_track_list
    # print detectors_params
    
    dt_pair = CAM_DT_PAIRS[cam]
    # print dt_pair
    dn = dt_pair[0]
    tn = dt_pair[1]

    sub_topic = CAM_TOPIC_VIDEO_INPUT[cam]
    sub_node = CAM_SUBSCRIBE_NODE + str(cam)
    sub_msg_type = Image
    sub_queue_size = 500
    
    pub_topic = CAM_TOPIC_DT_FRAME_ANALYSIS[cam]
    pub_node = CAM_PUBLISH_NODE + str(cam)
    pub_msg_type = frame_anal
    pub_queue_size = 500


    tracker_mngr = TrackerManager(mc_track_list, detectors_params, PF_PARAMS, 25)  #SURE IT'S CORRECT
    tracker_mngr.max_dist_radius = 40 
    tracker_mngr.set_colors()

    via = VideoInputAnal(sub_topic, sub_node, sub_msg_type, sub_queue_size,
                         pub_topic, pub_node, pub_msg_type, pub_queue_size,
                         tracker_mngr, dn, tn)

    rospy.spin()