#!/usr/bin/env python

''' This @FILE contais the code for the Manipulation of the Input Image Streams. 
    The main function in this file is called for every different Input Method.

    The Software has been designed in advance to be as distributed as possible.
    Thus, the Input Video can either be a Video or a Live Video Stream.
'''

import sys
sys.path.append(sys.path[0] + "/..")

import cv2
import time
import rospy

from mc_ros_pub import PublishROS
from mc_config import *
from mc_datasets_eval import VIPT_2014, VIPT_2014_INDEX_DMP

from sensor_msgs.msg import Image

from utils import ImgConvOpenCVRos

import os
import getpass


USERNAME = getpass.getuser()

# VIDEO_INPUT_PATH = 'Documents/Project_INSIGHT/data/Datasets/VILP/mc_system/cam' + str(CAM)
# VIDEO_INPUT_PATH = 'Documents/Project_INSIGHT/data/Datasets/Videos/ShoppingMallPortugal/EnterExitCrossingPaths1cor.mpg'



class VideoInput (PublishROS):
    ''' The Class inherites the PublishROS Class in order to create a Node 
        with which the frames can be sent to the appropriate Topic.
    '''

    def __init__(self,
                 pub_topic, pub_node, pub_msg_type, pub_queue_size,
                 video_input, video_timestamps=None):
        super(VideoInput, self).__init__(pub_topic, pub_node, pub_msg_type, pub_queue_size)

        self.video_input = video_input
        self.video_timestamps = video_timestamps

    def publish_frames(self, skip_frames_num):
        ''' This function uses the video input Stream and publishes the
        frames to the appropriate ROS Topic. 

        @param skip_frames_num: The number of frames to skip if you
            want to lose samples but increase the speed of the system.
        '''

        vc = cv2.VideoCapture(self.video_input)
        
        # rospy.sleep(5) 
        
        frame_count = -1
        skip = True        #It's used for the check of the skip_num_frames
        while True:
            if skip == False:
                break 
            ok, frame = vc.read()
            if ok is False:
                break

            frame_count +=1
            
            #Skip a number of frames to improve speed
            if  frame_count != 0  and  skip_frames_num!=0  and  frame_count % (skip_frames_num+1) == 0:
                for _ in range(skip_frames_num - 1):   #It's necessary to subtract 1 because the first frame to skip has already been cancelled
                    skip, frame = vc.read()
                    if skip is False:
                        break
                    frame_count += 1
                continue
    
            ros_img = ImgConvOpenCVRos().conv_to_ros(frame) # Convert the Img to ROS Img in order to ROS Publish it.
            
            # Import the timestamp of the frame in case of a Dataset Video File
            if self.video_timestamps is not None:
                unix_secs = self.video_timestamps[frame_count][0]
                unix_nanosecs = self.video_timestamps[frame_count][1]
                # print unix_secs
                # print unix_nanosecs
                ros_img.header.stamp.secs = unix_secs
                ros_img.header.stamp.nsecs = unix_nanosecs

            self.publish(ros_img)
            # print frame_count
            rospy.sleep(1.1)
        vc.release()


if __name__ == "__main__":


    cam = sys.argv[1]
    cam = int(cam)

    if cam >= CAMERAS_NUM:
        ValueError('[ERROR] Incorrect Name for The Camera')
    
    start = time.time()
    while (time.time() - start < 3.0):
        pass
    
    os.chdir('/home/' + USERNAME)
    print(os.getcwd())

    pub_topic = CAM_TOPIC_VIDEO_INPUT[cam]
    pub_node = CAM_PUBLISH_NODE + str(cam)
    pub_queue_size = 500

    skip_frames_num = 0
    if VIDEO_INPUT_MODE == 0:
        video_input = 0
        video_timestamps = None 
    else:
        video_input = VIDEO_INPUT_PATH + str(cam) + VIDEO_INPUT_EXTENSION
        print video_input
        
        filepath = VIPT_2014_INDEX_DMP + str(cam) + '/'
        filename = 'index.dmp'

        vipt_2014 = VIPT_2014()

        video_timestamps = vipt_2014.read_timestamps(filepath, filename)
        
    vi = VideoInput (pub_topic, pub_node, Image, pub_queue_size, video_input, video_timestamps)
    vi.publish_frames(skip_frames_num)



