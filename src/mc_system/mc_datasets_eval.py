#!/usr/bin/env python
import os
import getpass

import tf
import rospy

import time 
import numpy as np

from sensor_msgs.msg import PointCloud, CameraInfo

import sys
sys.path.append(sys.path[0] + "/..")

from mc_ros_pub import PublishROS
from mc_config import CAMERAS_NUM, CAM_PUBLISH_NODE, CAMS_2_USE

from image_geometry import PinholeCameraModel

VIPT_2014_INDEX_DMP = 'Documents/Project_INSIGHT/data/Datasets/VIPT_2014_Dataset/test_set/cam'

FILEPATH_GT = 'Documents/Project_INSIGHT/data/Datasets/VIPT_2014_Dataset/'
FILENAME_GT = 'VIPT_2014_GROUNDTRUTH.ref'

class VIPT_2014():
    '''
    The Class for the Evaluation and General Use of the VIPT 2014 Dataset
    provided by: https://tev-static.fbk.eu/DATABASES/VIPT.html
    '''
    def __init__(self):
        
        self.cams_num = CAMS_2_USE

        self.gt_timestamps = []
        self.gt_timestamps_sec = []
        self.gt_centers = []

        # -2.1230866,  -0.02594921,  2.26935216
        # self.cams_euler = [ (-2.1230866,  0.02594921,  -2.26935216),
        #                     (-2.33567327,  -0.7190028, 0.53039854),
        #                     ( -2.1729094,  0.7076869,  -2.7135797),
        #                     ( -2.2822040,   0.6454525, 2.6581176)
        #                   ]

        self.cams_euler =   [[-2.16359771,  0.03076845 ,-2.42807726],
                            [-2.1230866 , -0.02594921,  2.26935216],
                            [-2.01639091,  0.00540461, -0.81607488],
                            [-2.12467621,  0.00616788,  0.77912667]]

        self.cams_translation = [ (0.15, 5.78, 2.41946),
                                  (4.59, 5.99, 2.54995),
                                  (0.20, 0.28, 2.53317),
                                  (4.36, 0.29, 2.51547)
                                ]
        self.cam_info = [CameraInfo() for _ in range(4)]
        self.cam_models = [PinholeCameraModel() for _ in range(4)]
        # self.cam_models[0].projectPixelTo3dRay
        self.ground_width = 4.75
        self.ground_height = 5.92
        
        self.cam_width = 1024
        self.cam_height = 768
        
        resized_ratio_x = 512.0 / self.cam_width
        resized_ratio_y = 368.0 / self.cam_height
        
        self.fov = []
        
        self.real_width = self.cam_width * resized_ratio_x
        self.real_height = self.cam_height * resized_ratio_y

        self.cams_set_params(resized_ratio_x, resized_ratio_y)
        self.read_gt_bboxes(FILEPATH_GT, FILENAME_GT)
        self.gt_timestamps_sec = [x[0] for x in self.gt_timestamps]

        
    def cams_set_params(self, resized_ratio_x=1.0, resized_ratio_y=1.0):
        '''
        Set the parameters for the cameras

        @param resized_ratio_x: resized_width / 1024 (default width)
        @param resized_ratio_y: resized_height / 768 (default_height)
        '''
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        cam_distortion_mode = 'plumb_bob'

        cam0_D = [-0.258295, 0.0573538, 0, 0, 0]

        cam0_K = [557.27 * resized_ratio_x,       0                             , 505.582 * resized_ratio_x,
                            0             , 557.27 * 0.999916 * resized_ratio_y , 403.742 * resized_ratio_y,
                            0             ,       0                             ,    1   ]

        cam0_R = [  [-0.755708,  -0.346367,    0.55582],
                    [-0.654186,   0.439103,   -0.615816],
                    [-0.0307636,   -0.828986, -0.558422]]

        cam0_P = [557.27 * resized_ratio_x,          0       , 505.582 * resized_ratio_x, 0.15,
                     0  , 557.27 * 0.999916 * resized_ratio_y, 403.742 * resized_ratio_y, 5.78,
                     0  ,   0              ,    1   ,  0]  

        self.cam_info[0].width = self.real_width
        self.cam_info[0].height = self.real_height
        self.cam_info[0].distortion_model = cam_distortion_mode
        self.cam_info[0].D = cam0_D
        self.cam_info[0].K = cam0_K
        self.cam_info[0].R = cam0_R
        self.cam_info[0].P = cam0_P

        self.cam_models[0].fromCameraInfo(self.cam_info[0])


        cam_w = self.cam_width * resized_ratio_x
        cam_h = self.cam_height * resized_ratio_y
        
        fx_0 = cam0_K[0]
        fy_0 = cam0_K[4]

        # Result in Radians
        fovx_0 = 2 * np.arctan(cam_w / (2 * fx_0))
        fovy_0 = 2 * np.arctan(cam_h / (2 * fy_0))

        self.fov.append([fovx_0, fovy_0])


        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        cam1_D = [-0.266752, 0.0618027, 0, 0, 0]

        cam1_K = [635.302 * resized_ratio_x,                     0                , 561.471 * resized_ratio_x,
                               0           ,  635.302 * 0.999355 * resized_ratio_y, 403.957 * resized_ratio_y,
                               0           ,                     0                ,           1]

        cam1_R = [  [-0.642896,  0.387548,  -0.660675],
                    [0.765514,   0.354316,  -0.537073],
                    [0.0259463, -0.851039,  -0.524462]]

        cam1_P = [635.302 * resized_ratio_x,   0   , 561.471 * resized_ratio_x, 4.59,
                     0  , 635.302 * 0.999355 * resized_ratio_y, 403.957 * resized_ratio_y, 5.99,
                     0  ,   0   ,    1   ,  0]  

        self.cam_info[1].width = self.real_width
        self.cam_info[1].height = self.real_height
        self.cam_info[1].distortion_model = cam_distortion_mode
        self.cam_info[1].D = cam1_D
        self.cam_info[1].K = cam1_K
        self.cam_info[1].R = cam1_R
        self.cam_info[1].P = cam1_P

        self.cam_models[1].fromCameraInfo(self.cam_info[1])


        fx_1 = cam1_K[0]
        fy_1 = cam1_K[4]

        # Result in Radians
        fovx_1 = 2 * np.arctan(cam_w / (2 * fx_1))
        fovy_1 = 2 * np.arctan(cam_h / (2 * fy_1))

        self.fov.append([fovx_1, fovy_1])

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        cam2_D = [-0.259997, 0.0580502, 0, 0, 0]

        cam2_K = [   570.376 * resized_ratio_x  ,          0                          , 506.18 * resized_ratio_x,
                                  0             , 570.376 * 0.998422 * resized_ratio_y, 377.477 * resized_ratio_y,
                                  0             ,          0                          ,    1]

        cam2_R = [  [0.685076,    -0.317305,  0.655735],
                    [-0.728452,   -0.291716,  0.619887],
                    [-0.00540459, -0.902341, -0.430988]]

        cam2_P = [570.376* resized_ratio_x,   0   , 506.18* resized_ratio_x,  0.2,
                     0  , 570.376 * 0.998422* resized_ratio_y, 377.477 * resized_ratio_y, 0.28,
                     0  ,   0   ,    1   ,  0]  

        self.cam_info[2].width = self.real_width
        self.cam_info[2].height = self.real_height
        self.cam_info[2].distortion_model = cam_distortion_mode
        self.cam_info[2].D = cam2_D
        self.cam_info[2].K = cam2_K
        self.cam_info[2].R = cam2_R
        self.cam_info[2].P = cam2_P

        self.cam_models[2].fromCameraInfo(self.cam_info[2])


        fx_2 = cam2_K[0]
        fy_2 = cam2_K[4]

        # Result in Radians
        fovx_2 = 2 * np.arctan(cam_w / (2 * fx_2))
        fovy_2 = 2 * np.arctan(cam_h / (2 * fy_2))

        self.fov.append([fovx_2, fovy_2])

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

        cam3_D = [-0.26013, 0.0565506, 0, 0, 0]

        cam3_K = [560.641 * resized_ratio_x,    0  , 504.244 * resized_ratio_x,
                     0  , 560.641 * 0.998414 * resized_ratio_y, 385.837 * resized_ratio_y,
                     0  ,   0   ,    1]

        cam3_R = [  [0.711514,     0.365859,  -0.599912],
                    [0.702645,    -0.377943,   0.602868],
                    [-0.00616784, -0.850474,  -0.525981]]

        cam3_P = [560.641 * resized_ratio_x,   0   , 504.244 * resized_ratio_x, 4.36,
                     0  , 560.641 * 0.998414 * resized_ratio_y, 385.837 * resized_ratio_y, 0.29,
                     0  ,   0   ,    1   ,  0]  

        self.cam_info[3].width = self.real_width
        self.cam_info[3].height = self.real_height
        self.cam_info[3].distortion_model = cam_distortion_mode
        self.cam_info[3].D = cam3_D
        self.cam_info[3].K = cam3_K
        self.cam_info[3].R = cam3_R
        self.cam_info[3].P = cam3_P

        self.cam_models[3].fromCameraInfo(self.cam_info[3])


        fx_3 = cam3_K[0]
        fy_3 = cam3_K[4]

        # Result in Radians
        fovx_3 = 2 * np.arctan(cam_w / (2 * fx_3))
        fovy_3 = 2 * np.arctan(cam_h / (2 * fy_3))

        self.fov.append([fovx_3, fovy_3])


    def read_timestamps (self, filepath, filename):
        '''
        Basically Read the <<<index.dmp>>> File for each camera 
        in order to insert the correct timestamps to the frames for evaluation

        @param filepath: path of the file

        @param filename: name of the file

        @return a list that contains the unix timestamp for every frame
        '''
        unix_timestamps = list()

        with open(filepath + filename, 'r') as fp:
            for cnt, line in enumerate(fp):
                tmp_list = list()
                tmp_line = line.split()
                tmp_list.append(int(tmp_line[1]))
                tmp_list.append(int(tmp_line[2]))
                unix_timestamps.append(tmp_list)

        return unix_timestamps


    def read_gt_bboxes(self, filepath, filename):
        '''
        Read the ground truth bboxes from the Dataset.

        @param filepath: path of the file

        @param filename: name of the file

        @return a list that contains the ground truth bboxes for every frame.
        '''
        timestamps = list()
        gt_bboxes = list()

        with open(filepath + filename, 'r') as fp:
            for cnt, line in enumerate(fp):  
                tmp_gt_list = list()              
                tmp_line = line.split()

                stamp = tmp_line[0].split('.')
                stamp = [int(x) for x in stamp]
                timestamps.append(stamp)

                line_size = len(tmp_line)
                i = 0
                while True:
                    if i == line_size:
                        break

                    if tmp_line[i].find('XXX') != -1:
                        gt_x = tmp_line[i+1]
                        gt_x = float(gt_x)
                        gt_x = gt_x / 1000.0
                        gt_x = np.round(gt_x, 3)
                        
                        gt_y = tmp_line[i+2]
                        gt_y = float(gt_y)
                        gt_y = gt_y / 1000.0
                        gt_y = np.round(gt_y, 3)
                        tmp_gt_list.append([gt_x, gt_y])             
                        
                        i += 2
                    i += 1

                gt_bboxes.append(tmp_gt_list)
        
        self.gt_timestamps = timestamps
        self.gt_centers = gt_bboxes
        return timestamps, gt_bboxes


if __name__ == "__main__":
    username = getpass.getuser()
    os.chdir('/home/' + username)
    print os.getcwd() 

    cam = sys.argv[1]
    cam = int(cam)

    if cam >= CAMERAS_NUM:
        ValueError('[ERROR] Incorrect Name for The Camera')
    
    
    vipt_2014 = VIPT_2014()
    
    timestamps, gt_centers = vipt_2014.read_gt_bboxes(FILEPATH_GT, FILENAME_GT)
    
    vipt_2014.gt_timestamps = timestamps
    vipt_2014.gt_centers = gt_centers
    vipt_2014.gt_timestamps_sec = [x[0] for x in timestamps]
    cam_translation = vipt_2014.cams_translation[cam]
    
    cam_euler = vipt_2014.cams_euler[cam]

    cam_quaternion_tmp = tf.transformations.quaternion_from_euler(cam_euler[0], cam_euler[1], cam_euler[2])
    cam_quaternion = (cam_quaternion_tmp[0], cam_quaternion_tmp[1], cam_quaternion_tmp[2], cam_quaternion_tmp[3])
    
    # print cam_quaternion_tmp
    # print cam_quaternion

    pub_node = CAM_PUBLISH_NODE + str(cam)
    rospy.init_node (pub_node, anonymous=True)
    pub = PublishROS('pub_topic', pub_node, PointCloud, 100)
    
    br = tf.TransformBroadcaster()

    start = time.time()
    while time.time() - start < 2.0:
    # while True:
        br.sendTransform(cam_translation, 
                        cam_quaternion, 
                        rospy.Time.now(), 
                        'cam' + str(cam), 
                        "world")
        rospy.sleep(0.2)
    print ('CAM' + str(cam) + ' HAS BROADCASTED!')

    rospy.spin()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

    # filepath = 'Documents/test_set/cam0/'
    # filename = 'index.dmp'

    # filepath_gt = 'Documents/Project_INSIGHT/data/Datasets/VIPT_2014_Dataset/'
    # filename_gt = 'VIPT_2014_GROUNDTRUTH.ref'
    

    # unix_secs = vipt_2014.read_timestamps(filepath, filename)
    # timestamps, gt_bboxes = vipt_2014.read_gt_bboxes(filepath_gt, filename_gt)

    # total_sum = 0
    # for i in range(len(gt_bboxes)):
    #     part_sum = len(gt_bboxes[i])
    #     total_sum += part_sum
    # print total_sum
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 