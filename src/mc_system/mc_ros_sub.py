#!/usr/bin/env python

import sys
sys.path.append(sys.path[0] + "/..")

import rospy
import roslib
from sensor_msgs.msg import Image
from utils import Utils, ImgConvOpenCVRos

import cv2

import argparse

SUBSCRIBER_NAME = "/cams/rgb/sub_img_stream_rpi3"     #SUBSCRIBER name for the topic
TOPIC = "/TOPIC1"
PATH_FOLDER = 'Documents/Project_INSIGHT/data/camera_1/'
IMG_FORMAT = '.jpg'


class SubscribeROS ():
    """
    This class is used for subscribing to the publish topic with the 
    image stream from the RPi3s

    The functions of this class are:
        __init__ : using the inheritance we manage to handle easily the conversion 
                   between ROS and OpenCV images.
        
        callback : after the subscription to the image topic, this is the function
                   that is going to be executed.
    """

    def __init__ (self, sub_topic, sub_node, sub_msg_type, sub_callback, sub_queue_size):
        # super (SubscribeROS, self).__init__ ()
        rospy.init_node (sub_node, anonymous=True)
        rospy.Subscriber (sub_topic, sub_msg_type, callback=sub_callback, queue_size=sub_queue_size)
        
  
    
def callback (ros_img):
    opencv_img = ImgConvOpenCVRos().conv_to_opencv (ros_img)       # Conversion of the ROS img to OpenCV img 
    cv2.imshow('Project INSIGHT', opencv_img)
    cv2.waitKey (1)
    # full_path = Utils().set_img_name (PATH_FOLDER, IMG_FORMAT)  # Return the FULL PATH NAME to store the images from the RPis
    # cv2.imwrite (full_path, opencv_img)
        




def main ():
    ap = argparse.ArgumentParser()
    ap.add_argument("-r", "--haar", type=str, required=False,
        help="name of the tracker assigned to haar detector")
    ap.add_argument("-g", "--hog", type=str, required=False,
        help="name of the tracker assigned to hog detector")
    args = ap.parse_args()
    print args
    subscriber = SubscribeROS (TOPIC, 'suber', Image, callback, 50)
    rospy.spin()

if __name__ == '__main__':
    main ()
