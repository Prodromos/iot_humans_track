#!/usr/bin/env python

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# #     # #     # #     # #     # #     # #     # #     # #     # # 

# --------------------   Particle Filter 3D    ---------------------

# #     # #     # #     # #     # #     # #     # #     # #     # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import sys
sys.path.append(sys.path[0] + "/..")

import cv2
import numpy as np

import rospy
import tf
import geometry_msgs.msg 
from visualization_msgs.msg import Marker, MarkerArray
import time

from mc_ros_sub import SubscribeROS
from mc_ros_pub import PublishROS
from mc_config import *

from mc_datasets_eval import VIPT_2014

from iot_humans_track.msg import frame_anal, llint16, lint16

from utils import Utils
from config import *
from tracker_manager import TrackerManager
from particle_filters.particle_filter import ParticleFilter

import copy
import os
import getpass
# global pf_3d_objs = []

class ParticleFilter3D():
    '''
    This class is used for receiving the data from every camera
    input. Then, with the use of TF ROS, a Particle Filter, rviz ROS
    etc the Particle Filters are created and updated based on every camera.
    '''
    
    # Static Variable. Accessed by every Object (Subscriber)
    pf_3d_objs = list()
    
    def __init__(self,
                 sub_topic, sub_node, sub_msg_type, sub_queue_size, 
                 pub_topic, pub_node, pub_msg_type, pub_queue_size,
                 trans, rot, cam, dataset=None):
        # Cam name. E.G. 'cam0'
        self.cam = cam

        # Number of the camera, e.g. 0
        self.cam_num = int(cam[-1])

        # ROS Subscriber and Publisher
        self.sub = SubscribeROS(sub_topic, sub_node, sub_msg_type, self.callback, sub_queue_size)   
        self.pub = PublishROS (pub_topic, pub_node, pub_msg_type, pub_queue_size)

        # Translation and Rotation in Quaternions for the specific Camera that will be used.
        self.translation = trans
        self.rotation = rot

        # The 3D particle filter objects for each camera
        self.cam_pf_3d_objs = list()

        # The general data input required for each camera. 
        # TODO CHANGE the name dataset to something more generic. We will use the same software 
        # for real time processing also. 
        self.dataset = dataset

        self.metrics = {'tp': 0, 'fp': 0, 'fn': 0, 'dists': []}
        self.db_bboxes_found = 0
        self.processed_frames = 0

        self.gt_set = 0

    def get_world_coords_from_ros(self):
        world_points_cams_all = []

        for i in range(CAMS_2_USE):
            try:
                cam_points = []
                for j in range(15):
                    try:
                        if rospy.has_param('/cam' + str(self.cam_num) + '/world_coords' + str(j)): 
                            cam_point = rospy.get_param('/cam' + str(i) + '/world_coords' + str(j))
                            if len(cam_point) == 2:
                                cam_points.append(cam_point)
                    except:
                        pass
                # if len(cam_points) > 0:
                world_points_cams_all.append(cam_points)            
    
            except:
                pass

        return world_points_cams_all



    def set_cam_world_points_to_ros(self, world_points):
        # print rospy.get_param_names()

        try:
            for i in range(15):
                try:
                    if rospy.has_param('/cam' + str(self.cam_num) + '/world_coords' + str(i)): 
                        rospy.delete_param('/cam' + str(self.cam_num) + '/world_coords' + str(i))
                except:
                    print 'EXCEPTION FOR ' + str(i)
                    

            # print type(world_points)
            # print type(world_points[0][0])
            
            for i in range(len(world_points)):
                # print world_points[i]
                if len(world_points[i]) != 2:
                    continue

                try:
                    s = '/cam' + str(self.cam_num) + '/world_coords' + str(i)
                    tmp = list()
                    tmp.append(float(world_points[i][0]))
                    tmp.append(float(world_points[i][1]))
                    if tmp[0] > -4.0  and  tmp[1] > -4.0:
                        rospy.set_param(s, tmp)
                except:
                    print rospy.get_param_names()
                    print 'Failed AGAIN'
        except: 
            print 'Cannot Set World Points to Parameter Server for Cam ' + str(self.cam_num)
            print rospy.get_param_names()
            print '\n\n'
            print world_points
            print '\n\n'


    def reset_ros_server(self):
        print rospy.get_param_names()

        try:
            for i in range(15):
                try:
                    if rospy.has_param('/cam' + str(self.cam_num) + '/world_coords' + str(i)): 
                        rospy.delete_param('/cam' + str(i) + '/world_coords' + str(i))
                except:
                    pass
        except:
            print 'Cannot Reset Wworld Points to Parameter Server for Cam ' + str(self.cam_num)



    def correlate_world_points_from_all_cams(self, world_points_total, error=0.5):
        
        
        # if len(world_points_total) < CAMS_2_USE:
        #     diff = len(world_points_total) - CAMS_2_USE
        #     for i in range(diff):
        #         world_points_total.append([])

        # Number of humans detected in each camera
        cams_length = []
        filtered_points = []
        for i,inv_cam in enumerate(world_points_total):
            tmp = filter(None, inv_cam)
            filtered_points.append(tmp)
            cams_length.append(len(filtered_points[i]))          

        world_points_total_copy = copy.deepcopy(filtered_points)
        
        # print 'List with Length for each Camera     |     ' + str(cams_length)
        print 'World Points in Total l-l-l          |     ' + str(world_points_total)
        print 'World Points in Filterd              |     ' + str(filtered_points)
        # Find the index of the camera that the maximum number of points detected
        max_points_index = cams_length.index(max(cams_length))

        # The list that represents the points that are maximum for that camera
        max_points_cam = copy.deepcopy(filtered_points[max_points_index])
        print 'max points mam            |   ' + str(max_points_cam)

        world_points_total_copy.remove(world_points_total_copy[max_points_index])
        print 'world_points_total_copy   |   ' + str(world_points_total_copy)

        final_points = [[None] for _ in range(len(max_points_cam))]
        sorted_points = [[None] for _ in range(len(max_points_cam))]

        total_sorted_points = [sorted_points for _ in range(len(world_points_total_copy))]
        try:
            total_sorted_points[0] = max_points_cam
            total_sorted_points.append(sorted_points)
        except:
            pass
        
        # print 'total sorted points       |' + str(total_sorted_points)
        rest_points = []
        for j, world_point in enumerate(max_points_cam):
            # if world_point[0] < -0.5  and world_point[1] < -0.5  and
            #             world_point[0] > 7  and world_point[1] > 7:
            #     continue
            if len(world_point) != 2:
                continue
            for i in range(len(world_points_total_copy)):
                dists = []
                # print world_points_total_copy[i]
                if len(world_points_total_copy[i]) != 2:
                    continue
                for point in world_points_total_copy[i]:
                    if len(point) != 2:
                        continue
                    dist = np.linalg.norm(np.asarray(world_point) - np.asarray(point))
                    dists.append(dist)

                min_dist_index = dists.index(min(dists))
                if dists[min_dist_index] <= error:
                    total_sorted_points[i][j] = world_points_total_copy[i][min_dist_index]
                else:
                    if world_point[0] > -3  and world_point[1] > -3  and  world_point[0] < 8  and world_point[1] < 8:
                        rest_points.append([world_point[0], world_point[1]])
        
        # total_sorted_points.append(rest_points)
        
        print 'total sorted points       |' + str(total_sorted_points)
        np_arr = np.asarray(total_sorted_points)
        # print 'Numpy array to use      |     ' + str(np_arr)
        for i in range(len(final_points)):
            points = np_arr[:, i]
            # points = filter(None, points)
            # tmp = np_arr[:,i]
            print 'Numpy array points with i,:      |     ' + str(points)
            # print 'tmp to list               |     ' + str(tmp.tolist())    
            Nones = 0
            x=0
            y=0
            x_depths = 0
            y_depths = 0

            for point in points:
                if point is None:
                    Nones += 1
                    continue
                try:
                    x = point[0]
                    y = point[1]
                except:
                    if len(point) != 2:
                        continue
                        # x = point[0][0]
                        # y = point[0][1]
                x_depths += x
                y_depths += y
            
            try:
                x_depths = x_depths / (len(final_points) - Nones)
                y_depths = y_depths / (len(final_points) - Nones)
            except:
                x_depths = x
                y_depths = y

            final_points[i] = [float(x_depths), float(y_depths)]

        if len(rest_points) > 0:
            print '\n\nRest Points to Final Debug\n' + str(rest_points)
            for point in rest_points:
                final_points.append(point)
                
        print '\n\nFinal Points - Normally same    ' + str(final_points)
        
        return final_points



    def filter_points(self, world_points):
        '''
        The world points are a list of lists that represents the points that have been detected
        from the cameras
        '''
        rm_indexes = []
        for i, point in enumerate(world_points):
            if point[0] < -1.0  or  point[0] > 7.0 or  point[1] < -1.0  or  point[1] > 7.0:
                rm_indexes.append(i)
        
        for rm_index in rm_indexes:
            world_points.remove(world_points[rm_index])
        
        return world_points


    def correlate_data(self, input1, input2, acc_error=30, mode='cor_centers'):
        '''
        Make the best correlation between two lists of data
        
        May use bboxes (4 parameters) or centers (2 parameters)

        CRUCIAL for CORRECT results!!! Input1 is the Ground Truth
        and the Input2 is the data we want to evaluate!!!

        @param input1: the ground truth. It may be only the center of the bbox or the whole bbox. 
                        The code operated beautifully!.
        
        @param input2: the data for evaluation. It may be only the center of the bbox or the whole bbox. 
                        The code operated beautifully!.
        
        @param acc_error: the accuracy tolerable error. Usually small, it is measured in pixels

        @param mode: If the mode == 'cor_centers' ---> correlate centers the code returns the center dists and the metrics
                        Else, it returns the ordered list of the evaluated bboxes correlated by index to the ground truth bboxes
                        
        @return
           -   The pf bboxes relative to the detector bboxes
           -   The detector bboxes that were not used
           -   The indices of the pf bboxes that were not used

        track_index == pf_index!!! IMPORTANT! 
        '''
        utils = Utils()

        if type(input1) == np.ndarray:
            input1 = input1.tolist()
        if type(input2) == np.ndarray:
            input2 = input2.tolist()
        
        dists_mtx = np.array([])
    
        try:
            inp1_len = len(input1)
            input1 = utils.remove_Nones(input1)
            input1_unused = copy.deepcopy(input1)
            
            inp2_len = len(input2)
            input2 = utils.remove_Nones(input2)
            input2_unused = copy.deepcopy(input2)

            inp2_unused_indexes = [i for i in range(inp2_len)]

        except:
            inp1_len = 0
            input1 = []
            
            inp2_len = 0
            input2 = []
            

        dist_centers_eval = list()
        tp = 0

        #The ordered lists that the function will return
        ord_input2 = [None for _ in range(len(input1))]

        if inp1_len == 0  and  inp2_len == 0:
            if mode != 'eval':
                return [], [], []
            else:
                return [0,0,0], [], []

        elif inp1_len == 0 and  inp2_len !=0:
            if mode != 'eval':
                return [], [], inp2_unused_indexes
            else:
                return [0, inp2_len, 0], [], []

        elif inp1_len != 0  and  inp2_len == 0:
            if mode != 'eval':
                return ord_input2, input1_unused, []
            else:
                return [0, 0, inp1_len], [], []

        elif inp1_len != 0  and  inp2_len != 0:          
            for di, input1box in enumerate(input1):
                if input1box is None  or  len(input1box) == 0:
                    continue
                if len(input1box) == 2:
                    dxc = input1box[0] 
                    dyc = input1box[1] 
                elif len(input1box) == 4:
                    dxc = input1box[0] + input1box[2]/2
                    dyc = input1box[1] + input1box[3]/2
                else:
                    ValueError('Wrong Argument at Dataset Evaluation Correlation Function')
                                
                dist_tmp = list()
                
                for ti, input2box in enumerate(input2):
                    if input2box is None  or  len(input2box) == 0:
                        continue
                    if len(input2box) == 2:
                        txc = input2box[0]
                        tyc = input2box[1]
                    elif len(input2box) == 4:
                        txc = input2box[0] + input2box[2]/2
                        tyc = input2box[1] + input2box[3]/2
                    else:
                        ValueError('Wrong Argument at Dataset Evaluation Correlation Function')

                    centers_dist = np.sqrt((dxc - txc)**2 + (dyc - tyc)**2)
                    dist_tmp.append(centers_dist)

                dist_arr = np.asarray(dist_tmp)

                if di == 0:
                    dists_mtx = dist_arr
                elif len(dists_mtx) == 0:
                    continue
                else:
                    dists_mtx = np.vstack((dists_mtx, dist_arr))    
                # dist_list.append(dist_tmp)

            # Find the minimum element of the array and remove(or make 1000) the column in which it was found
            if len(dists_mtx) != 0:
                if len(dists_mtx.shape) > 1:
                    rows, cols = dists_mtx.shape
                else:
                    rows = dists_mtx.shape
                    cols = 1
                    dists_mtx = np.array([dists_mtx])
            else:
                rows = cols = 0
            #deleted_cols = 0
            while cols > 0  and  np.min(dists_mtx) <= acc_error:
                
                min_coords = np.where(dists_mtx == np.min(dists_mtx))
                        
                det_index = min_coords[0][0] 
                track_index = min_coords[1][0]  
                
                row = det_index
                col = track_index

                input1box = input1[det_index]               
                input2box = input2[track_index]
            
                if mode == 'eval':
                    if len(input1box) == 2:
                        inp1_xc = input1box[0] 
                        inp1_yc = input1box[1]
                    elif len(input1box) == 4:
                        inp1_xc = input1box[0] + input1box[2] / 2 
                        inp1_yc = input1box[1] + input1box[3] / 2

                    if len(input2box) == 2:
                        inp2_xc = input2box[0] 
                        inp2_yc = input2box[1]
                    elif len(input2box) == 4:
                        inp2_xc = input2box[0] + input2box[2] / 2 
                        inp2_yc = input2box[1] + input2box[3] / 2
                    
                    dxc = abs(inp1_xc - inp2_xc)
                    dyc = abs(inp1_yc - inp2_yc)
                    dist_centers_eval.append(np.sqrt((dxc)**2 + (dyc)**2))

                # else:
                #     tp += 1

                #Create the lists with the unused previous and current bboxes
                #They are used for the improvement of the code
                try:
                    input1_unused.remove(input1box)
                    input2_unused.remove(input2box)                
                    inp2_unused_indexes.remove(track_index)
                except:
                    pass        
                
                ord_input2[det_index] = track_index
                
                # dists_mtx = np.delete(dists_mtx, col, axis=1)   # Delete Column
                dists_mtx[:, col] = 1000
                dists_mtx[row, :] = 1000    # Assign a large number to the elements of the row in order to not be selected
                # deleted_cols += 1


        fp = len(input2_unused)
        if len(input1) - len(input2) > 0:
            fn = len(input1) - len(input2) 
        else:
            fn = 0

        if mode == 'eval':
            tp = len(dist_centers_eval)
            return [tp, fp, fn], dist_centers_eval, []
        # else:

        # Return 
        #   -   The pf indices of the bboxes that were used relative to the detector bboxes
        #   -   The detector bboxes that were not used
        #   -   The indices of the pf bboxes that were not used
        return ord_input2, input1_unused, inp2_unused_indexes
            
    
    def set_transform(self, header_id, child_id):
        '''
        Set the transform from the camera frame to the world frame or anyone else.
        For this thesis we will transform only to world frame
        '''
        try:
            m = geometry_msgs.msg.TransformStamped()
            m.header.frame_id = header_id
            m.child_frame_id = child_id
            
            m.transform.translation.x = self.translation[0]
            m.transform.translation.y = self.translation[1]
            m.transform.translation.z = self.translation[2]
            
            m.transform.rotation.x = self.rotation[0]
            m.transform.rotation.y = self.rotation[1]
            m.transform.rotation.z = self.rotation[2]
            m.transform.rotation.w = self.rotation[3]
        
            transform = tf.TransformerROS(True, rospy.Duration(1.0))
            # print 'Line 91'
            transform.setTransform(m)
            # print 'Line 93'
            # print g.getFrameStrings()
            
            # pmsg = geometry_msgs.msg.PointStamped()
            # pmsg.header.frame_id = self.cam
            
            # pmsg.point.x = pf_2d.particles[0][0]
            # pmsg.point.y = pf_2d.particles[0][1]
            # pmsg.point.z = 3
            
            # t_point = g.transformPoint('world', pmsg)
            
            # print ('pmsg\n')
            # print pmsg
            # print ('\n\nt_point\n')
            # print t_point
            return transform

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print 'ERROR Exception Found'
            return []


    def get_bboxes_and_centers_at_camX(self, db, camX):
        '''
        Get the bboxes of the detector from the frame coords to the world coords.
        Also, get the center of the bbox in order to estimate a better z for the 
        particle filter.

        @param db: the incoming bboxes from a detector
        @param camX: usually it will be 'cam0'
        '''

        # Number of the camera, e.g. 0,1, etc
        cam_num = self.cam_num

        points_trans_to_3d = list()
        for bbox in db:
            xtl = bbox[0]
            xbr = bbox[0] + bbox[2]

            ytl = bbox[1]
            ybr = bbox[1] + bbox[3]

            xc = bbox[0] + bbox[2] / 2
            yc = bbox[1] + bbox[3] / 2

            # The points to be transformed. We use a list for simplicity and reduction of complexity
            points_trans_to_3d.append([[xtl, ytl], [xbr, ybr], [xc, yc]])

        # The bboxes in the form of [x_top_left, y_top_left, width, height] will be stored in this list
        db_at_camX_frame = list()

        # The bbox centers [xc, yc, zc] relative to the camX frame will be stored here
        bbox_centers_at_camX_frame = list()
        
        # If the cam0 is the one that is being used for the processing there is no need for transformations
        if camX == self.cam:
            for i, points in enumerate(points_trans_to_3d):
                # xtl = points[0][0]
                # ytl = points[0][1]

                # xbr = points[1][0]
                # ybr = points[1][1]

                # Get the width and the height of the bbox in camX coords
                # width = abs(xbr - xtl)
                # height = abs(ybr - ytl)
                
                # Make the bbox in such a way to insert it to the particle filter
                # bbox = [int(xtl), int(ytl), int(width), int(height)]
                
                db_at_camX_frame.append(db[i])

                bbox_centers_at_camX_frame.append(points[2])
            
            return db_at_camX_frame, bbox_centers_at_camX_frame


        # Get the transform of the 3d coords from camera frame to the camX frame (usually cam0 as reference)
        tf_3d = self.set_transform(camX, self.cam)   
        

        for i, points in enumerate(points_trans_to_3d):
            point_tl = points[0]
            point_br = points[1]
            point_c = points[2] 
            
            # Get the rectified points from the image
            # rect_point_tl = self.dataset.cam_models[cam_num].rectifyPoint(point_tl)
            # rect_point_br = self.dataset.cam_models[cam_num].rectifyPoint(point_br)
            # rect_point_c = self.dataset.cam_models[cam_num].rectifyPoint(point_c)


            # -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   
            tl_xyz = self.get_camX_3D_coords_from_2d(point_tl, self.cam)
            br_xyz = self.get_camX_3D_coords_from_2d(point_br, self.cam)
            c_xyz = self.get_camX_3D_coords_from_2d(point_c, self.cam)
            # print tl_xyz

            # Get the 3D unit vector at the coord system of the camera from the points
            # The below points are vectors 1.0 size. Reveal only direction, not pixels
            unit_vector_3d_tl = self.dataset.cam_models[cam_num].projectPixelTo3dRay(point_tl)
            unit_vector_3d_br = self.dataset.cam_models[cam_num].projectPixelTo3dRay(point_br)
            unit_vector_3d_c = self.dataset.cam_models[cam_num].projectPixelTo3dRay(point_c)

            # print point_tl
            

            # -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   
            

            # Transform the 3D points from the camera frame to camX coordinates
            pmsg = geometry_msgs.msg.PointStamped()
            pmsg.header.frame_id = self.cam
            
            # Top Left point of bbox to world frame coords 
            pmsg.point.x = tl_xyz[0]
            pmsg.point.y = tl_xyz[1]
            pmsg.point.z = tl_xyz[2]
            # pmsg.point.z = 1.0

            # Transform the unit vector to the camX frame. The transformation to world coordinates will 
            # occur later (not in pixels, as here, but in meters S.I.)
            unit_vector_tl_camX = tf_3d.transformPoint(camX, pmsg)

            
            # Convert the vector outcome of the transformation to unit vector (don't know the result so just to be sure)
            # unit_vector_tl_camX = [unit_vector_tl_camX.point.x, unit_vector_tl_camX.point.y, unit_vector_tl_camX.point.z]
            
            unit_vector_tl_camX = [unit_vector_tl_camX.point.x , unit_vector_tl_camX.point.y, unit_vector_tl_camX.point.z]
            unit_vector_tl_camX = unit_vector_tl_camX / np.linalg.norm(unit_vector_tl_camX)
            unit_vector_tl_camX = unit_vector_tl_camX.tolist()

            unit_vector_tl_camX[2] = 1.0

            # Project the unit vector in the 2D space of the camX frame. Remember: camX = 'cam0' so int(camX[-1]) to get index 0 e.g.
            # This is in pixels
            point_tl_camX = self.dataset.cam_models[int(camX[-1])].project3dToPixel(unit_vector_tl_camX)




            # -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   

            # Transform the 3D points from the camera frame to camX coordinates
            pmsg = geometry_msgs.msg.PointStamped()
            pmsg.header.frame_id = self.cam
            
            # Top Left point of bbox to world frame coords 
            pmsg.point.x = unit_vector_3d_br[0]
            pmsg.point.y = unit_vector_3d_br[1]
            pmsg.point.z = unit_vector_3d_br[2]

            # Transform the unit vector to the camX frame. The transformation to world coordinates will 
            # occur later (not in pixels, as here, but in meters S.I.)
            unit_vector_br_camX = tf_3d.transformPoint(camX, pmsg)

            # Convert the vector outcome of the transformation to unit vector (don't know the result so just to be sure)
            unit_vector_br_camX = [unit_vector_br_camX.point.x, unit_vector_br_camX.point.y, unit_vector_br_camX.point.z]
            unit_vector_br_camX = unit_vector_br_camX / np.linalg.norm(unit_vector_br_camX)
            unit_vector_br_camX = unit_vector_br_camX.tolist()

            unit_vector_br_camX[2] = 1.0

            # Project the unit vector in the 2D space of the camX frame. Remember: camX = 'cam0' so int(camX[-1]) to get index 0 e.g.
            # This is in pixels
            point_br_camX = self.dataset.cam_models[int(camX[-1])].project3dToPixel(unit_vector_br_camX)
            

            # -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----   

            # Transform the 3D points from the camera frame to camX coordinates
            pmsg = geometry_msgs.msg.PointStamped()
            pmsg.header.frame_id = self.cam

            # Top Left point of bbox to world frame coords 
            pmsg.point.x = unit_vector_3d_c[0]
            pmsg.point.y = unit_vector_3d_c[1]
            pmsg.point.z = unit_vector_3d_c[2]

            # Transform the unit vector to the camX frame. The transformation to world coordinates will 
            # occur later (not in pixels, as here, but in meters S.I.)
            unit_vector_c_camX = tf_3d.transformPoint(camX, pmsg)

            # Convert the vector outcome of the transformation to unit vector (don't know the result so just to be sure)
            unit_vector_c_camX = [unit_vector_c_camX.point.x, unit_vector_c_camX.point.y, unit_vector_c_camX.point.z]
            unit_vector_c_camX = unit_vector_c_camX / np.linalg.norm(unit_vector_c_camX)
            unit_vector_c_camX = unit_vector_c_camX.tolist()

            unit_vector_c_camX[2] = 1.0

            # Project the unit vector in the 2D space of the camX frame. Remember: camX = 'cam0' so int(camX[-1]) to get index 0 e.g.
            # This is in pixels
            point_c_camX = self.dataset.cam_models[int(camX[-1])].project3dToPixel(unit_vector_c_camX)
            

            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
            #   - - - - - - - -         MINOR UPDATE       - - - - - - - - -
            #          We have the top_left, bottom_right, center point in pixels 
            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


            # Get the coords of the top left point in camX coords in pixels
            xtl = point_tl_camX[0]
            ytl = point_tl_camX[1]
            # ztl = point_tl_camX.point.z
            
            # Get the coords of the bottom right point in camX coords
            xbr = point_br_camX[0]
            ybr = point_br_camX[1]
            # zbr = point_br_camX.point.z
            
            # Get the width and the height of the bbox in camX coords
            width = abs(xbr - xtl)
            height = abs(ybr - ytl)
            
            # Make the bbox in such a way to insert it to the particle filter
            bbox = [int(xtl), int(ytl), int(width), int(height)]
            
            # Append the bbox of the detector at world coords
            db_at_camX_frame.append(bbox)


            # Center point of bbox to camX frame coords
            pmsg = geometry_msgs.msg.PointStamped()
            pmsg.header.frame_id = self.cam
            
            pmsg.point.x = point_c_camX[0]
            pmsg.point.y = point_c_camX[1]
            # pmsg.point.z = point_c_camX[2]

            # Get the center point of the bbox in world coords
            # point_c_world = tf_3d.transformPoint('world', pmsg)
            
            # Get the center point in world coords 
            # xc = point_c_world.point.x
            # yc = point_c_world.point.y
            # zc = point_c_world.point.z

            # Append the center point of the bbox of the detector at world coords
            # bbox_centers_at_camX_frame.append([int(xc), int(yc)])

        return db_at_camX_frame, bbox_centers_at_camX_frame


    def get_camX_3D_coords_from_2d(self, center_point, camX):
        '''
        Transform a pixel from a camera to X, Y, Z coordinates relative to the
        world frame
        '''

        # transform = self.set_transform('world', camX)
        
        # Angle between the Z world and the Z camX coordinate systems. To understand better read chapter 4 of thesis
        angle_z_world_z_camX = abs(self.dataset.cams_euler[0][0])

        # Get the camera number for the transformations
        cam_num = int(camX[-1]) 

        fov_x = self.dataset.fov[cam_num][0]
        fov_y = self.dataset.fov[cam_num][1]
        
        # Find the Human's center. (If an average human is 1.70m then the center is 0.85m)
        human_center = HUMAN_HEIGHT / 2.0

        # Find the Z distance from camX to the bounding box (in meters)
        z_camX = self.dataset.cams_translation[cam_num][2]

        # Unit vector of z axis relative to the camera coordinate systems
        unit_vector_z = np.array([0, 0, 1])
        
        unit_vector_camX = self.dataset.cam_models[cam_num].projectPixelTo3dRay(center_point)
        
        x = (center_point[0] - self.dataset.cam_models[0].cx()) / self.dataset.cam_models[0].fx()
        y = (center_point[1] - self.dataset.cam_models[0].cy()) / self.dataset.cam_models[0].fy()
        z = 1.0


        # pmsg = geometry_msgs.msg.PointStamped()
        # pmsg.header.frame_id = CAM_X
        
        # Center Point of particle filter to world frame coords 
        # pmsg.point.x = unit_vector_camX[0]
        # pmsg.point.y = unit_vector_camX[1]
        # pmsg.point.z = unit_vector_camX[2]

        # pmsg.point.x = x
        # pmsg.point.y = y
        # pmsg.point.z = z

        unit_vector_camX = np.asarray(unit_vector_camX)
        vector_mul = unit_vector_camX.dot(unit_vector_z)
        vector_mul_norm = np.linalg.norm(unit_vector_camX) * np.linalg.norm(unit_vector_z)

        # The angle is in radians
        angle_z_camX_vector_camX = np.arccos(vector_mul / vector_mul_norm)

        if unit_vector_camX[1] < 0:
            angle_z_camX_vector_camX = - angle_z_camX_vector_camX

        theta = np.radians(180) - angle_z_world_z_camX - angle_z_camX_vector_camX

        if theta < 0:
            ValueError('theta angle cannot be < 0. Find the logical error in the code')
        # theta = angle_z_camX_vector_camX
        # print theta
        # print angle_z_camX_vector_camX

        # Try to avoid the 90 degrees trap. Put a random value to escape
        if abs(np.cos(theta)) < 1e-3:
            z_total = z_camX
        else:
            z_total = z_camX / abs(np.cos(theta))

        # z after the bbox (small part)
        z_to_subtract = human_center / abs(np.cos(theta))

        # The z in meters
        z = abs(z_total - z_to_subtract)

        # Convert the pixels we use here relative to camera center
        center_point[0] = center_point[0] - self.dataset.real_width / 2.0
        center_point[0] = int(center_point[0])

        center_point[1] = center_point[1] - self.dataset.real_height / 2.0
        center_point[1] = int(center_point[1])

        phi_x = (center_point[0] / (self.dataset.real_width / 2.0)) * (fov_x / 2.0)
        phi_y = (center_point[1] / (self.dataset.real_height / 2.0)) * (fov_y / 2.0)

        if abs(np.cos(theta)) < 1e-3:
            x = self.dataset.ground_width
            y = self.dataset.ground_height
        else:
            # x = (z / np.cos(theta_z) ) * (np.cos(phi_x))
            x = (z) * np.sin(theta) * np.sin(phi_x)
            # x = abs(x)

            # y = (z / np.cos(theta_z) ) * (np.cos(phi_y))
            y = (z) * np.sin(phi_y)
            # y = abs(y)


        return [x, y, z]



    def dataset_eval(self, pf_world_centers, frame_timestamp):
        gt_tmp_stamp = frame_timestamp

        print 'timestamp ' + str(frame_timestamp)
        # print self.dataset.gt_timestamps_sec
        try:
            gt_index = self.dataset.gt_timestamps_sec.index(gt_tmp_stamp)
            print 'gt index found ' + str(gt_index) + ' at timestamp ' + str(frame_timestamp)
        except:
            gt_index = -1

        if gt_index == -1:
            return []
        
        gt_centers = self.dataset.gt_centers[gt_index]
        print 'gt centers found ' + str(gt_centers)
        print 'pf centers found ' + str(pf_world_centers)
        metrics, dists_centers, tmp = self.correlate_data(gt_centers, pf_world_centers, 1.0, 'eval')
        tp = metrics[0]
        fp = metrics[1]
        fn = metrics[2]

        self.metrics['tp'] += tp
        self.metrics['fp'] += fp
        self.metrics['fn'] += fn
        self.metrics['dists'].append(dists_centers)

        print 'metrics that were found   |   ' + str(tp) + '   |   ' + str(fp) + '   |   ' + str(fn)  
        # print 'metrics that were found   |   ' + str(tmp)


    def callback(self, frame_anal_data):
        self.processed_frames +=1

        print 'Camera   |   ' + str(self.cam_num)

        if self.translation is None  or  self.rotation is None:
            return

        utils = Utils()
        # Copy the camera pf objects in order to evaluate the new input from the camera 
       
        # pfs = self.get_pfs_from_ros()
        # rospy.sleep(0.1)
        # self.cam_pf_3d_objs = copy.deepcopy(pfs)
        # self.cam_pf_3d_objs = copy.deepcopy(self.pf_3d_objs)
        
        # for k in range(len(self.cam_pf_3d_objs)):
            # print 'Filter Derived for Camera ' + str(self.cam_num) + '  from Camera '  + str(self.cam_pf_3d_objs[k].last_cam)
            # self.cam_pf_3d_objs[k].last_cam = self.cam_num
        
        
        # --------------------- Initialisation for Particle Filter Parameters -------------
        move_conf = PF_PARAMS[0]
        acc_error = PF_PARAMS[1]
        resample_method = PF_PARAMS[2]
        N = PF_PARAMS[3]
        w = PF_PARAMS[4]
        pf_delete_thres = PF_PARAMS[5]
        max_vel = PF_PARAMS[6]
        std = PF_PARAMS[7]
        # -------------- End of Initialisation for Particle Filter Parameters -------------

        timestamp = frame_anal_data.timestamp
        dn = frame_anal_data.dn
        tn = frame_anal_data.tn
        frame_width = frame_anal_data.frame_width
        frame_height = frame_anal_data.frame_height

        # print dn
        # print tn
        # print timestamp

        db = list()
        dv = list()
        tb = list()
        tv = list()
        
        # -------------------- Initialisation for BBOXES ARRIVAL ----------------
        for dbbox in frame_anal_data.db.data:
            if dbbox == [-1, -1, -1, -1]:
                dbbox = None
            db.append(list(dbbox.data))
        for tbbox in frame_anal_data.tb.data:
            if tbbox == [-1, -1, -1, -1]:
                tbbox = None
            tb.append(list(tbbox.data))
        for dvel in frame_anal_data.dv.data:
            if dvel == [-1, -1, -1, -1]:
                dvel = None
            # elif len(dvel) == 1:
                # dvel = [0, 0]
            dv.append(list(dvel.data))
        for tvel in frame_anal_data.tv.data:
            if tvel == [-1, -1, -1, -1]:
                tvel = None
            tv.append(list(tvel.data))
        # ----------------------- End of BBOXES ARRIVAL -------------------
        
        
        # Return before any transformation takes place. There is no point to use resources
        rospy.sleep(0.1 * self.cam_num + 0.2)

        self.db_bboxes_found += len(db)
        print 'Detector boxes found   |   ' + str(len(db)) + '   for camera   |   ' + str(self.cam_num)

        # if len(db) == 0:
        #     self.set_cam_world_points_to_ros([])
        
        # if len(self.cam_pf_3d_objs) == 0  and  len(db) == 0:
        #     return

        gt = self.dataset.gt_centers

        try:
            gt_index = self.dataset.gt_timestamps_sec.index(timestamp.secs)
        except:
            gt_index = -1

        if gt_index == -1:
            return 
        
        gt_centers = self.dataset.gt_centers[gt_index]

        # db_at_camX_frame, bbox_centers_at_camX_frame = self.get_bboxes_and_centers_at_camX(db, CAM_X)
        
        # xyz_at_world_meters = list()
        # xyz_at_world_meters = self.get_camX_3D_coords_from_2d()
        # print db_at_camX_frame
        
        tf_3d = self.set_transform('world', self.cam)  

        world_centers = list()
        for box in db:
            bbox_center = [box[0] + box[2]/2.0, box[1] + box[3]/2.0]
            xyz = self.get_camX_3D_coords_from_2d(bbox_center, self.cam)

            # convert the point to the world frame (at last)
            # Transform the 3D points from the camera frame to camX coordinates
            pmsg = geometry_msgs.msg.PointStamped()
            pmsg.header.frame_id = self.cam
            
            # Center Point of particle filter to world frame coords 

            pmsg.point.x = xyz[0]
            pmsg.point.y = xyz[1]
            pmsg.point.z = xyz[2]

            # Transform the unit vector to the camX frame. The transformation to world coordinates will 
            # occur later (not in pixels, as here, but in meters S.I.)
            world_xyz_point = tf_3d.transformPoint('world', pmsg)
            x = world_xyz_point.point.x
            y = world_xyz_point.point.y
            z = world_xyz_point.point.z
            world_centers.append([x, y, z])
        

        world_c_xy = [[world_centers[i][0], world_centers[i][1]] for i in range(len(world_centers))]
        
        # Set the world centers that were detected from the camera to the ros server for each camera
        self.set_cam_world_points_to_ros(world_c_xy)
        


        #Get the coodinates of all cameras as a list of lists
        if self.cam == CAM_X:
            world_total_coords_all_cams = self.get_world_coords_from_ros()
            print world_total_coords_all_cams
            # if len(world_total_coords_all_cams) == 0:
            #     return
            print 'LINE PASSED'
          
            # world_all_cams_means = world_c_xy
            world_all_cams_means = self.correlate_world_points_from_all_cams(world_total_coords_all_cams, error=3.5)

            world_all_cams_means = self.filter_points(world_all_cams_means)


            if len(self.cam_pf_3d_objs) == 0  and  len(world_all_cams_means) != 0:
                for _ in range(len(world_all_cams_means)):
                    pf = ParticleFilter(N, (self.dataset.ground_width, self.dataset.ground_height), w, self.dataset.cams_num)
                    self.cam_pf_3d_objs.append(pf)

            # TODO  Use flags to the pfs in order to delete them later
            elif len(self.cam_pf_3d_objs) != 0  and  len(world_all_cams_means) == 0:
                pfs_copy = copy.deepcopy(self.cam_pf_3d_objs)
                for i, pf in enumerate(pfs_copy):
                    try:
                        self.cam_pf_3d_objs[i].flag_none_bbox += 1
                        if self.cam_pf_3d_objs[i].flag_none_bbox == pf_delete_thres:
                            self.cam_pf_3d_objs[i] = None
                            self.cam_pf_3d_objs.remove(None)
                    except:
                        pass

                # print '\n\n----------  Debug    2    --------\n'
                # print 'Particle Filters before check for Cam ' + str(self.cam_num) + '  ||  ' + str(len(pfs_copy))
                # for m in range(len(pfs_copy)):
                #     print 'Particle Filters Flags for    Cam ' + str(self.cam_num) + '  ||  ' + str(pfs_copy[m].flag_none_bbox)
        
                # print '\n\nParticle Filters before check for Cam ' + str(self.cam_num) + '  ||  ' + str(len(self.cam_pf_3d_objs))
                # print '\n-------- End Of Debugging 2 -------\n'


            else:
                pf_centers = list()
                for pf in self.cam_pf_3d_objs:
                    tmp_center = []
                    try:
                        tmp_center = [int(c) for c in pf.center]
                        pf_centers.append(tmp_center)
                    except:
                        pass
                pfs_num = len(pf_centers)
                
                # The below function returns the pf indices that are connected with the db as well as the unused dbboxes
                
                pfs_indices_used, world_xy_unused, pfs_indices_unused = self.correlate_data(world_all_cams_means, pf_centers)
                
                # print '\n\n----------  Debug    3   --------\n\n'
                # print 'Particle Filter Centers'
                # for m, pf in enumerate(self.cam_pf_3d_objs):
                #     print 'Partice Filter     | ' + str(m)
                #     print 'PF Center:         | ' + str(pf.center)
                #     print 'PF World Center    | ' + str(pf.world_center)
                #     print 'PF x_depths        | ' + str(pf.x_depths)
                #     print 'PF y_depths        | ' + str(pf.y_depths) + '\n\n'

                # print '\nDetector Bbboxes unused    | ' + str(world_xy_unused)
                # print '\nParticle Filters used      | ' + str(pfs_indices_used)  
                # # print pfs_indices_used
                # print '\nParticle Filters unused    | ' + str(pfs_indices_unused)
                # # print pfs_indices_unused
                # print '\n-------- End Of Debugging 3-------\n'
                
                for i in range(len(world_all_cams_means)):
                    # If the particle filter exists, then use it to track the bbox from the detector.
                
                    if pfs_indices_used[i] is not None:
                        
                        pf_index = pfs_indices_used[i]
                        
                        # Track the bbox, update xy particles and weights 
                        pf_bbox = self.cam_pf_3d_objs[pf_index].track_point_3D(world_all_cams_means[i], [0,0], move_conf, max_vel, resample_method, std)
                        # try:
                        #     self.cam_pf_3d_objs[pf_index].x_depths[self.cam_num] = pf_bbox[0]
                        #     self.cam_pf_3d_objs[pf_index].y_depths[self.cam_num] = pf_bbox[1]
                        # except:
                        #     pass            
                        # if self.cam_num == 1:
                        #     print self.cam_pf_3d_objs[pf_index].x_depths
                        #     print self.cam_pf_3d_objs[pf_index].y_depths
                        #     print pf_bbox
                        # Update the z coord (z_depth) of the particles
                        # self.cam_pf_3d_objs[pf_index].z_depths[self.cam_num] = bbox_centers_at_camX_frame[i][2]
                
                pfs_indices_unused_copy = copy.deepcopy(pfs_indices_unused)

                for unused_xy in world_xy_unused:
                    index = world_all_cams_means.index(unused_xy)   # Index to find the right velocity for this bbox. 
                    if len(pfs_indices_unused) == 0:
                        try:
                            pf = ParticleFilter(N, (self.dataset.ground_width, self.dataset.ground_height), w, self.dataset.cams_num)
                            pf_bbox = pf.track_point_3D(unused_xy, [0,0], move_conf, max_vel, resample_method, std)

                            self.cam_pf_3d_objs.append(pf)

                        except:
                            pass
                    
                    else:
                        # Select a random filter, it doesn't matter
                        try:
                            pf_index = pfs_indices_unused[0]
                            
                            pfs_indices_unused_copy.remove(pf_index)
                            
                            pf_bbox = self.cam_pf_3d_objs[pf_index].track_point_3D(world_all_cams_means[pf_index], [0,0], move_conf, max_vel, resample_method, std)
                            
                            # self.cam_pf_3d_objs[pf_index].x_depths[self.cam_num] = pf_bbox[0]
                            # self.cam_pf_3d_objs[pf_index].y_depths[self.cam_num] = pf_bbox[1]
                            
                            # if self.cam_num == 1:
                            #     print self.cam_pf_3d_objs[pf_index].x_depths
                            #     print self.cam_pf_3d_objs[pf_index].y_depths
                            #     print pf_bbox
                            # Update the z coord (z_depth) of the particles
                            # self.cam_pf_3d_objs[pf_index].z_depths[self.cam_num] = bbox_centers_at_world_frame[index][2]

                            pfs_indices_unused.remove(pf_index) # Delete this index from the list in order to come to a conclusion
                        
                        except:
                            print '[ERROR] LINE 809 mc_pf_3D.py File.'
                
                if len(pfs_indices_unused_copy) > 0:
                
                    for pf_index in pfs_indices_unused_copy:
                        try:
                            pf_bbox = self.cam_pf_3d_objs[pf_index].track_point_3D([], [0, 0], move_conf, max_vel, resample_method, std)
                        except: 
                            pass

                        try:
                            if self.cam_pf_3d_objs[pf_index].flag_none_bbox == pf_delete_thres:
                                self.cam_pf_3d_objs[pf_index] = None
                                self.cam_pf_3d_objs.remove(None)

                        except:
                            pass


            pfs_copy = copy.deepcopy(self.cam_pf_3d_objs)
            for i, pf in enumerate(pfs_copy):
                try:
                    if pf.center[0] < 0 or pf.center[1] < 0:
                        self.cam_pf_3d_objs[i].flag_none_bbox += 5
                        if self.cam_pf_3d_objs[i].flag_none_bbox == pf_delete_thres:
                            self.cam_pf_3d_objs[i] = None
                            self.cam_pf_3d_objs.remove(None)
                except:
                    pass

        # Draw the particles only from 1 camera. There is no point in ploting the particles
        # from multiple cameras simultaneously
        # for p in self.cam_pf_3d_objs:
        #     print p.particles

        if self.cam_num == 0:# or self.cam_num == 1:
            
            # pf_objs_shared = self.get_pfs_from_ros()
            # print pf_objs_shared
            pf_objs_shared = self.cam_pf_3d_objs
            # Plot the particles from every Particle Filter using the RVIZ Software in ROS Kinetic
            # The particles will be converted from pixel based to meter based and then they will be
            # plotted using the Marker Array Library for visualization. 
            marker_array = MarkerArray()

            colors = [((i+1) * (1.0 / len(pf_objs_shared))) for i in range(len(pf_objs_shared))]
            color_select = [[1,0,0], [0,1,0], [0,0,1], [1,1,0], [0,1,1], [1,1,1], [1,0,1]]
            # print colors
            total_centers = []
            for i, pf_obj in enumerate(pf_objs_shared):
                world_particles = copy.deepcopy(pf_obj.particles)
                
                pf_obj.update_xyz(0.3)
                
                x_mean = np.mean(pf_obj.particles[:,0])
                y_mean = np.mean(pf_obj.particles[:,1])
                total_center = [x_mean,y_mean]

                total_centers.append(total_center)
                # print '\n\n ---------------TOTAL CENTER-----------------'
                # print 'Total center Conglomeration of x,y depths    | ' + str(total_center)
                # print 'List of x_depths for Particle Filter ' + str(i) +  '  Cam ' + str(self.cam_num) + '  |' + str(pf.x_depths)
                # print 'List of y_depths for Particle Filter ' + str(i) +  '  Cam ' + str(self.cam_num) + '  |' + str(pf.y_depths)

                # print world_particles
                marker = Marker()
                marker.header.frame_id = 'world'
                marker.ns = 'mns'
                marker.id = i

                marker.type = marker.POINTS
                marker.action = marker.ADD
                marker.color.a = 1.0

                # Set the color of the markers for each particle filter
                try:
                    marker.color.r = colors[i] * color_select[i][0]
                    marker.color.g = colors[i] * color_select[i][1]
                    marker.color.b = colors[i] * color_select[i][2]
                except:
                    marker.color.r = colors[i]
                    marker.color.g = colors[i]
                    marker.color.b = colors[i]

                # Set the scale for the markers for each particle filter (the same 4all)
                marker.scale.x = 0.05
                marker.scale.y = 0.05
                marker.scale.z = 0.05

                # Set lifetime of the markers to infinity. They will be refreshed at each iteration
                marker.lifetime = rospy.Duration(1.1)


                points = list()

                # Number of particles
                N = pf_obj.N
                
                for j in range(N):
                    p = geometry_msgs.msg.Point()
                    
                    p.x = world_particles[j,0]
                    p.y = world_particles[j,1]
                    # p.z = world_particles[j][2]
                    p.z = 1.0
                    points.append(p)
                 
                marker.points = points

                marker_array.markers.append(marker)

                marker = Marker()
                marker.header.frame_id = 'world'
                marker.ns = 'cns'
                marker.id = i

                marker.type = marker.SPHERE
                marker.action = marker.ADD

                marker.color.a = 1.0                
                marker.color.r = 0.8
                marker.color.g = 0.8
                marker.color.b = 0.0

                # Set the scale for the markers for each particle filter (the same 4all)
                marker.scale.x = 0.2
                marker.scale.y = 0.2
                marker.scale.z = 0.2

                marker.pose.position.x = total_center[0]
                marker.pose.position.y = total_center[1]
                marker.pose.position.z = 1.0

                marker.lifetime = rospy.Duration(1)
                marker_array.markers.append(marker)

            # print marker_array
            self.pub.publish(marker_array)
            # rospy.sleep(0.3)


            # Dataset Evaluation
            self.dataset_eval(total_centers, timestamp.secs)
            print 'PROCESSEDDDDD    |    ' + str(self.processed_frames)

            if self.processed_frames == PROCESSED_FRAMES: 
                tp = self.metrics['tp']
                fp = self.metrics['fp']
                fn = self.metrics['fn']
                dists = self.metrics['dists']
                
                print 'BBOXES FOUND IN GENERAL   |   ' + str(self.db_bboxes_found)

                utils.csv_write(CSV_DATA_PATH + 'cams' + str(CAMS_2_USE) + 'PF_3D_metrics' + CSV_END, 'a', [tp,fp,fn], [])
                utils.csv_write(CSV_DATA_PATH + 'cams' + str(CAMS_2_USE) + 'PF_3D_dists' + CSV_END, 'w', dists, [])
                    
        # Restore the Normal Workflow for the Particle Filter to be used by other cameras
        # try:
            # print 'CAM' + str(self.cam_num)
            # print self.cam_pf_3d_objs[0].x_depths
            # print self.pf_3d_objs[0].x_depths
        

        # self.set_pfs_to_ros()
        # rospy.sleep(0.2)

        



if __name__ == "__main__":
    username = getpass.getuser()
    os.chdir('/home/' + username)
    print(os.getcwd())


    cam = sys.argv[1]
    cam = int(cam)

    if cam >= CAMERAS_NUM:
        ValueError('[ERROR] Incorrect Name for The Camera')

    # TOPIC Subscription, Node creation, parameters fill
    pub_topic = CAM_TOPIC_PF_3D_POINTS
    pub_node = CAM_PUBLISH_NODE + str(cam)
    pub_msg_type = MarkerArray
    pub_queue_size = 500

    sub_topic = CAM_TOPIC_DT_FRAME_ANALYSIS[cam]
    sub_node = CAM_SUBSCRIBE_NODE + str(cam)
    sub_msg_type = frame_anal
    sub_queue_size = 500

    trans = None
    rot = None

    # Set the parameter on the server of ROS
    # rospy.set_param('pf_3d_objs', [])


    vipt_2014 = VIPT_2014()
    

    pf_3d = ParticleFilter3D(sub_topic, sub_node, sub_msg_type, sub_queue_size, 
                             pub_topic, pub_node, pub_msg_type, pub_queue_size,
                             trans, rot, 'cam' + str(cam), vipt_2014)
    

    # pf_3d.reset_ros_server()

    tf_listen = tf.TransformListener()

    start_time = time.time()
    end_time = time.time ()
    while (end_time - start_time < 2.0):
        end_time = time.time()
        try:
            # print (tf_listen.allFramesAsString())
            # print tf_listen.getFrameStrings()
            # print tf_listen.canTransform("world", 'cam' + str(cam), rospy.Time.now())
            # tf_listen.waitForTransform('cam' + str(cam), "world", rospy.Time.now(), rospy.Duration(2.0))
            (trans, rot) = tf_listen.lookupTransform('world', 'cam' + str(cam), rospy.Time())
            # print trans
            # print rot
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue            

    # if len(trans) != 3:
    #     ValueError('[ERROR] Translation Matrix was not sent correctly')
    # if len(rot) != 4:
    #     ValueError('[ERROR] Rotation Matrix was not sent correctly')
    
    
    pf_3d.translation = trans
    pf_3d.rotation = rot

    print pf_3d.translation
    print pf_3d.rotation
    
    # print ('Time Elapsed for cam' + str(cam))
    # print (time.time() - start_time)

    
    rospy.spin()