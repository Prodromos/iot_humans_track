#!/usr/bin/env python
 
import sys
sys.path.append(sys.path[0] + "/..")

import cv2
import rospy
from sensor_msgs.msg import Image
from utils import ImgConvOpenCVRos
import time

# Constant string, the name of the topic for the communication and the name of the publisher
PUBLISHER_NAME = "img_stream_rpi3"
TOPIC = "/TOPIC1"



class PublishROS (object):
    """
    With this class the publish nodes from the RPI3s send the ROS images to the mainframe

    There are two function in this class:
        __init__ : initialisation of the publishing node
        pub_start : once ready, the publish node starts sending the images
    """

    def __init__ (self, pub_topic, pub_node, pub_msg_type, pub_queue_size):
        # super (PublishROS, self).__init__()
        self.pub = rospy.Publisher (pub_topic, pub_msg_type, queue_size=pub_queue_size) 
        try:
            rospy.init_node (pub_node, anonymous=True)
        except:
            pass
            

    def publish (self, data):
        self.pub.publish (data)


def main():
    """
    Under Construction...
    """
    
    pub_rpi3 = PublishROS (TOPIC, 'puber', Image, 50)
    start_time = time.time ()
    vc = cv2.VideoCapture(0)
    while vc.isOpened():
        ret, frame = vc.read()
        cv2.waitKey (10)
        if ret == True:
            ros_img = ImgConvOpenCVRos().conv_to_ros (frame)
            pub_rpi3.publish (ros_img)

        end_time = time.time ()
        if (end_time - start_time) >= 30.0:
            break
    del pub_rpi3
    vc.release()
    cv2.destroyAllWindows()
    

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass