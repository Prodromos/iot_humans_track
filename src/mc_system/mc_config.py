#!/usr/bin/env python

'''
    @FILE mc_config.py  Multi Camera configuration file

    Contains all the necessary variables to give life to the
    multi camera distributed system. It uses fairly the ROS
    Kinetic Topics, Nodes (Pubs / Subs)
'''
# Human Height assumption (in meters)
HUMAN_HEIGHT = 1.70

#Input Video Mode. If this parameter is set to 0, then we have Live Streaming
VIDEO_INPUT_MODE = 'Video_Input'

#Video Input Path. Depends on the num of the camera which video will be selected
VIDEO_INPUT_PATH = 'Documents/Project_INSIGHT/data/Datasets/VIPT_2014_Dataset/test_set/size_400x300/cam'

#Extension of the Video Input
VIDEO_INPUT_EXTENSION = '.avi'

# Number of total cameras in the System.
CAMERAS_NUM = 4

# Number of Cameras we will use
CAMS_2_USE = 3

# The camera X to use for the particle filter tracking
CAM_X = 'cam0'

# The general name topics of the cameras. They will not be used but it's a useful list.
CAM_ID = ['/cam' + str(i) for i in range(CAMERAS_NUM)]

''' The topics where the video stream is loaded. They publish each frame of 
    the stream.
'''
CAM_TOPIC_VIDEO_INPUT = [CAM_ID[i] + '/VideoInput' for i in range(CAMERAS_NUM)]

''' The topic where the analysis of the frames of the cameras, seperately takes place

    It is important to notice that these topics will subscribe the Video Inputs and
    publish the bboxes to another topic.
'''
CAM_TOPIC_DT_FRAME_ANALYSIS = [CAM_ID[i] + '/dtFrameAnalysis' for i in range(CAMERAS_NUM)]

''' The Detector Tracker Pair that will be used for each camera. 
TODO This Parameter Feature will change when we use the Raspberry Pi 
     Input Streams since each RPi will have each own configuration. 
'''
CAM_DT_PAIRS = [ ['ssd', 'CSRT'],
                 ['ssd', 'TLD'],
                 ['ssd' , 'CSRT' ],
                 ['ssd' , 'TLD' ]
               ]

CAM_TOPIC_PF_3D_POINTS = '/pfPointsDisplay'

''' The name of the Publish and Subscribe Nodes of the entite system. We will use only 
    topics with pub - subfunctionalities and the anonymous = True option. Thus, every
    node will be unique.
'''
CAM_PUBLISH_NODE = 'publisher'
CAM_SUBSCRIBE_NODE = 'subscriber'

#        move_conf, acc_error, resample_method,   N ,   w_thres,   pf_delete_thres,   max_vel,   z_std(in meters)
PF_PARAMS = [0.4,        5,         'residual',    100,     0.3,           10,            0.2,    0.25]

PROCESSED_FRAMES = 1000