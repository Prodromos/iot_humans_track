#!/usr/bin/env python

import rospy
from iot_humans_track.msg import lint16

class SubTracker(object):
    '''
    This class is being inherited by the MultiTrackers class located 
    in the multi_obj_track file. It initialises a Subscriber and returns
    the msg data with the callback function. For every tracker initialised,
    a corresponding object of this class will be instantiated
    '''
    def __init__(self):
        self.sub_data = lint16()

    def sub_init(self, nd_name, topic, queue_size):
        rospy.init_node (nd_name, anonymous=True)  
        self.node = rospy.Subscriber(topic, lint16, callback=self.callback, queue_size=queue_size) 
        rospy.spin()

    def callback(self, msg):
        self.sub_data = msg.data



if __name__ == "__main__":
    pass   