#!/usr/bin/env python

import cv2

import xml.etree.ElementTree as ET
import numpy as np 
import glob
import math

# Change the working directory accordingly
import os
import getpass

# Pandas Data Science Library
import pandas as pd 

from utils import Utils
import copy
# Import the classes from the files created by user
from detectors.cascade_human_detection import CascadeHumanDetection
from detectors.hog_human_detection import HOGHumanDetection


import matplotlib.pyplot as plt


#In order to avoid any problems regarding the CWD, it is changed according to the needs
#of the program
USERNAME = getpass.getuser()
os.chdir ('/home/' + USERNAME)

# Definition of the constant variables
CAVIAR_XML_FILES = 'Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/TwoLeaveShop2cor.xml'

CSV_DATA_PATH = 'Documents/Project_INSIGHT/data/evaluations/eval1/'
CSV_DATA_HAAR = 'eval_1_haar'
CSV_DATA_HOG = 'eval_1_hog'
CSV_END = '.csv'


VIDEOS_PATH = 'Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/'
VIDEOS_ENCODING = '.mpg'

XML_PATH = 'Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/'

CASCADE_HAAR_CLASSIFIER_PATH = 'opencv-3.4.4/data/haarcascades/'
CASCADE_LBP_CLASSIFIER_PATH = 'opencv-3.4.4/data/lbpcascades/'
CASCADE_HAAR_FULL_BODY_FILE = 'haarcascade_fullbody.xml'
CASCADE_HAAR_UPPER_BODY_FILE = 'haarcascade_upperbody.xml'
CASCADE_HAAR_LOWER_BODY_FILE = 'haarcascade_lowerbody.xml'
CASCADE_LBP_FILE = 'haarcascade_fullbody.xml'

class CaviarDataset():
    '''
    This class is responsible for the correct use of the CAVIAR Dataset.
    From this Dataset, only the Shopping Mall in Portugal videos (80) 
    will be evaluated and not the INRIA Clips. 
    The Class contains the function for the parsing of the XML files
    that correspond to the ground truth of each video. 

    This Dataset consists of 52 different videos, 27 with the camera
    at the corridor and 25 at the front.
    ONLY 1 OBJECT is required. 
    '''
    def __init__(self):
        '''
        Empty function. Exists only due to consistency.
        '''
        pass


    def coords_convert(self, gt_bboxes):
        '''
        The ground truth bboxes of the dataset are of the format
        [xc, yc, w, h] where xc, yc comprise the center point of the bbox.
        The eval_bboxes calculated from the OpenCV 
        algorithms are of the format [x, y, w, h], where x and y 
        comprise the top left point of the bbox.

        @param gt_bboxes: the ground truth bboxes in a video

        :return a list of these gt_bboxes with x,y instead of xc,yc
        '''
        for i in range(len(gt_bboxes)):
            for bbox in gt_bboxes[i]:
                x = bbox[0] - bbox[2]/2.0
                y = bbox[1] - bbox[3]/2.0     
                x = int(round(x))       # Using the round func leads to smaller errors 
                y = int(round(y))       # instead of using the int() func
                bbox[0] = x
                bbox[1] = y
        
        return gt_bboxes


    def xml_parser_gt_bboxes(self, XML_FILE):
        '''
        
        This function parses an XML file that contains information about each frame.
        The function uses primarily lists to extract the crucial information for every 
        frame (empty, number of bboxes in a frame, etc.) and a conversion of a list
        into a numpy array for easier manipulation of the data. 

        @param filename: the name of the xml file

        :returns: a list of lists. The index of the list represents the frame_num and
                the list assigned to that index contains all the bboxes in that frame.
        '''
        
        tree = ET.parse (XML_FILE)
        root = tree.getroot ()
        bboxes = []
        ids = []
        for bbox in root.findall("./frame/objectlist/object/box"):       # Get the coordinates of all the bboxes
            bboxes.append (bbox.attrib)

        for id in root.iter('object'):      # Make a list of the ids of the objects
            ids.append(id.attrib.get('id'))
        
        full_tree_tags = []
        for objl in root.findall('.//'):
            full_tree_tags.append(objl.tag)

        # bool_frames = []
        number_bboxes_per_frame = []       # A list that contains the total number of bboxes in a frame
        frame_index = 0
        bbox_cntr = 0
        for i in range(len(full_tree_tags)):
            if full_tree_tags[i] == 'frame':
                if i == 0:
                    continue
                else:
                    number_bboxes_per_frame.append(bbox_cntr)        
                    frame_index += 1
                bbox_cntr = 0
            # if full_tree_tags[i] == 'grouplist' and full_tree_tags[i-1] == 'objecinp2_lenist':
            #     bool_frames.append(False)
            # elif full_tree_tags[i] == 'object' and full_tree_tags[i-1] == 'objecinp2_lenist':
            #     bool_frames.append(True)
            elif full_tree_tags[i] == 'box' and full_tree_tags[i-2] == 'object':
                bbox_cntr += 1
            else:
                pass
        number_bboxes_per_frame.append(bbox_cntr)        
        #print(number_bboxes_per_frame)

        # print(bool_frames.count(True))
        # number_bboxes_per_frame = []       # A list that contains the total number of bboxes in a frame
        # bbox_cntr = 0
        # frame_cntr = 0
        # for i in range(len(ids)):
        #     if i==0:
        #         bbox_cntr +=1
        #     elif ids[i] > ids[i-1]:
        #         bbox_cntr +=1
        #     elif ids[i] <= ids[i-1]:
        #         number_bboxes_per_frame.append(bbox_cntr)      # To each frame correspond many bboxes
        #         bbox_cntr = 1       # If the new id is equal or lower than the previous, this is the first bbox of the new frame
        #         frame_cntr +=1
        #     else:
        #         pass

        # bboxes_total = []       # A list that contains the total number of bboxes in a frame
        # empty_frames_cntr = 0
        # for i in range(len(bool_frames)):
        #     if bool_frames[i] == True:
        #         # print(i - empty_frames_cntr)
        #         bboxes_total.append(number_bboxes_per_frame[i - empty_frames_cntr])
        #     else:
        #         empty_frames_cntr += 1
        #         bboxes_total.append(0)
        
        # print(bboxes_total)
        # print(len(bboxes_total))


        np_array_bboxes = np.asarray(number_bboxes_per_frame)    
        bboxes_per_frame = []

        for i in range(len(number_bboxes_per_frame)):
            bbox_index = np.sum(np_array_bboxes[0:i], dtype=int)        # Add all the previous bboxes to find the correct index of the bboxes
            temp_bbox = []
            if (number_bboxes_per_frame[i] == 0):
                temp_bbox.append([0,0,0,0])
                bboxes_per_frame.insert(i, temp_bbox)
            else:
                for j in range(number_bboxes_per_frame[i]):
                    # temp_bbox.append(bboxes[bbox_index + j].values())
                    temp_coords = []
                    xc = int(bboxes[bbox_index + j].get('xc'))
                    yc = int(bboxes[bbox_index + j].get('yc'))
                    w = int(bboxes[bbox_index + j].get('w'))
                    h = int(bboxes[bbox_index + j].get('h'))
                    temp_coords.append(xc)
                    temp_coords.append(yc)
                    temp_coords.append(w)
                    temp_coords.append(h)
                    temp_bbox.append(temp_coords)
                    # print(temp_bbox)
                bboxes_per_frame.insert(i, temp_bbox)    
        # for i in range(len(number_bboxes_per_frame)):
        #     bbox_index = np.sum(np_array[0:i], dtype=int)        # Add all the previous bboxes to find the correct index of the bboxes
        #     temp_bbox = []
        #     for j in range(number_bboxes_per_frame[i]):
        #         temp_bbox.append(bboxes[bbox_index + j].values())
        #     bboxes_per_frame.insert(i, temp_bbox)

        return bboxes_per_frame       # Return a list of lists with all the coordinates of bboxes for each frame


    def eval_files_list(self, videos_path, videos_encoding, xml_files_path):
        '''
        This functions lists all the videos and ground truth XML
        files in the dataset. 

        The videos_path and xml_files_path variables must end with 
        the character '/'.
        The videos_encoding variable must be in the form '.encoding'
        e.g. '.mpg' 
        Hence the parameter inside the glob.glob funct will be 
        glob.glob(videos_path + '*' + videos_encoding)

        @param videos_path: the path to the videos of the Dataset
        
        @param videos_encoding: the encoding method (e.g. '.mpg')

        @param xml_files_path: the path to the XML files
        
        :return videos_list, xml_files_list The names of all the videos
        and the corresponding ground truth file for each.
        '''
        
        videos_list = []
        for video in glob.glob(videos_path + '*' + videos_encoding):
            videos_list.append(video)
        videos_list.sort()

        xml_files_list = []
        for xml_file in glob.glob(xml_files_path + '*' + '.xml'):
            xml_files_list.append(xml_file)
        xml_files_list.sort()
        
        # Check both list to find if there are any problems
        if len(videos_list) == len(xml_files_list):
            for i in range(len(videos_list)):
                if videos_list[i][0:-4] != xml_files_list[i][0:-4]:
                    raise ValueError('Videos list and Ground Truth files do not coincide')
            print('The videos list and the gt list are OK!')
            return videos_list, xml_files_list
        else:
            raise ValueError('Videos list and Ground Truth files do not coincide')

        return None    




class DatasetEvaluation():
    '''
    This class contains the necessary functions to calculate 
    the metrics required for the evaluation of the Dataset
    '''

    def __init__(self):
        '''
        TODO Under Construction
        '''
        pass
        # self.empty_frames = 0
        # self.precision = 0
        # self.accuracy = 0
        # self.recall = 0
        # self.fmeasure = 0
        # self.dxdy_dists = []

    def correlate_data(self, input1, input2, acc_error=30, mode='cor_centers'):
        '''
        Make the best correlation between two lists of data
        
        May use bboxes (4 parameters) or centers (2 parameters)

        CRUCIAL for CORRECT results!!! Input1 is the Ground Truth
        and the Input2 is the data we want to evaluate!!!

        @param input1: the ground truth. It may be only the center of the bbox or the whole bbox. 
                       The code operated beautifully!.
        
        @param input2: the data for evaluation. It may be only the center of the bbox or the whole bbox. 
                       The code operated beautifully!.
        
        @param acc_error: the accuracy tolerable error. Usually small, it is measured in pixels

        @param mode: If the mode == 'cor_centers' ---> correlate centers the code returns the center dists and the metrics
                     Else, it returns the ordered list of the evaluated bboxes correlated by index to the ground truth bboxes
                     
        @return it depends on the mode. 
                -   mode = 'cor_centers'    :   dists_centers_eval, [tp, fp, fn]
                -   mode != 'cor_centers'   :   ordered_list, [tp, fp, fn]

        '''
        utils = Utils()

        if type(input1) == np.ndarray:
            input1 = input1.tolist()
        if type(input2) == np.ndarray:
            input2 = input2.tolist()
        
        dists_mtx = np.array([])
        
   
        try:
            inp1_len = len(input1)
            input1 = utils.remove_Nones(input1)
            input1_unused = copy.deepcopy(input1)
            
            inp2_len = len(input2)
            input2 = utils.remove_Nones(input2)
            input2_unused = copy.deepcopy(input2)
    
        except:
            inp1_len = 0
            input1 = []
            
            inp2_len = 0
            input2 = []
            

        dist_centers_eval = list()
        tp = 0

        #The ordered lists that the function will return
        ord_input2 = [None for _ in range(len(input1))]
             
        if inp1_len == 0 or inp2_len == 0:
            return [], [0, 0, 0]

        elif input1 == [0, 0, 0, 0]:
            fp = len(input2)
            return [], [0, fp, 0]

        elif inp1_len != 0  and  inp2_len != 0:          
            for di, input1box in enumerate(input1):
                if input1box is None  or  len(input1box) == 0:
                    continue
                if len(input1box) == 2:
                    dxc = input1box[0] 
                    dyc = input1box[1] 
                elif len(input1box) == 4:
                    dxc = input1box[0] + input1box[2]/2
                    dyc = input1box[1] + input1box[3]/2
                else:
                    ValueError('Wrong Argument at Dataset Evaluation Correlation Function')
                                
                dist_tmp = list()
                
                for ti, input2box in enumerate(input2):
                    if input2box is None  or  len(input2box) == 0:
                        continue
                    if len(input2box) == 2:
                        txc = input2box[0]
                        tyc = input2box[1]
                    elif len(input2box) == 4:
                        txc = input2box[0] + input2box[2]/2
                        tyc = input2box[1] + input2box[3]/2
                    else:
                        ValueError('Wrong Argument at Dataset Evaluation Correlation Function')

                    centers_dist = np.sqrt((dxc - txc)**2 + (dyc - tyc)**2)
                    dist_tmp.append(centers_dist)

                dist_arr = np.asarray(dist_tmp)

                if di == 0:
                    dists_mtx = dist_arr
                elif len(dists_mtx) == 0:
                    continue
                else:
                    dists_mtx = np.vstack((dists_mtx, dist_arr))    
                # dist_list.append(dist_tmp)

            # Find the minimum element of the array and remove(or make 1000) the column in which it was found
            if len(dists_mtx) != 0:
                if len(dists_mtx.shape) > 1:
                    rows, cols = dists_mtx.shape
                else:
                    rows = dists_mtx.shape
                    cols = 1
                    dists_mtx = np.array([dists_mtx])
            else:
                rows = cols = 0
            #deleted_cols = 0
            while cols > 0  and  np.min(dists_mtx) <= acc_error:
                
                min_coords = np.where(dists_mtx == np.min(dists_mtx))
                        
                det_index = min_coords[0][0] 
                track_index = min_coords[1][0]  
                
                row = det_index
                col = track_index

                input1box = input1[det_index]               
                input2box = input2[track_index]
            
                if mode == 'cor_centers':
                    if len(input1box) == 2:
                        inp1_xc = input1box[0] 
                        inp1_yc = input1box[1]
                    elif len(input1box) == 4:
                        inp1_xc = input1box[0] + input1box[2] / 2 
                        inp1_yc = input1box[1] + input1box[3] / 2

                    if len(input2box) == 2:
                        inp2_xc = input2box[0] 
                        inp2_yc = input2box[1]
                    elif len(input2box) == 4:
                        inp2_xc = input2box[0] + input2box[2] / 2 
                        inp2_yc = input2box[1] + input2box[3] / 2
                    
                    dxc = abs(inp1_xc - inp2_xc)
                    dyc = abs(inp1_yc - inp2_yc)
                    dist_centers_eval.append(np.sqrt((dxc)**2 + (dyc)**2))

                else:
                    tp += 1

                #Create the lists with the unused previous and current bboxes
                #They are used for the improvement of the code
                input1_unused.remove(input1box)
                input2_unused.remove(input2box)                
                
                ord_input2[det_index] = input2box
                
                # ids[det_index] = ident

                # dists_mtx = np.delete(dists_mtx, col, axis=1)   # Delete Column
                dists_mtx[:, col] = 1000
                dists_mtx[row, :] = 1000    # Assign a large number to the elements of the row in order to not be selected
                # deleted_cols += 1


        fp = len(input2_unused)
        fn = len(input1_unused)
        if mode == 'cor_centers':
            tp = len(dist_centers_eval)
            return dist_centers_eval, [tp, fp, fn]
        else:
            return ord_input2, [tp, fp, fn]
            

    def eval_metrics(self, metrics):
        tp = fp = fn = 0
        for metric in metrics:
            tp += metric[0]
            fp += metric[1]
            fn += metric[2]

        if (tp + fp + fn) == 0:
            accuracy = np.NaN
        else:
            accuracy = float(tp) / float(tp + fp + fn)
       
        if (tp + fp) == 0:
            precision = np.NaN
        else:
            precision = float(tp) / float(tp + fp)
        
        if (tp + fn) == 0:
            recall = np.NaN
        else:
            recall = float(tp) / float(tp + fn)
       
        if np.isnan(precision) or np.isnan(recall) or (precision + recall) == 0:
            fmeasure = np.NaN
        else:
            fmeasure = 2.0 * ((precision * recall) / (precision + recall))

        return [accuracy, precision, recall, fmeasure, tp, fp, fn]


    def eval_iou(self, bboxA, bboxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(bboxA[0], bboxB[0])
        yA = max(bboxA[1], bboxB[1])
        xB = min(bboxA[2], bboxB[2])
        yB = min(bboxA[3], bboxB[3])
    
        # compute the area of intersection rectangle
        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (bboxA[2] - bboxA[0] + 1) * (bboxA[3] - bboxA[1] + 1)
        boxBArea = (bboxB[2] - bboxB[0] + 1) * (bboxB[3] - bboxB[1] + 1)
    
        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)

        # return the intersection over union value
        return iou

    # def eval_frame(self, gt_bboxes, eval_bboxes, iou_threshold, frame_size=(384, 288)):
    def eval_frame(self, gt_bboxes, eval_bboxes, iou_threshold, frame_size=(384, 288)):
        '''
        Evaluation and Comparison of the detected bboxes using
        the gt_bboxes as ground truth. The function is used for 
        the evaluation of only 1 video, although it is possible 
        to be used for the whole Dataset. TODO

        @param gt_bboxes: ground truth bboxes. It is a list of lists which contains at 
                          each position a list of all the bboxes of the current frame. TODO  FOR NOW a list of bboxes
        
        @param eval_bboxes: detected bboxes. It is a list of lists which contains at 
                            each position a list of all the bboxes of the current frame. TODO FOR NOW a list of bboxes
        
        @param frame_size: the size of each frame. By default for the CAVIAR Dataset the frame size 
                           is 384x288 pixels
        
        @param acc_err: acceptable error percent (should be a float 0 <= acc_err <= 1)
        
        :return a list of the metrics calculated [empty_frames, tp, fp, fn, dxdy_dist]
        '''
        true_positives = 0
        false_positives = 0
        false_negatives = 0
        empty_frames = 0
        dxc_dyc_dists = []
        # TODO DELETE THESE ONCE THE PARTICLE FILTER WORKS
        axis_x_error = 0  #acc_err * frame_size[0]      # Tolerable error at x_axis as number of pixels
        axis_y_error = 0  #acc_err * frame_size[1]      # Tolerable error at y_axis as number of pixels

        used_bboxes = list()
        for eval_bbox in eval_bboxes:
            #TODO DELETE ONCE THE PARTICLE FILTER WORKS
            # if len(eval_bbox) == 2:     # Only [xc,yc]
            #     # (xc,yc) coordinates of the center of the bbox
            #     eval_xc = int(eval_bbox[0])
            #     eval_yc = int(eval_bbox[1])

            if len(eval_bbox) == 4:   # Evaluate the rectangular bboxes      
                #TODO DELETE THESE ONCE MY PF WORKS
                # eval_bbox_xinp2_len = eval_bbox[0]
                # eval_bbox_yinp2_len = eval_bbox[1]
                # eval_bbox_xbr = eval_bbox[0] + eval_bbox[2]
                # eval_bbox_ybr = eval_bbox[1] + eval_bbox[3]

                # (xc,yc) coordinates of the center of the bbox
                eval_xc = int(eval_bbox[0] + eval_bbox[2]/2.0)
                eval_yc = int(eval_bbox[1] + eval_bbox[3]/2.0)

                eval_bbox[2] = eval_bbox[0] + eval_bbox[2]
                eval_bbox[3] = eval_bbox[1] + eval_bbox[3]

            bbox_detected = False
            
            max_iou = -1
            best_gt_bbox = None
            for gt_bbox in gt_bboxes:              
                temp_gt_bbox = []
                #TODO DELETE THESE ONCE MY PF WORKS 
                # gt_bbox_xinp2_len = gt_bbox[0]
                # gt_bbox_yinp2_len = gt_bbox[1]
                # gt_bbox_xbr = gt_bbox[0] + gt_bbox[2]
                # gt_bbox_ybr = gt_bbox[1] + gt_bbox[3]
                
                # # (xc,yc) coordinates of the center of the bbox
                # gt_xc = int(gt_bbox[0] + gt_bbox[2]/2.0)
                # gt_yc = int(gt_bbox[1] + gt_bbox[3]/2.0)

                # The bboxes are set to the form [xinp2_len,yinp2_len, xbr, ybr]
                temp_gt_bbox.append(gt_bbox[0])
                temp_gt_bbox.append(gt_bbox[1])                
                temp_gt_bbox.append(gt_bbox[0] + gt_bbox[2])
                temp_gt_bbox.append(gt_bbox[1] + gt_bbox[3])  
                # gt_bbox[2] = gt_bbox[0] + gt_bbox[2]
                # gt_bbox[3] = gt_bbox[1] + gt_bbox[3] 
                
                iou = self.eval_iou(temp_gt_bbox, eval_bbox)
                if iou > max_iou:
                    max_iou = iou
                    best_gt_bbox = gt_bbox
                    continue
            
            used_flag = 0
            for box in used_bboxes:
                if box == best_gt_bbox:
                    used_flag = 1

            if gt_bboxes != [[0,0,0,0]]:
                gt_bbox = best_gt_bbox
                used_bboxes.append(gt_bbox)
                # (xc,yc) coordinates of the center of the bbox
                gt_xc = int(gt_bbox[0] + gt_bbox[2]/2.0)
                gt_yc = int(gt_bbox[1] + gt_bbox[3]/2.0)

                #TODO DELELTE THIS ONCE MY PARTICLE FILTER WORKS! This is Useful only for deepgaze!
                # if len(eval_bbox) == 2:
                #     dist = math.sqrt((eval_xc - gt_xc)**2 + (eval_yc - gt_yc)**2)
                #     error_dist = math.sqrt((axis_x_error)**2 + (axis_y_error)**2)
                #     if dist <= error_dist:
                #         bbox_detected = True    
                #         true_positives += 1
                #         dxc_dyc_dists.append(dist)      # The distance of the centers of the bboxes
                #         continue
                        
                if len(eval_bbox) == 4:
                    # iou = self.eval_iou(gt_bbox, eval_bbox)
                    max_iou = np.round(max_iou, decimals=2)
                    if  max_iou >= iou_threshold:
                        bbox_detected = True    
                        true_positives += 1
                        dxc_dyc_dists.append(math.sqrt( (eval_xc - gt_xc)**2 + (eval_yc - gt_yc)**2) )      # The distance of the centers of the bboxes
                        continue
                    # if  abs(eval_bbox_xinp2_len - gt_bbox_xinp2_len) <= axis_x_error    and  \
                    #     abs(eval_bbox_yinp2_len - gt_bbox_yinp2_len) <= axis_y_error    and  \
                    #     abs(eval_bbox_xbr - gt_bbox_xbr) <= axis_x_error    and  \
                    #     abs(eval_bbox_ybr - gt_bbox_ybr) <= axis_y_error:

                    else:
                        if bbox_detected == False:
                            false_positives +=1
                            continue
        
        # Find the false negatives of the frame
        if gt_bboxes != [[0, 0, 0, 0]] and len(gt_bboxes) > len(eval_bboxes):
            false_negatives = len(gt_bboxes) - len(eval_bboxes)

        # If a frame is detected empty or is truly empty extract the FN or FP values
        # Cannot be calculated in the for loop (not executed for 0 bboxes)
        if len(eval_bboxes) == 0:      # If 0 bboxes were detected the for loop won't be executed        
            if gt_bboxes == [[0, 0, 0, 0]]:      # If 0 bboxes were actually in the frame     
                empty_frames += 1
            else:
                false_negatives = len(gt_bboxes)
        elif gt_bboxes == [0, 0, 0, 0]:
            false_positives += len(eval_bboxes)        
        else:
            pass
        
        return [empty_frames, true_positives, false_positives, false_negatives, dxc_dyc_dists]

    # def eval_video(self, gt_bboxes, eval_bboxes, iou_threshold, frame_size=(384, 288)):
    def eval_video(self, gt_bboxes, eval_bboxes, iou_threshold, frame_size=(384, 288)):
        '''
        Evaluation of a video and extraction of statistical metrics

        @param video_name: the name of the video being evaluated
        
        @param gt_bboxes: ground truth bboxes. It is a list of lists which contains at 
                          each position a list of all the bboxes of the current frame. TODO  FOR NOW a list of bboxes
        
        @param eval_bboxes: detected bboxes. It is a list of lists which contains at 
                            each position a list of all the bboxes of the current frame. TODO FOR NOW a list of bboxes
        
        @param frame_size: the size of each frame. By default for the CAVIAR Dataset the frame size 
                           is 384x288 pixels

        :return a list with the metrics and a list with the dists 
        '''
        tp = 0
        fp = 0
        fn = 0
        
        empty_frames = 0
        precision = 0
        accuracy = 0
        recall = 0
        fmeasure = 0
        dxdy_dists = []

        metrics = []
        if len(gt_bboxes) != len(eval_bboxes):
            ValueError('GT BBOXES DIFFER FROM THE EVAL BBOXES. EXITING...')

        for i in range(len(gt_bboxes)):       
            # fr_read, frame = vc.read()
            # if fr_read:
            metrics.append(self.eval_frame(gt_bboxes[i], eval_bboxes[i], iou_threshold, frame_size))
            # else: 
                # break

        for metric in metrics:
            # self.empty_frames += metric[0]
            empty_frames += metric[0]
            tp += metric[1]
            fp += metric[2]
            fn += metric[3]
            for dxdy in metric[4]:
                if dxdy != []:
                    # self.dxdy_dists.append(dxdy)
                    dxdy_dists.append(dxdy)

        if (tp + fp + fn) == 0:
            accuracy = np.NaN
        else:
            accuracy = float(tp) / float(tp + fp + fn)
       
        if (tp + fp) == 0:
            precision = np.NaN
        else:
            precision = float(tp) / float(tp + fp)
        
        if (tp + fn) == 0:
            recall = np.NaN
        else:
            recall = float(tp) / float(tp + fn)
       
        if np.isnan(precision) or np.isnan(recall) or (precision + recall) == 0:
            fmeasure = np.NaN
        else:
            fmeasure = 2.0 * ((precision * recall) / (precision + recall))

        # self.accuracy = float(tp) / float(tp + fp + fn)
        # self.precision = float(tp) / float(tp + fp)
        # self.recall = float(tp) / float(tp + fn)
        # self.fmeasure = 2.0 * ((self.precision * self.recall) / (self.precision + self.recall))

        if dxdy_dists == []:
            mean_dist = 0
        else:
            mean_dist = np.mean(dxdy_dists)
        
        return [empty_frames, accuracy, precision, recall, fmeasure, mean_dist, len(dxdy_dists), fp, fn], dxdy_dists



def eval1():
    caviar_dataset = CaviarDataset()
    dataset_eval = DatasetEvaluation()  
    utils = Utils()  
    # dataset_eval_haar_objs = []      # List of DatasetEvaluation objects. In each position an instance of the class e
    # dataset_eval_hog_objs = []

    cascade_haar_full_classifier = CascadeHumanDetection(CASCADE_HAAR_CLASSIFIER_PATH, CASCADE_HAAR_FULL_BODY_FILE)
    hog_classifier = HOGHumanDetection()
  
    #Authentication to the ploinp2_leny platform
    utils.plotly_auth()

    videos_list, gt_list = caviar_dataset.eval_files_list(VIDEOS_PATH, VIDEOS_ENCODING, XML_PATH)    
    # i = videos_list.index("Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/OneLeaveShopReenter1cor.mpg")
    # print(i)

    acc_err = 0.10


    # parameters_list = [ (winStride), (padding), scale]    The scale is used for HOG and Haar
    # parameters_list = [ [(4,4), (8,8), 1.02], [(4,4), (8,8), 1.03], [(4,4), (8,8), 1.05], [(4,4), (8,8), 1.1],          \
    #                     [(4,4), (8,8), 1.2], [(4,4), (8,8), 1.3], [(4,4), (8,8), 1.4], [(4,4), (8,8), 1.5],            \
    #                     [(8,8), (8,8), 1.02], [(8,8), (8,8), 1.03], [(8,8), (8,8), 1.05], [(8,8), (8,8), 1.1],          \
    #                     [(8,8), (8,8), 1.2], [(8,8), (8,8), 1.3], [(8,8), (8,8), 1.4], [(8,8), (8,8), 1.5],            \
    #                     [(8,8), (16,16), 1.02], [(8,8), (16,16), 1.03], [(8,8), (16,16), 1.05], [(8,8), (16,16), 1.1],  \
    #                     [(8,8), (16,16), 1.2], [(8,8), (16,16), 1.3], [(8,8), (16,16), 1.4], [(8,8), (16,16), 1.5] ]  
    
    
    # parameters_list = [ [(8,8), (8,8), 1.01], [(8,8), (8,8), 1.03], [(8,8), (8,8), 1.05], [(8,8), (8,8), 1.1],          \
    #                     [(8,8), (8,8), 1.2], [(8,8), (8,8), 1.3], [(8,8), (8,8), 1.4], [(8,8), (8,8), 1.5],            \
    #                     [(8,8), (16,16), 1.01], [(8,8), (16,16), 1.03], [(8,8), (16,16), 1.05], [(8,8), (16,16), 1.1],  \
    #                     [(8,8), (16,16), 1.2], [(8,8), (16,16), 1.3], [(8,8), (16,16), 1.4], [(8,8), (16,16), 1.5] ] 

    parameters_list = [ [(8,8), (8,8), 1.1],                                                                           \
                        [(8,8), (8,8), 1.2], [(8,8), (8,8), 1.3], [(8,8), (8,8), 1.4], [(8,8), (8,8), 1.5],            \
                        [(8,8), (16,16), 1.1],                                                                         \
                        [(8,8), (16,16), 1.2], [(8,8), (16,16), 1.3], [(8,8), (16,16), 1.4], [(8,8), (16,16), 1.5] ] 

    # print(parameters_list)
    for parameters in parameters_list:
        p = parameters_list.index(parameters)
        casc_eval = []
        casc_dists = []
        hog_eval = []
        hog_dists = []
        for i in range(len(videos_list)):
            print('attempt ' + str(p) + '  |  video ' + str(i))
            # gt_bboxes = []
            gt_bboxes = caviar_dataset.xml_parser_gt_bboxes(gt_list[i])
            gt_bboxes = caviar_dataset.coords_convert(gt_bboxes)

            
            if p < 5: 
                casc_bboxes = cascade_haar_full_classifier.cascade_detection_video(videos_list[i], parameters[2])       
                tmp_casc, tmp_haar_dists = dataset_eval.eval_video(videos_list[i], gt_bboxes, casc_bboxes, acc_err) 
                casc_eval.append(tmp_casc)
                casc_dists.append(tmp_haar_dists)
            
            hog_bboxes = hog_classifier.hog_detection_frame(videos_list[i], parameters[0], parameters[1],parameters[2])      
            tmp_hog, tmp_hog_dists = dataset_eval.eval_video(gt_bboxes, hog_bboxes, acc_err)
            hog_eval.append(tmp_hog)
            hog_dists.append(tmp_hog_dists)



        # Export metrics to csv files    
        metrics_list = ['Empty_Frames', 'Accuracy', 'Precision', 'Recall', 'F_Measure', 'Mean_dist', 'True_Positives']

        if p < 5: 
            utils.csv_write(CSV_DATA_PATH + CSV_DATA_HAAR + '_param_' + str(p) + CSV_END, 'w', casc_eval, metrics_list)
            utils.csv_write(CSV_DATA_PATH + CSV_DATA_HAAR + '_dists' + '_param_' + str(p) + CSV_END, 'w', casc_dists, [])
            # Create Dataframes using the pandas library to read the csv files
            # df_haar = pd.read_csv(CSV_DATA_PATH + CSV_DATA_HAAR + '_' + str(attempt))

            
        utils.csv_write(CSV_DATA_PATH + CSV_DATA_HOG + '_param_' + str(p) + CSV_END, 'w', hog_eval, metrics_list)
        utils.csv_write(CSV_DATA_PATH + CSV_DATA_HOG + '_dists' + '_param_' + str(p) + CSV_END, 'w', hog_dists, [])

        # Create Dataframes using the pandas library to read the csv files
        # df_hog = pd.read_csv(CSV_DATA_PATH + CSV_DATA_HOG + '_' + str(attempt))

        
        # if attempt < 8:
        #     VANILLA_HAAR = 'vanilla_haar' + '_' + str(attempt)
        #     haar_data = draw_table_ploinp2_leny(VANILLA_HAAR ,df_haar, metrics_list, 'left', '#C2D4FF', '#F5F8FF')
        # VANILLA_HOG = 'vanilla_hog' + '_' + str(attempt)
        # hog_data = draw_table_ploinp2_leny(VANILLA_HOG ,df_hog, metrics_list, 'left', '#C2D4FF', '#F5F8FF')


    # haar_trace = go.Table( header=dict( values=list(df_haar.columns),
    #                                     fill = dict(color='#C2D4FF'),
    #                                     align = ['left'] * len(metrics_list)),
    #                        cells=dict(values=[df_haar.Empty_Frames, df_haar.Accuracy, df_haar.Precision, \
    #                                           df_haar.Recall, df_haar.F_Measure.dxcdyc_dist],
    #                                   fill = dict(color='#F5F8FF'),
    #                                   align = ['left'] * len(metrics_list)))
    # haar_data = [haar_trace]

    # hog_trace = go.Table( header=dict( values=list(df_hog.columns),
    #                                     fill = dict(color='#C2D4FF'),
    #                                     align = ['left'] * len(metrics_list)),
    #                        cells=dict(values=[df_hog.Empty_Frames, df_hog.Accuracy, df_hog.Precision, \
    #                                           df_hog.Recall, df_hog.F_Measure],
    #                                   fill = dict(color='#F5F8FF'),
    #                                   align = ['left'] * len(metrics_list)))
    # hog_data = [hog_trace]

        # Plot the dxc, dyc distances of the true positives in each video
        # draw_scatter_dists(hog_dists,'Distances of the centers of the bboxes for HOG', 'video', 'dist_xc_yc')
        # if attempt < 8:
        #     draw_scatter_dists(casc_dists,'Distances of the centers of the bboxes for Haar', 'video', 'dist_xc_yc')

    csv_eval_list, csv_name_list = utils.csv_read(CSV_DATA_PATH, 'normal_csv', 0)

    csv_haar_indices = utils.str_search(csv_name_list, 'haar_param')
    csv_hog_indices = utils.str_search(csv_name_list, 'hog_param')
    csv_haar_dists_indices = utils.str_search(csv_name_list, 'haar_dists')
    csv_hog_dists_indices = utils.str_search(csv_name_list, 'hog_dists')

    for data in csv_eval_list:
        i = csv_eval_list.index(data)
        if csv_haar_indices.count(csv_eval_list.index(data)) > 0: 
            vanilla_haar = 'vanilla_haar' + '_' + csv_name_list[i][-5]
            haar_data = utils.draw_table_plotly(vanilla_haar , data, metrics_list, 'left', '#C2D4FF', '#F5F8FF', 'normal_csv')
        elif csv_hog_indices.count(csv_eval_list.index(data)) > 0:
            vanilla_hog = 'vanilla_hog' + '_' + csv_name_list[i][-5]
            hog_data = utils.draw_table_plotly(vanilla_hog , data, metrics_list, 'left', '#C2D4FF', '#F5F8FF', 'normal_csv') 
           
        elif csv_haar_dists_indices.count(csv_eval_list.index(data)) > 0:
            utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'haar_' + csv_name_list[i][-5]          \
                               , 'video', 'dist_xc_yc', CSV_DATA_PATH, True)  
        elif csv_hog_dists_indices.count(csv_eval_list.index(data)) > 0:
            utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'hog_' +  csv_name_list[i][-5]  \
                               , 'video', 'dist_xc_yc', CSV_DATA_PATH, True)  
        else:
            ValueError('The index is not inside the folder')


import os
import getpass
     
def pf_eval ():
    
    utils = Utils()

    os.chdir('/home/pro/')
    csv_path = 'Documents/Project_INSIGHT/data/evaluations/Single_Camera_PF_eval/new_pf_eval/'

    
    pf_paths = ['Haar_CSRT/',  #/accept_error_40 pixels/', 
                'Haar_TLD/',   #/accept_error_40 pixels/',
                'SSD_CSRT/',    #/accept_error_40 pixels/',
                'SSD_TLD/']    #/accept_error_40 pixels/' ]

    pf_param_paths = [#'pf_N_100_w_0.4_multinomal/',
                      #'pf_N_150_w_0.4_multinomal/',
                      'pf_N_100_w_0.3_residual']
                      #'pf_N_150_w_0.4_residual/' ]
                
    filenames = [[ 'haar_dists.csv', 'CSRT_dists.csv'],
                 [ 'haar_dists.csv', 'TLD_dists.csv'],
                 [ 'SSD_dists.csv', 'CSRT_dists.csv'],
                 [ 'SSD_dists.csv', 'TLD_dists.csv']]

    tables_filenames = [[ 'haar_mse_std.csv', 'CSRT_mse_std.csv'],
                        [ 'haar_dists.csv', 'TLD_dists.csv'],
                        [ 'SSD_dists.csv', 'CSRT_dists.csv'],
                        [ 'SSD_dists.csv', 'TLD_dists.csv']]


    metrics =['Accuracy', 'Recall', 'Precision', 'F-Measure']
    for pf_path in pf_paths:
        for pf_param_path in pf_param_paths:
          
            total_path = csv_path + pf_path #+ pf_param_path
    
            csv_eval_list, csv_name_list = utils.csv_read(total_path, 'normal_csv', 0)

            haar_indices = utils.str_search(csv_name_list, 'haar_dists')
            haar_metrics = utils.str_search(csv_name_list, 'haar_metrics')
            
            csrt_indices = utils.str_search(csv_name_list, 'CSRT_dists')
            csrt_metrics = utils.str_search(csv_name_list, 'CSRT_metrics')

            ssd_indices = utils.str_search(csv_name_list, 'ssd_dists')
            ssd_metrics = utils.str_search(csv_name_list, 'ssd_metrics')
            
            tld_indices = utils.str_search(csv_name_list, 'TLD_dists')
            tld_metrics = utils.str_search(csv_name_list, 'TLD_metrics')
            
            pf_indices = utils.str_search(csv_name_list, pf_param_path[:-1] + '_dists')
            pf_mean = utils.str_search(csv_name_list, pf_param_path[:-1] + '_mean')
            pf_metrics = utils.str_search(csv_name_list, pf_param_path[:-1] + '_metrics')


            for data in csv_eval_list:
                i = csv_eval_list.index(data)
                if haar_indices.count(csv_eval_list.index(data)) > 0:
                    _, _, mse_std = utils.find_mse_std(data)
                    utils.csv_write(total_path + 'haar_mse_std.csv', 'w', mse_std)

                    # utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'haar_'          \
                    #                 , 'video', 'dist_xc_yc', total_path, True)  

                elif csrt_indices.count(csv_eval_list.index(data)) > 0:
                    _, _, mse_std = utils.find_mse_std(data)
                    utils.csv_write(total_path + 'CSRT_mse_std.csv', 'w', mse_std)
                    # utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'CSRT_'   \
                    #                 , 'video', 'dist_xc_yc', total_path, True)  

                elif  ssd_indices.count(csv_eval_list.index(data)) > 0:
                    _, _, mse_std = utils.find_mse_std(data)
                    utils.csv_write(total_path + 'ssd_mse_std.csv', 'w', mse_std)

                    # utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'SSD'   \
                    #                 , 'video', 'dist_xc_yc', total_path, True)

                elif  tld_indices.count(csv_eval_list.index(data)) > 0:
                    _, _, mse_std = utils.find_mse_std(data)
                    utils.csv_write(total_path + 'TLD_mse_std.csv', 'w', mse_std)
                    # utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'TLD'   \
                    #                 , 'video', 'dist_xc_yc', total_path, True)  

                elif  pf_indices.count(csv_eval_list.index(data)) > 0:
                    _, _, mse_std = utils.find_mse_std(data)
                    utils.csv_write(total_path + 'pf_mse_std.csv', 'w', mse_std)
                    # utils.draw_scatter_dists(np.asarray(data),'dists_xc_yc_' + 'Particle Filter'   \
                    #                 , 'video', 'dist_xc_yc', total_path, True)  
                

                # https://www.datacamp.com/community/tutorials/matplotlib-tutorial-python
                # if haar_metrics.count(csv_eval_list.index(data)) > 0: 
                #     haar_table = plt.table(cellText=data, loc='center')
                #     haar_table.set_fontsize(14)
                #     haar_table.scale(1,4)
                #     plt.show()

                
                else:    
                    ValueError('The index is not inside the folder')

if __name__ == "__main__":
    '''
    In the main function the evaluation of the whole
    Dataset takes place. Hence, after each video evaluation
    the instance is deleted.
    '''
    pf_eval()

