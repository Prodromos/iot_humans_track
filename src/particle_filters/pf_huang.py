#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import copy
import numpy as np
from numpy.random import normal
from skimage.measure import compare_ssim as ssim
from functools import cmp_to_key
from time import time
import os

# drawing = False
# # ix,iy,w,h= -1, -1,-1,-1
# cropped = None
# firstFrame = None
# the_firstFrame = None
# is_cropped = False

def add_guassnoise(rng,mem):
    return np.random.normal(rng,mem)

# #高斯噪音的标准差
# TRANS_X_STD = 1.0
# TRANS_Y_STD = 0.5
# TRANS_S_STD = 0.001

# #二阶动态回归模型参数
# A1  = 2.0
# A2 = -1.0
# B0  = 1.0000

class Particle():
    def __init__(self, x=0, y=0, s=0, w=0, h=0, 
                       x_std=1.0, y_std=0.5, s_std=0.001, 
                       a1=2.0, a2=-1.0, b0=1.000):
        self.x = x
        self.y = y
        self.s = s
        self.xp = x
        self.yp = y
        self.sp = s
        self.x0 = x
        self.y0 = y
        self.width = w
        self.height = h
        self.weight = 1


        self.x_std = x_std
        self.y_std = y_std
        self.s_std = s_std
        self.A1 = a1
        self.A2 = a2
        self.B0 = b0



    #使用二阶动态回归来更新粒子状态
    def transition(self,frame, img_track, img_w, img_h, rng = 0):
        frame_h, frame_w = frame.shape[:2]
        up_x = self.A1 * (self.x - self.x0) + self.A2 * (self.xp - self.x0) + self.B0 * add_guassnoise(rng,self.x_std) + self.x0
        up_x = max(0.0, min(frame_w - 1.0 , up_x))
        up_y = self.A1 * (self.y - self.y0) +  self.A2 * (self.yp - self.y0) + self.B0 * add_guassnoise(rng,self.y_std) + self.y0
        up_y = max(0.0, min(frame_h - 1.0, up_y))
        up_s = self.A1 * (self.s - 1.0) + self.A2 * (self.sp - 1.0) + self.B0 * add_guassnoise(rng,self.s_std) + 1.0
        # print(up_s,self.s)
        up_s = max(0.1,up_s)
        self.xp = self.x
        self.yp = self.y
        self.sp = self.s
        self.x = up_x
        self.y = up_y
        self.s = up_s
        y0 = max(0,int(self.y - img_h * self.s * 0.5))
        y1 = max(0,int(self.y + img_h * self.s * 0.5))
        x0 = max(0,int(self.x - img_w * self.s * 0.5))
        x1 = max(0,int(self.x + img_w * self.s * 0.5))
        # print(self.x,self.y)
        #print(y0,y1,x0,x1)
        noisy_sub = frame[y0: y1,x0:x1]
        noisy_sub = cv2.resize(noisy_sub,(img_track.shape[1], img_track.shape[0]),interpolation=cv2.INTER_CUBIC)
        self.weight =max(0.0, ssim(img_track, noisy_sub, multichannel=True))
        #print(self.weight)


class HuangPF():
    '''
    Implementation of a Particle Filter originally implemented
    by InkdyeHuang (GitHub). In order to make good use of the 
    filter, the user must enter the desired number of particles 
    when they declare the object. Then the user calls the init_track
    function and after that, for each iteration and a new frame, 
    call only the track_frame()
    '''
    def __init__(self, particle_num=50):
        '''
        @param particle_num: number of particles to use
        @var particle_list: a list that contains particle_num particles from class Particle
        @var bbox = a list that contains the bounding box. Must be of the form [xtl,ytl,xbr,ybr]
        @var xc: the center x of the bbox
        @var yc: the center y of the bbox
        @var image_w: the width of the tracked image
        @var image_h: the height of the tracked image
        @var img_track: the image that is being tracked
        '''
        self.particle_num = particle_num        
        self.particle_list = []

        self.bbox = []

        self.xc = -1
        self.yc = -1
        self.image_w = -1
        self.image_h = -1

        # self.xtl = int(self.xc - self.image_w / 2.0)
        # self.ytl = int(self.yc - self.image_h / 2.0)
        # self.xbr = int(self.xc + self.image_w / 2.0)
        # self.ybr = int(self.yc + self.image_h / 2.0)
        # self.bbox = [int(self.xc - self.image_w / 2.0), int(self.yc - self.image_h / 2.0), 
        #              int(self.xc + self.image_w / 2.0), int(self.yc + self.image_h / 2.0)]

        self.img_track = None       # The image to track

    def init_HuangPF(self, img_track=None, bbox=None):
        '''
        This function initialises the Huang Particle Filter.
        If the img or the bbox is None, then the code uses
        assumes that the variables xc,yc,w,h have been set
        with another way.

        @param img_track: the image to track
        @param bbox: the bounding box of the image
        
        @return None
        '''
        # particle_list = []
        # ix, iy, w, h = [-1]*4
        if img_track is None  or  bbox is None:    
            xc = self.xc
            yc = self.yc
            w = self.img_track.shape[1]
            h = self.img_track.shape[0]
        else:
            self.img_track = img_track
            self.bbox = bbox
            h,w = self.image_h, self.image_w = img_track.shape[:2]
            xc = self.xc = int(bbox[0] + w/2.0)
            yc = self.yc = int(bbox[1] + h/2.0)

        for i in range(self.particle_num):
            p = Particle(xc, yc, 1, w, h)
            self.particle_list.append(p)
        self.normal_weight()
        self.resample()
        # self.particle_list
        return None
    
    def choose_frame(self, event, x, y, flags, param):
        '''
        Demo function to help find if the program functions properly
        '''
        global drawing, is_cropped
        # ix, iy, w, h, 
        if event == cv2.EVENT_LBUTTONDOWN:
            print('left button down')
            drawing = True
            self.xc, self.yc = x,y
            # print x,y
        elif event == cv2.EVENT_MOUSEMOVE:
            print('mouse move')
            if drawing == True:
                cv2.rectangle(firstFrame, (self.xc, self.yc), (x,y), (0,255,0), -1)
        elif event == cv2.EVENT_LBUTTONUP:
            print('left button up')
            drawing = False
            is_cropped = True
            self.img_track = the_firstFrame[self.yc:y,self.xc:x]
            self.width, self.height = x - self.xc, y - self.yc
            print self.xc, self.yc
            self.xc = self.xc + self.width * 0.5
            self.yc = self.yc + self.height * 0.5
            print self.xc, self.yc
            cv2.imshow("cropped",self.img_track)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    def normal_weight(self):
        '''
        Normalise the weights
        '''
        Sum = 0
        for p in self.particle_list:
            Sum += p.weight
        for i in range(self.particle_num):
            self.particle_list[i].weight /= Sum
        return None

    def add_noise(self, mat):
        '''
        Add noise to the measurements
        '''
        mat = mat + np.random.normal(0,3,mat.shape[0] * mat.shape[1]).reshape(mat.shape)
        for i in range(self.particle_num):
            mat[i][0] %= self.image_w
            mat[i][1] %= self.image_h
        return mat

    def resample(self):
        '''
        The resample function of the class
        '''
        new_particle_list = []
        k = 0
        for i in range(self.particle_num):
            np = int(round(self.particle_list[i].weight * self.particle_num))
            for j in range(np + 1):
                if k < self.particle_num:
                    new_particle_list.append(copy.deepcopy(self.particle_list[i]))
                    k += 1
        self.particle_list = new_particle_list
        return None

    def track_frame(self, frame, params, display=False):
        '''
        This function is used for tracking a frame. If it is used
        constantly, it can track an object in a video

        @param params: the image being tracked  TODO ADD COMMENT
        @param display: bool. If True use OpenCV to plot the frame

        @return bbox, particle_list: the estimated position of the bbox 
            and the particle list that is linked to the object that called 
            this function
        '''
        # if type(params) != int:
        #     ValueError('Wrong Number of Args inside Huang track_frame(). Exiting...')

        # print(len(particle_list))
        # particle_list = self.particle_list
        h, w = self.img_track.shape[:2]
        for i in range(self.particle_num):
            self.particle_list[i].transition(frame, self.img_track, w, h)
        self.normal_weight()
        self.resample()
        bbox = self.show_predict(frame, display)
        # self.particle_list = particle_list
        return bbox

    def particle_cmp(self, p1,p2):
        if p1.weight < p2.weight:
            return 1
        elif p1.weight > p2.weight:
            return  -1
        return 0

    def show_predict(self, frame, display=False):
        '''
        Make a final step towards the prediction
        @param frame: the current frame
        @param display: if True, show with OpenCV the bbox

        @return bbox: a list that contains the coordinates of the bbox
        '''
        sx,sy,ss = 0,0,0
        self.particle_list.sort(key = cmp_to_key(lambda a,b : (b.weight - a.weight)))
        index = 0
        h, w = self.img_track.shape[:2]
        for p in self.particle_list[:2]:
            index += 1
            #print("index = ",index,p.weight)
            xtl = int(p.x -  w * p.s  * 0.5)
            ytl = int(p.y -  h * p.s * 0.5)
            xbr =  int(p.x + w * p.s  * 0.5)
            ybr = int(p.y +  h * p.s * 0.5)
            width = xbr - xtl
            height = ybr - ytl
            if display is True:
            #     ix, iy = self.ix, self.iy
            #     sx, sy = self.sx, self.sy
                cv2.rectangle(frame, (xtl, ytl), (xbr,ybr), (0,255,0), 1)
        return [xtl, ytl, width, height]





if __name__ == "__main__":
    # capture = cv2.VideoCapture("video/hockey.avi")
    os.chdir('/home/pro/')
    capture = cv2.VideoCapture('Documents/Project_INSIGHT/Particle Filters from Sites/Particle-Filter-video-tracking/video/OneLeaveShop1cor.mpg')
    is_destroy = False
    huang_obj = HuangPF()
    if capture.isOpened():
        while(True):
            start = time()
            ret,prev = capture.read()
            if ret == True:
                if is_cropped == False:
                    firstFrame = copy.deepcopy(prev)
                    the_firstFrame = copy.deepcopy(prev)
                    huang_obj.image_w, huang_obj.image_h = prev.shape[1],prev.shape[0]
                while(is_cropped == False):
                    cv2.namedWindow("choose_image",flags = 0)
                    cv2.resizeWindow('choose_image', 384, 288) 
                    cv2.setMouseCallback('choose_image', huang_obj.choose_frame)
                    # cv2.selectROI()
                    cv2.imshow('choose_image',firstFrame)
                    cv2.waitKey(1)&0xff
                if is_destroy == False :
                    cv2.destroyAllWindows()
                    is_destroy = True
                    particle_list = huang_obj.init_HuangPF()
                    w = huang_obj.width
                    h = huang_obj.height
                    xc = huang_obj.xc
                    yc = huang_obj.yc
                    cv2.rectangle(prev, (int(xc - w * 0.5), int(yc - 0.5 * h)), (int(xc + w * 0.5), int(yc + 0.5 * h)), (0,255,0), 1)
                    cv2.namedWindow('video', flags=0)  
                    cv2.resizeWindow('video', 384, 288) 
                    cv2.imshow('video',prev)
                else:
                    # f = copy.deepcopy(prev)
                    bbox = huang_obj.track_frame(prev, True)
                    # cv2.rectangle(prev, (-15,-37), (15,37), (255,255,255),3)
                    cv2.imshow('video',prev)
                    cv2.waitKey(1)
                    
            else:
                break
            end  = time() - start
            print ('Elapsed time: {}'.format(end))
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    capture.release()
    cv2.destroyAllWindows()