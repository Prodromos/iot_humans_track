#!/usr/bin/env python
import cv2
import numpy as np
import math
import copy
'''
This file contains the particle filter that was implemented for the Thesis with 

Title: Detection and Tracking of Humans in indoor environments using Multiple Cameras and IoT frameworks.

Author: Kampas Prodromos
Date: 15 March 2019
'''

class ParticleFilter():

    def __init__(self, N, frame_size, w_thres, cams_num):
        self.N = N
        self.id = -1
        self.weights = np.array([1.0/N]*N)
        self.w_thres = w_thres
        self.particles = np.empty((N,3))
        
        # X coord for the particles in 3D space.
        self.particles[:, 0] = np.random.uniform(0, frame_size[0], size=N)
        self.particles[:, 0] = [np.round(x,4) for x in self.particles[:,0]]
        
        # Y coord for the particles in 3D space.
        self.particles[:, 1] = np.random.uniform(0, frame_size[1], size=N)
        self.particles[:, 1] = [np.round(x, 4) for x in self.particles[:,1]]

        # Z coord for the particles in 3D spaces. Init them with random values 
        # each of which in a range of (0 - 2) meters, assuming that the world 
        # coordinates correspond to 0 depth.
        self.particles[:, 2] = np.random.uniform(0, 2, size=N)
        self.particles[:, 2] = [np.round(x, 4) for x in self.particles[:,2]]
        
        # XY center coordinates of the particles
        self.center = [-1, -1]
        
        # Center of the particles for the X, Y, Z (depth) coordinate
        self.x_depths = [0 for _ in range(cams_num)]
        self.y_depths = [0 for _ in range(cams_num)]
        self.z_depths = [0 for _ in range(cams_num)]

        self.world_center = [0, 0]# 0]

        self.world_frame_particles = np.zeros((N,3))

        # Flag that is used for the removal of the extra particle filters
        self.flag_none_bbox = 0

        self.last_cam = -1
        
    def update_xyz(self, std):
        '''
        Update the z coordinates of the particles for the particle filter.
        The conversion from pixels to meters, as represented in the 3D space,
        occurs also inside this function. This way, we do not care for the 
        initial z values of the particles (random initialization)

        @param std: the standard deviation we want to draw the particles from the z_center
        '''

        N = len(self.particles[:,2])

        z_mean = np.mean(self.z_depths)

        self.particles[:, 2] = np.random.normal(z_mean, std, (N))
        # self.particles[:, 2] = self.particles[:, 2].astype(int)

        # x_new_center = []
        # y_new_center = []

        # for x in range(len(self.x_depths)):
        #     if x != 0:
        #         x_new_center.append(self.x_depths[x])

        # for y in range(len(self.y_depths)):
        #     if y != 0:
        #         y_new_center.append(self.y_depths[y])

        # if len(x_new_center) > 0  and  len(y_new_center) > 0:
        # x_mean = np.mean(x_new_center)
        # y_mean = np.mean(y_new_center)

        # else:
        xs = self.x_depths
        ys = self.y_depths
        x_mean = np.mean(xs)
        y_mean = np.mean(ys)
        
        # if np.isnan(x_mean):
        #     x_mean = np.average(self.particles[:, 0], weights=self.weights, axis=0)
        # if np.isnan(y_mean):
        #     y_mean = np.average(self.particles[:, 1], weights=self.weights, axis=0)
           
           
        self.world_center = [x_mean, y_mean]#, z_mean]
        return [x_mean, y_mean]

 
    def predict(self, velocity, move_confidence, max_vel):
        """Predict the position of the point in the next frame.
        Move the particles based on how the real system is predicted to behave.
 
        The position of the point at the next time step is predicted using the 
        estimated velocity along X and Y axis and adding Gaussian noise sampled 
        from a distribution with MEAN=0.0 and STD=std. It is a linear model.
        
        @param velocity the velocity of the object. It is a list (vector of 2 or 3 coords,
            depending on where we use it.)
        
        @param move_confidence: depending on the confidence we preserve the detector/tracker 
            with, the one responsible for the velocity vector, there is an extra moving step 
            that takes place here. The particles move once more using another way.

        @param std the standard deviation of the gaussian distribution used to add noise
        """
        N = len(self.particles)

        if velocity == []  or  velocity is None:
            velocity = [0,0]
        
        velocity_x = velocity[0]
        velocity_y = velocity[1]

        # The maximum velocity results in having a tool to 
        # add randomness to the code 
        max_v = max_vel
        # x_std = np.random.random_integers(0, max_v)
        # y_std = np.random.random_integers(0, max_v)

        x_std = abs(np.random.normal(0, max_v))
        y_std = abs(np.random.normal(0, max_v))

        #To predict the position of the point at the next step we take the
        #previous position and we add the estimated speed and Gaussian noise
        x_mean = np.mean(self.x_depths)
        y_mean = np.mean(self.y_depths)

        x = np.mean(self.particles[:, 0])
        y = np.mean(self.particles[:, 1])

        if np.isnan(x):
            x = np.average(self.particles[:, 0], weights=self.weights, axis=0)
            if np.isnan(x):
                x=x_mean

        if np.isnan(y):
            y = np.average(self.particles[:, 1], weights=self.weights, axis=0)
            if np.isnan(y):
                y=y_mean

        center_diff = [abs(x_mean-x), abs(y_mean-y)]

        self.particles[:, 0] += ((move_confidence * velocity_x) + np.random.normal(0, x_std, (N)))#.round() #predict the X coord
        # self.particles[:,0] = self.particles[:, 0].astype(int)

        self.particles[:, 1] += ((move_confidence * velocity_y) + np.random.normal(0, y_std, (N)))#.round() #predict the Y coord
        # self.particles[:, 1] = self.particles[:, 1].astype(int)
        return None

    def update(self, bbox, acc_error):
        """Update the weights associated which each particle based on the (x,y) coords measured.
        Particles that closely match the measurements give an higher contribution.
 
        By measuring the euclidean distance of each particle from the center and 
        the distance of the point where the line that passes throught both the particle and the 
        boundary of the bbox the weights are being calculated. That's why at the beginning the
        gradient of these lines are calculated.

        @param bbox: the bbox that was detected/tracked
        @param acc_error: Integer. Number of extra iterations. The accuracy error with which
         the iterations through the desired rectangle in order to find the point at the boundary. 
        Unlike math on paper, the use of the computer comes with accuracy errors. 
        @return TODO
        """
        N = len(self.particles)

        grad = np.empty((N,1))
        dist_pc = np.empty((N,1))   # Distance of particles from center of the bbox
        dist_bc = [] #np.empty((N,1))   # Distance of boundary point that the particle-center line collides
        position = np.empty((N,2))  # Position of the center of the bbox

        bbox_xtl = bbox[0]
        bbox_ytl = bbox[1]
        
        bbox_xbr = bbox[0] + bbox[2]
        bbox_ybr = bbox[1] + bbox[3]
        
        bbox_width = bbox[2]
        bbox_height = bbox[3]

        # The center coordinates must be integers. They represent pixels
        bbox_xc = bbox[0] + bbox_width/2
        bbox_yc = bbox[1] + bbox_height/2

        position[:, 0].fill(bbox_xc)
        position[:, 1].fill(bbox_yc)

        # Gradients of the lines that connect the particles with the bbox center
        # They will be used to find the boundary at the bbox and save complexity
        x_pos = (self.particles[:,0] - position[:,0]) 
        x_pos[x_pos==0] = 1 / 100
        grad = (self.particles[:,1] - position[:,1]) / x_pos

        
        
        # dist_pc = np.sqrt((position[:,0] - self.particles[:,0])**2 + (position[:,1] - self.particles[:,1])**2)
        # print dist_pc
        
        dist_pc = np.linalg.norm(position - self.particles[:,0:2], axis=1)
        
        # print dist_pc
        
        # The projection coordinated that will be used in order to find the point
        # AT the bbox that the line passes through
        # p_xp1 = 0
        # p_yp1 = 0
        # p_xp2 = 0
        # p_yp2 = 0

        #Projections to the boundaries of the bbox
        A = [0, 0]
        B = [0, 0]
        
        # Find the best square inside the bbox to loop and find the correct boundary coordinates
        for i in range(N):
            if abs(bbox_xtl - self.particles[i][0]) < abs(bbox_xbr - self.particles[i][0]):
                A[0] = bbox_xtl
            else:
                A[0] = bbox_xbr
            A[1] = self.particles[i][1]

            if abs(bbox_ytl - self.particles[i][1]) < abs(bbox_ybr - self.particles[i][1]):
                B[1] = bbox_ytl
            else:
                B[1] = bbox_ybr
            B[0] = self.particles[i][0]


            xiters = abs(B[0] - A[0]) + acc_error
            yiters = abs(B[1] - A[1]) + acc_error
            x_init = min(A[0], B[0])
            y_init = min(A[1], B[1])

            # w_temp = 0
            # h_temp = 0
            # if A[0] == bbox_xtl  or  A[0] == bbox_xbr:
            #     x_init = A[0]
            # elif p_xp2 == 0  or  p_xp2 == bbox_xbr:
            #     w_temp = p_xp2

            # if p_yp1 == 0  or p_yp1 == bbox_ybr:
            #     h_temp = p_yp1
            # elif p_yp2 == 0  or  p_yp2 == bbox_ybr:
            #     h_temp = p_yp2
            x, y = 0, 0
            if grad[i] == np.inf  or  grad[i] == -np.inf:
                x = B[0]
                y = B[1]
                xo = bbox_xc
                yo = bbox_yc
                result = (x - xo)
                
            else:
                xres = 100
                M = [0,0]
                
                yres = 100
                Z = [0,0]
                
                flag = True
                # xr = []
                # yr = []
                for j in range(int(xiters)):
                    x = x_init + j
                    y = B[1]
                    xo = bbox_xc
                    yo = bbox_yc
                    result = y - yo - grad[i]*(x - xo)
                    # xr.append(result)
                    if xres > abs(result):
                        xres = abs(result)
                        M = [x,y]
                    # print result
                    if abs(result) < 0.1:
                        flag = False 
                        # print x, y
                        break

                if flag == True:
                    for j in range(int(yiters)):
                        x = A[0]
                        y = y_init + j
                        xo = bbox_xc
                        yo = bbox_yc
                        result = y - yo - grad[i]*(x - xo)
                        # yr.append(result)
                        if yres > abs(result):
                            yres = abs(result)
                            Z = [x,y]
                        # print result
                        if abs(result) < 0.1:
                            # print x, y
                            break
                
            if grad[i] == np.inf  or  grad[i] == -np.inf:
                bc = np.asarray([x,y])  # Point boundary from center    
            else:
                if xres < yres:
                    x, y = M
                else:
                    x, y = Z

                bc = np.asarray([x,y])  # Point boundary from center
            
            center = np.asarray([bbox_xc, bbox_yc])
            norm = np.linalg.norm(bc - center)   # Distance boundary from center of the bbox
            dist_bc.append(norm)
          
        
        dist_bc = np.asarray(dist_bc)
        # print dist_bc
        self.weights = 1 - (1-self.w_thres) * (dist_pc / dist_bc)
        self.weights[self.weights<0] = 0
        self.weights = np.round(self.weights, decimals=3)   # Round the weights to the 3rd decimal digit
        
        self.weights /= sum(self.weights)   #Normalise the weights
        # print self.weights
        return grad


    def resample(self, method='residual'):
        """Resample the particle based on their weights.
 
        The resempling (or importance sampling) draws with replacement N
        particles from the current set with a probability given by the current
        weights. The new set generated has always size N, and it is an
        approximation of the posterior distribution which represent the state
        of the particles at time t. The new set will have many duplicates 
        corresponding to the particles with highest weight. The resampling
        solve a huge problem: after some iterations of the algorithm
        some particles are useless because they do not represent the point 
        position anymore, eventually they will be too far away from the real position.
        The resample function removes useless particles and keep the
        useful ones. It is not necessary to resample at every epoch.
        If there are not new measurements then there is not any information 
        from which the resample can benefit. To determine when to resample 
        it can be used the returnParticlesContribution function.
        @param method the algorithm to use for the resampling.
            'multinomal' large weights are more likely to be selected [complexity O(n*log(n))]
            'residual' (default value) it ensures that the sampling is uniform across particles [complexity O(N)]
            'stratified' it divides the cumulative sum into N equal subsets, and then 
                selects one particle randomly from each subset.
            'systematic' it divides the cumsum into N subsets, then add a random offset to all the susets
        """
        N = len(self.particles)
        if(method == 'multinomal'):
            #np.cumsum() computes the cumulative sum of an array. 
            #Element one is the sum of elements zero and one, 
            #element two is the sum of elements zero, one and two, etc.
            cumulative_sum = np.cumsum(self.weights)
            cumulative_sum[-1] = 1. #avoid round-off error
            #np.searchsorted() Find indices where elements should be 
            #inserted to maintain order. Here we generate random numbers 
            #in the range [0.0, 1.0] and do a search to find the weight 
            #that most closely matches that number. Large weights occupy 
            #more space than low weights, so they will be more likely 
            #to be selected.
            indices = np.searchsorted(cumulative_sum, np.random.uniform(low=0.0, high=1.0, size=N))      
             
        elif(method == 'residual'):
            indices = np.zeros(N, dtype=np.int32)
            # take int(N*w) copies of each weight
            num_copies = (N*np.asarray(self.weights)).astype(int)
            k = 0
            for i in range(N):
                for _ in range(num_copies[i]): # make n copies
                    indices[k] = i
                    k += 1
            #multinormial resample
            residual = self.weights - num_copies     # get fractional part
            residual /= sum(residual)     # normalize
            cumulative_sum = np.cumsum(residual)
            cumulative_sum[-1] = 1. # ensures sum is exactly one
            indices[k:N] = np.searchsorted(cumulative_sum, np.random.random(N-k))
        
        elif(method == 'stratified'):
            #N subsets, chose a random position within each one
            #and generate a vector containing this positions
            positions = (np.random.random(N) + range(N)) / N
            #generate the empty indices vector
            indices = np.zeros(N, dtype=np.int32)
            #get the cumulative sum
            cumulative_sum = np.cumsum(self.weights)
            i, j = 0, 0
            while i < N:
                if positions[i] < cumulative_sum[j]:
                    indices[i] = j
                    i += 1
                else:
                    j += 1
        
        elif(method == 'systematic'):
            # make N subsets, choose positions with a random offset
            positions = (np.arange(N) + np.random.random()) / N
            indices = np.zeros(N, dtype=np.int32)
            cumulative_sum = np.cumsum(self.weights)
            i, j = 0, 0
            while i < N:
                if positions[i] < cumulative_sum[j]:
                    indices[i] = j
                    i += 1
                else:
                    j += 1
        else:
            raise ValueError("[ERROR] particle_filter.py: the resampling method selected '" + str(method) + "' is not implemented")
        #Create a new set of particles by randomly choosing particles 
        #from the current set according to their weights.
        self.particles[:, 0:2] = self.particles[indices, 0:2] #resample according to indices
        # print self.particles[:][0:1]
        # print self.particles[indices][0:1]  
        self.weights[:] = self.weights[indices]
        #Normalize the new set of particles
        self.weights /= np.sum(self.weights)        


    def get_Neff(self):
        """This function gives an estimation of the number of particles which are
        contributing to the probability distribution (also called the effective N). 
 
        This function gets the effective N value which is a good estimation for
        understanding when it is necessary to call a resampling step. When the particles
        are collapsing in one point only some of them are giving a contribution to 
        the point estimation. If the value is less than N/2 then a resampling step
        should be called. A smaller value means a larger variance for the weights, 
        hence more degeneracy
        @return get the effective N value. 
        """
        return 1.0 / np.sum(np.square(self.weights))

    def get_particles_coordinates(self, index=-1):
        """It returns the (x,y) coord of a specific particle or of all particles. 
 
        @param index: the position in the particle array to return
            when negative it returns the whole particles array
        @return a single coordinate (x,y) or the entire array
        """
        if(index<0):
            return self.particles.astype(int)
        else:
            return self.particles[index,:].astype(int)

    def draw_particles(self, frame, color=(0,0,255), radius=2):
        """
        Draw the particles on a frame and return it.
        @param frame the image to draw
        @param color the color in BGR format, ex: [0,0,255] (red)
        @param radius is the radius of the particles
        @return the frame with particles
        """
        for x_particle, y_particle in self.particles[:, 0:2].astype(int):
            cv2.circle(frame, (x_particle, y_particle), radius, color, -1) #RED: Particles
        
    
    def estimate_center(self):    
        """
        Estimate the position of the center point given the particle weights.
 
        This function get the mean and associated with the point to estimate.
        @return get the x_mean, y_mean
        """
        #Using the weighted average of the particles
        #gives an estimation of the position of the point
        # x_mean = np.average(self.particles[:, 0], weights=self.weights, axis=0)#.astype(int)
        # y_mean = np.average(self.particles[:, 1], weights=self.weights, axis=0)#.astype(int)

        x_mean = np.mean(self.particles[:, 0])
        y_mean = np.mean(self.particles[:, 1])

        if np.isnan(x_mean):
            x_mean = np.average(self.particles[:, 0], weights=self.weights, axis=0)
        
        if np.isnan(y_mean):
            y_mean = np.average(self.particles[:, 1], weights=self.weights, axis=0)
        
        self.center = [x_mean, y_mean]
        return [x_mean, y_mean]

    def estimate_rectangle(self, color=(0,255,0)):
        '''
        Estimate the rectangle that covers the object 
        that is being tracked. The function performs 
        this estimation by finding the width and height
        of the bbox using the estimate_center() and 
        the euclidean distance between the particles 
        and the center point.
        '''
        
        mean = np.average(self.particles[:, 0:2], weights=self.weights, axis=0)
        xc = int(mean[0])
        yc = int(mean[1])

        var  = np.average((self.particles[:, 0:2] - mean)**2, weights=self.weights, axis=0)
        x_var = int(var[0])
        y_var = int(var[1])
        
        width = int(np.sqrt(x_var))
        height = int(np.sqrt(y_var))

        xtl = (xc - width/2)
        ytl = (xc - height/2)

        xbr = (xc + width/2)
        ybr = (yc + height/2)

        # cv2.rectangle(frame, (xtl,ytl), (xbr,ybr), color, 2)

        return [xtl, ytl, width, height]

    def bbox_track(self, bbox, velocity, move_conf, max_vel, acc_error, resample_method):
        
        if bbox is None  or  len(bbox)==0:
            self.flag_none_bbox += 1
            
        self.predict(velocity, move_conf, max_vel)

        est_bbox = self.estimate_center()

        if bbox is None  or  len(bbox) != 4:  
            self.resample(resample_method)
        else:
            self.update(bbox, acc_error)
            self.resample(resample_method)
                    
        return est_bbox


    def track_point_3D(self, center_point, velocity, move_conf, max_vel, resample_method, sigma=0.25):
        
        if center_point is None  or  len(center_point)==0:
            self.flag_none_bbox += 1
            
        self.predict(velocity, move_conf, max_vel)

        est_bbox = self.estimate_center()

        if center_point is None  or  len(center_point) != 2:  
            self.resample(resample_method)
        else:
            self.update_particle_weights_world(center_point, sigma)
            self.resample(resample_method)
                    
        return est_bbox



    def Gaussian(self, mu, sigma, x):
	
	    # calculates the probability of x for 1-dim Gaussian with mean mu and var. sigma
	    return math.exp(- ((mu - x) ** 2) / (sigma ** 2) / 2.0) / math.sqrt(2.0 * math.pi * (sigma ** 2))


    def update_particle_weights_world(self, pf_center, sigma):
        dists = list()
        probs = list()

        for i in range(len(self.particles)):
            dist = np.linalg.norm(pf_center - self.particles[i, 0:2])
            dists.append(dist)

            prob = self.Gaussian(dist, sigma, 0)
            probs.append(prob)

        probs = np.asarray(probs)
        self.weights = probs
        # self.weights[self.weights<0.01] = 0.01
        # self.weights= np.round(self.weights, 3)

        self.weights /= sum(self.weights)


if __name__ == "__main__":
    pf = ParticleFilter(50, (300,300), 0.3, 1)
    bbox = [100, 100, 40, 80]
    grad = pf.update(bbox, 40)
    # print pf.particles
    print grad