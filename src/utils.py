#!/usr/bin/env python

from cv_bridge import CvBridge, CvBridgeError
from datetime import datetime
import os

import matplotlib.pyplot as plt

# Create a Table with all the metrics of the evaluation
import plotly.plotly as py
import plotly.graph_objs as go 
import plotly

import csv
import pandas as pd
import glob

import numpy as np

import cv2

class ImgConvOpenCVRos (object):
    """
    This class is responisible for the conversion of OpenCV format images to ROS format
    images in order to be sent with the ROS Kinetic from the RPI3s to the mainframe.
    
    There are two function in this class: 
        __init__ : for the creation of an instance of CvBridge Class
        conversion : converts the OpenCV img to ROS img by calling the function cv2_to_imgmsg() 
    """
    
    def __init__ (self):
        self.bridge = CvBridge ()

    def conv_to_ros (self, img):
        try:
            return self.bridge.cv2_to_imgmsg (img, encoding='bgr8')
        except CvBridgeError as e:
            print (e)

    def conv_to_opencv (self, img):
        try:
            return self.bridge.imgmsg_to_cv2 (img, 'bgr8') 
        except CvBridgeError as e:
            print (e)

class Utils():
    '''
    A class implemented for the needs of the whole Project.
    There are many functions implemented. 
    '''
    def __init__(self):
        pass
            
    def set_img_name (self, path, img_format):
        '''
        This method uses the path of the desired store space for the images
        and the desired img_format (e.g. .png) and with the current datetime
        saves the images
        '''
        dt = datetime.now()
        return path + str(dt) + img_format


    def folder_check (self, path):
        '''
        @param path: the path of the specific directory.

        return: The function returns TRUE if the folder already exists or
                if it is created successfully.
        '''
        if not os.path.exists (path):
            try:
                os.makedirs (path)
            except OSError:
                return False
        return True


    def draw_scatter_dists(self, data_list, title, xlabel, ylabel, path, save=False):
        '''
        Draw a scatter plot using matplotlib

        @param data_list: it can be a list or a list of lists. If it is a list of lists, 
                        each list will be plot across the y axis for only 1 x point.
        
        @param title: the title of the plot
        @param xlabel: the label at the x axis
        @param ylabel: the label at the y axis
        @param save: boolean, whether or not to save the figure
        '''
        fig = plt.figure()
        ax = fig.add_subplot(111)
               
        colors = ['red']# 'blue', 'green', 'purple', 'black', 'yellow']
        
        for i in range(len(data_list)):
            x_axis = [i] *len(data_list[i])
            data = [float(x) for x in data_list[i]]
            ax.scatter(x_axis, data)        #label=str(len(data_list[i])))
        
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if save == True:
            plt.savefig(path + title + '.jpg')
        else:
            plt.show()
        #  plt.legend()
        
        return None


    def plotly_auth(self):
        '''
        Authentication for the plotly platform
        '''
        plotly.tools.set_credentials_file(username='protesla', api_key='7XqiRT8jffPzHn8xWq2v')


    def draw_table_plotly(self, filename, data, labels, align, header_color, cell_color, technique='pandas'):
        '''
        Draw a table using plotly online
        
        @param filename: the name of the file of the plot
        
        @param data: the data to plot
        
        @param labels: the labels for the plot. Must be a list
        
        @param header_color: the color of the header
        
        @param cell_color the color of every cell of the plot
        
        @param technique: the technique that is going to be used. If data
                          come from a Pandas Dataframe don't do anything. It's default.
                          Otherwise, type something.
        :return the plot data
        '''
        if technique == 'pandas':
            trace = go.Table( header=dict( values=list(data.columns),
                                           fill = dict(color=header_color),
                                           align = [align] * len(labels)),
                              cells=dict(values=np.asarray(data).T,
                                         fill = dict(color=cell_color),
                                         align = [align] * len(labels)))
        else:
            trace = go.Table( header=dict( values=list(data[0]),
                                           fill = dict(color=header_color),
                                           align = [align] * len(labels)),
                              cells=dict(values=np.asarray(data[1:]).T,
                                         fill = dict(color=cell_color),
                                         align = [align] * len(labels)))            
        plot_data = [trace]  
        py.plot(plot_data, filename=filename)  
        
        return plot_data 


    def csv_write(self, filename, mode, data, header=[]):
        '''
        Write a csv file
        
        @param filename: the name of the file (whole PATH)
        @mode: use 'w' for write a file, 'a' to append etc
        @param data: the data to write at the csv file
        @param header: the header of the data. If empty, skip the write
        
        :return None
        '''

        with open(filename, mode) as fl:
            writer = csv.writer(fl)
            if header != []:
                writer.writerow(header)
            if len(data) == 0:
                fl.close()
                return None
            
            if type(data[0]) == list  or  type(data[0]) == dict:
                writer.writerows(data)
            else:
                writer.writerow(data)    

            fl.close()

        return None


    def read_files (self, path, extension):
        '''
        Read multiple files from a specific PATH with a specific extension
        (e.g. '.csv') The files that have been successfully read are sorted
        according to their names.

        @param path: The PATH to the files. The path must end with a '/' 
                    character.

        @param extension: the extension of the file, e.g. '.csv'

        :return a sorted list of the names of the files with the desired extension
        '''

        file_list = []
        for fl in glob.glob(path + '*' + extension):
            file_list.append(fl)
        file_list.sort()
        return file_list


    def csv_read(self, path, technique, header):
        '''
        Read multiple csv files from a specific PATH

        @param path: The PATH to the csv files. The path must end with a '/' 
                    character.

        @param technique: set to 'pandas' if user wants to return a Dataframe from the 
                          csv file. Otherwise, the normal method will be used with csv module

        @param header: header for the Pandas Dataframe. Same values as for pandas. 
                    However, since many columns are not full and not empty either,
                    the Dataframe technique cannot be used for this type of 
                    csv files.

        :return a list of Pandas Dataframes, one for each csv file
                OR a list of lists, each of which contain the information from
                the csv files.
        '''
        
        csv_list = self.read_files(path, '.csv')
        df_list = []
        for csv_file in csv_list:
            if technique == 'pandas':
                df_list.append(pd.read_csv(csv_file, header=header))
            else:
                temp_list = []
                with open(csv_file) as csv_rd:
                    csvReader = csv.reader(csv_rd)
                    for row in csvReader:
                        temp_list.append(row)
                    df_list.append(temp_list)
        return df_list, csv_list


    def str_search (self, str_list, str_key):

        '''
        Search a string in a list of strings
        and return a list of integers with the positions
        that the strings have been found

        @param str_list: a list comprised of strings

        @param str_key: the string to search

        :return a list of the positions of the indices
                the string was found
        '''
        indices_list = []
        for string in str_list:
            if string.count(str_key) > 0:
                indices_list.append(str_list.index(string))
            else:
                continue
        
        return indices_list

    
    def crop_img(self, frame, bbox):
        '''
        Crop an image at specific coordinates
        @param frame: the image
        @param bbox: the bounding box to crop the image
        '''
        bbox_img = frame[bbox[1]: bbox[1]+bbox[3], bbox[0]: bbox[0]+bbox[2]]
        return bbox_img

    
    def remove_Nones(self, arr):
        '''
        Remove None Values from lists
        '''
        while 1:
            try:
                arr.remove(None)
            except:
                break
        return arr


    def find_mse_std(self, data):
        ''' Find the Mean Square Error and the StD of data
        '''
        mses = list()
        stds = list()
        mse_std = list()
        
        for row in data:
            row = [float(x) for x in row]
            arr = np.asarray(row)
            std = np.std(arr, axis=0)
            mse = 0
            for dist in row:
                mse += dist ** 2
            mse = np.sqrt(mse)
            mse /= len(row)
            mse = np.round(mse, 3)
            std = np.round(std, 3)
            mse_std.append([mse, std])
            mses.append(mse)
            stds.append(std)
        return mses, stds, mse_std



    def convert_imgs_to_video (self, imgs_path, img_extension, 
                               out_path, out_name, out_fps, out_extension,
                               resize=None, resize_method='normal'):
        '''
        Convert Images to Video Stream. It is easier to manipulate a video with OpenCV than
        to use Images as an Input for the Dataset.

        @param imgs_path: the path where the images are located
        
        @param img_extension: the extension of the images to be converted. All images must be of the same extension.

        @param out_path: the path to save the video.
        
        @param out_name: the name of the video.

        @param out_extension: the Encoding of the Video.

        @param resize: Typically it will be an Unisigned Integer or list of 2 uint Numbers to resize the Images.

        @param resize_method: - If 'normal' resize must be list of 2 numbers (width, height).
                              - If 'width' resize must be a number that corresponds to widht
                              - If 'height' resize must be a number that corresponds to height
        '''
        imgs_names = self.read_files(imgs_path, img_extension)
        
        if len(imgs_names) == 0:
            ValueError('[ERROR]   No Images in Current Folder with specific extension')

        img = cv2.imread(imgs_names[0])
        
        if resize is None:
            size = (img.shape[1], img.shape[0])
        else:
            img = self.img_resize(img, resize, resize_method)
            size = (img.shape[1], img.shape[0])    
        out = cv2.VideoWriter(out_path + out_name + out_extension,cv2.VideoWriter_fourcc(*'MJPG'), out_fps, size)

        # imgs = list()
        for img_name in imgs_names:
            img = cv2.imread(img_name)
            if resize is not None:
                img = self.img_resize(img, resize, resize_method)
            # imgs.append(img)
            out.write(img)
        


        # out = cv2.VideoWriter(out_path + out_name + out_extension,cv2.VideoWriter_fourcc(*'MJPG'), out_fps, size)

        # for img in imgs:
            # out.write(img)

        out.release()



    def img_resize (self, img, resize_dim, method='normal'):
        '''
        Resize method for Images. Something like the imutils Library.
        The function takes caution of the aspect ratio, in case the user
        wants only to resize the Image only for width or height.

        - For advanced users, who know what they are doing, they can use
          the normal method and a resize list that corresponds to (width, height)
        '''

        img_h, img_w = img.shape[:2]

        if len(resize_dim) == 2  and  method == 'normal':
            return cv2.resize(img, resize_dim, interpolation = cv2.INTER_AREA)
            
        elif len(resize_dim) == 1  and  method == 'width':
            r = float(resize_dim[0]) / img_w
            dims = (resize_dim[0], int(img_h * r))
            return cv2.resize(img, dims, interpolation = cv2.INTER_AREA)

        elif len(resize_dim) == 1  and  method == 'height':
            r = resize_dim[0] / img_h
            dims = (int(img_w * r), resize_dim[0])
            return cv2.resize(img, dims, interpolation = cv2.INTER_AREA)
            
        else:
            ValueError('[ERROR]   img_resize function Not Proper Args.')


import os
import getpass

if __name__ == "__main__":

    # fx_0 = 557.0
    # fy_0 = 555.0

    # fovx_0 = 2 * np.arctan(1024 / (2 * fy_0))
    # fovy_0 = 2 * np.arctan(768 / (2 * fx_0))
    
    # print np.cos(fovx_0)
    # print np.cos(fovy_0)

    # fovx_0 = 2 * np.arctan(512 / (fy_0))
    # fovy_0 = 2 * np.arctan(384 / (fx_0))
    
    username = getpass.getuser()
    os.chdir('/home/' + username)
    print os.getcwd()     
    
    utils = Utils()
    img_extension = '.jpeg'

    cam_path = 'Documents/Project_INSIGHT/data/Datasets/VIPT_2014_Dataset/test_set/cam'

    cam_paths = [cam_path + str(i) for i in range(4)]

    imgs_paths = [cam_path + str(i) + '/all_imgs/' for i in range(4)]

    out_fps = 15
    out_extension = '.avi'
    
    for i, imgs_path in enumerate(imgs_paths):
        out_path = cam_paths[i] 
        out_name = '/cam' + str(i)

        utils.convert_imgs_to_video(imgs_path, img_extension, 
                                    out_path, out_name, out_fps, out_extension, [512], 'width')         
        