#!/usr/bin/env python

import numpy as np 
import math 

# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-5
 
 
# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

import os
os.chdir('/home/pro/Documents/hta0-horizontal-robot-arm')
print os.getcwd()

savedir="camera_data/"

cam_mtx=np.load(savedir+'cam_mtx.npy')
dist=np.load(savedir+'dist.npy')
newcam_mtx=np.load(savedir+'newcam_mtx.npy')
roi=np.load(savedir+'roi.npy')
rvec1=np.load(savedir+'rvec1.npy')
tvec1=np.load(savedir+'tvec1.npy')
R_mtx=np.load(savedir+'R_mtx.npy')
Rt=np.load(savedir+'Rt.npy')
P_mtx=np.load(savedir+'P_mtx.npy')

s_arr=np.load(savedir+'s_arr.npy')
scalingfactor=s_arr[0]

inverse_newcam_mtx = np.linalg.inv(newcam_mtx)
inverse_R_mtx = np.linalg.inv(R_mtx)

# cam0_T = np.asarray([0.15, 5.78, 2.41946])

def calculate_XYZ(self,u,v):
                                    
    #Solve: From Image Pixels, find World Points
    XYZ = []
    uv_1 = np.array([[u,v,1]], dtype=np.float32)
    uv_1 = uv_1.T
    suv_1 = scalingfactor*uv_1
    xyz_c = inverse_newcam_mtx.dot(suv_1)
    xyz_c = xyz_c - tvec1
    XYZ = inverse_R_mtx.dot(xyz_c)

    return XYZ

if __name__ == "__main__":
    # cam0_R = [[0.751007,  0.659122,  0.0393187],
    #             [0.339899, -0.436961,  0.832786],
    #             [0.566089, -0.612064, -0.552196]]

    # cam1_R = [[0.649079,  -0.76019,  -0.0284345],
    #             [-0.380654, -0.356925,  0.853058],
    #             [-0.658635, -0.542878, -0.521042]]

    # cam2_R = [[-0.691322,  0.722512,  0.0070904],
    #             [0.315394,   0.29292,   0.902621],
    #             [0.650078,   0.626238, -0.430378]]

    # cam3_R = [[-0.70727,  -0.70693,  -0.00436166],
    #             [-0.371342,  0.366256,  0.853206],
    #             [-0.60156,   0.605067, -0.521555]]

    # cam0_R = [[-0.642896,  0.387548,  -0.660675],
    #             [0.765514, 0.354316,  -0.537073],
    #             [0.0259463, -0.851039, -0.524462]]

    # R0 = np.asarray(cam0_R)
    # R1 = np.asarray(cam1_R)
    # R2 = np.asarray(cam2_R)
    # R3 = np.asarray(cam3_R)
    
    

    # euler0 = rotationMatrixToEulerAngles(R0)
    # euler1 = rotationMatrixToEulerAngles(R1)
    # euler2 = rotationMatrixToEulerAngles(R2)
    # euler3 = rotationMatrixToEulerAngles(R3)

    # print euler0
    # print euler1
    # print euler2
    # print euler3


    cam2_R = [  [0.685076,  -0.317305,  0.655735],
                [-0.728452, -0.291716,  0.619887],
                [-0.00540459, -0.902341, -0.430988]]

    cam1_R = [  [-0.642896,  0.387548,  -0.660675],
                [0.765514, 0.354316,  -0.537073],
                [0.0259463, -0.851039, -0.524462]]

    
    cam3_R = [  [0.711514,  0.365859,  -0.599912],
                [0.702645,   -0.377943,   0.602868],
                [-0.00616784,   -0.850474, -0.525981]]

    cam0_R = [  [-0.755708,  -0.346367,    0.55582],
                [-0.654186,   0.439103,   -0.615816],
                [-0.0307636,   -0.828986, -0.558422]]

    R0 = np.asarray(cam0_R)
    R1 = np.asarray(cam1_R)
    R2 = np.asarray(cam2_R)
    R3 = np.asarray(cam3_R)

    euler0 = rotationMatrixToEulerAngles(R0)
    euler1 = rotationMatrixToEulerAngles(R1)
    euler2 = rotationMatrixToEulerAngles(R2)
    euler3 = rotationMatrixToEulerAngles(R3)

    # euler0 = rotationMatrixToEulerAngles(R0)
    # euler1 = rotationMatrixToEulerAngles(R1)
    # euler2 = rotationMatrixToEulerAngles(R2)
    # euler3 = rotationMatrixToEulerAngles(R3)

    inverse_R0 = np.linalg.inv(R0)

    cam0_T = np.asarray([0.15, 5.78, 2.41946])


    cam0_P = [[ 557.27,          0       , 505.582, 0.15],
              [ 0  , 557.27 * 0.999916, 403.742, 5.78],
              [ 0  ,   0              ,    1   ,  0]]
    
    cam0_P = np.asarray(cam0_P)  

    xyz1 = np.array([250, 250, 80, 1], dtype=np.float32)
    xyz1 = xyz1.T

    suv2 = cam0_P.dot(xyz1)
    
    
    print suv2
    scale = suv2[2]
    print scale

    img_points = suv2 / scale
    print img_points

    uv_1=np.array([500,600,1], dtype=np.float32)
    uv_1=uv_1.T
    suv_1=scale*uv_1
    xyz_c=inverse_newcam_mtx.dot(suv_1)
    xyz_c= xyz_c - cam0_T
    XYZ= inverse_R0.dot(xyz_c)
    print '\n\n'
    print euler0
    print euler1
    print euler2
    print euler3
