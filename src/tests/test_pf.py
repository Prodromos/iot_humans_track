#!/usr/bin/env python

# press ESC to exit the demo!
# from pfilter import ParticleFilter, gaussian_noise, squared_error, independent_sample
import numpy as np

# testing only
# from scipy.stats import norm, gamma, uniform
# import skimage.draw
import cv2

import os
import getpass


import sys
sys.path.append(sys.path[0] + "/..")

from mc_system.mc_pf_3D import ParticleFilter3D 
from mc_system.mc_config import *
from mc_system.mc_datasets_eval import VIPT_2014
import tf
import geometry_msgs.msg 
import rospy

from particle_filters.particle_filter import ParticleFilter

if __name__ == "__main__":

    # TOPIC Subscription, Node creation, parameters fill
    pub_topic = CAM_TOPIC_PF_3D_POINTS
    pub_node = CAM_PUBLISH_NODE + str(0)
    pub_msg_type = 0
    pub_queue_size = 500

    sub_topic = CAM_TOPIC_DT_FRAME_ANALYSIS[0]
    sub_node = CAM_SUBSCRIBE_NODE + str(0)
    sub_msg_type = 0
    sub_queue_size = 500

    trans = None
    rot = None


    vipt_2014 = VIPT_2014()

    pf_3d = ParticleFilter3D(sub_topic, sub_node, sub_msg_type, sub_queue_size, 
                             pub_topic, pub_node, pub_msg_type, pub_queue_size,
                             trans, rot, 'cam' + str(0), vipt_2014)

    pf_3d.cam_num = 1
    pf_3d.cam = 'cam1'


    cam_translation = vipt_2014.cams_translation[0]
    
    cam_euler = vipt_2014.cams_euler[0]

    cam_quaternion_tmp = tf.transformations.quaternion_from_euler(cam_euler[0], cam_euler[1], cam_euler[2])
    cam_quaternion = (cam_quaternion_tmp[0], cam_quaternion_tmp[1], cam_quaternion_tmp[2], cam_quaternion_tmp[3])
    
    pf_3d.rotation = cam_quaternion
    pf_3d.translation = cam_translation

    tf = pf_3d.set_transform('world', 'cam0')

    pf_2d = ParticleFilter(10, (512,368), 0.3, 1)
    
    pf_3d.pf_3d_objs.append(pf_2d)

    # Get the center of the pf
    pf_2d_center = pf_2d.estimate_center()
    pf_2d_center = [x for x in pf_2d_center]
    pf_2d_center = [400,300]
    xyz = pf_3d.get_camX_3D_coords_from_2d(pf_2d_center, 'cam0')


    # convert the point to the world frame (at last)
    # Transform the 3D points from the camera frame to camX coordinates
    pmsg = geometry_msgs.msg.PointStamped()
    pmsg.header.frame_id = CAM_X
    
    # Center Point of particle filter to world frame coords 

    pmsg.point.x = xyz[0]
    pmsg.point.y = xyz[1]
    pmsg.point.z = xyz[2]
    # pmsg.point.z = 1.0

    # Transform the unit vector to the camX frame. The transformation to world coordinates will 
    # occur later (not in pixels, as here, but in meters S.I.)
    world_xyz_point = tf.transformPoint('world', pmsg)
    
    # Update the z coord (z_depth) center point of the particles
    pf_3d.pf_3d_objs[0].x_depths[0] = world_xyz_point.point.x
    pf_3d.pf_3d_objs[0].y_depths[0] = world_xyz_point.point.y
    pf_3d.pf_3d_objs[0].z_depths[0] = world_xyz_point.point.z

    # Update the z coordinates of the particles according to its center            
    pf_2d.update_xyz(0.1)

    world_point = [world_xyz_point.point.x, world_xyz_point.point.y, world_xyz_point.point.z]

    # pf_3d.get_bboxes_and_centers_at_camX([[200,200,80,80]], 'cam0')
    