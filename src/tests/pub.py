#!/usr/bin/env python

import numpy as np

if __name__ == "__main__":
    z_camX = 2.41946
   
    center_point = [200,200]
    
    fx_0 = 557.27 / 2.0
    fy_0 = (557.27 * 0.999916) / 2.0

    fovx_0 = 2 * np.arctan(512.0 / (2 * fx_0))
    fovy_0 = 2 * np.arctan(368.0 / (2 * fy_0))

    print '\nField of View'
    print np.degrees(fovx_0)
    print np.degrees(fovy_0)

    theta = 0.8
    print '\ntheta angle'
    print np.degrees(theta)

    z = z_camX / abs(np.cos(theta))
  
    # z after the bbox (small part)
    z_to_subtract = 0.85 / abs(np.cos(theta))

    z = z - z_to_subtract

    # The z in meters
    print '\nz'
    print z
    # Convert the pixels we use here relative to camera center
    center_point[0] = center_point[0] - 512.0 / 2.0
    center_point[0] = int(center_point[0])

    center_point[1] = center_point[1] - 368.0 / 2.0
    center_point[1] = int(center_point[1])

    phi_x = (center_point[0] / (512 / 2.0)) * (fovx_0 / 2.0)
    phi_y = (center_point[1] / (368 / 2.0)) * (fovy_0 / 2.0)

    print '\nphix, phiy'
    print np.degrees(phi_x)
    print np.degrees(phi_y)
    
    if abs(np.cos(theta)) < 1e-3:
        x = 4.75
        y = 5.92
    else:
        # x = (z / np.cos(theta_z) ) * (np.cos(phi_x))
        x = (z) * np.sin(theta) * np.sin(phi_x)
        # x = abs(x)

        # y = (z / np.cos(theta_z) ) * (np.cos(phi_y))
        y = (z) * np.sin(phi_y)
        # y = abs(y)

    xyz = [x,y,z]
    print '\nxyz'
    print xyz