#!/usr/bin/env python

import numpy as np
from math import exp, sqrt, pi

def resample(method, particles, weights):
	"""Resample the particle based on their weights.

	The resempling (or importance sampling) draws with replacement N
	particles from the current set with a probability given by the current
	weights. The new set generated has always size N, and it is an
	approximation of the posterior distribution which represent the state
	of the particles at time t. The new set will have many duplicates 
	corresponding to the particles with highest weight. The resampling
	solve a huge problem: after some iterations of the algorithm
	some particles are useless because they do not represent the point 
	position anymore, eventually they will be too far away from the real position.
	The resample function removes useless particles and keep the
	useful ones. It is not necessary to resample at every epoch.
	If there are not new measurements then there is not any information 
	from which the resample can benefit. To determine when to resample 
	it can be used the returnParticlesContribution function.
	@param method the algorithm to use for the resampling.
		'multinomal' large weights are more likely to be selected [complexity O(n*log(n))]
		'residual' (default value) it ensures that the sampling is uniform across particles [complexity O(N)]
		'stratified' it divides the cumulative sum into N equal subsets, and then 
			selects one particle randomly from each subset.
		'systematic' it divides the cumsum into N subsets, then add a random offset to all the susets
	"""
	N = len(particles)
	if(method == 'multinomal'):
		#np.cumsum() computes the cumulative sum of an array. 
		#Element one is the sum of elements zero and one, 
		#element two is the sum of elements zero, one and two, etc.
		cumulative_sum = np.cumsum(weights)
		cumulative_sum[-1] = 1. #avoid round-off error
		#np.searchsorted() Find indices where elements should be 
		#inserted to maintain order. Here we generate random numbers 
		#in the range [0.0, 1.0] and do a search to find the weight 
		#that most closely matches that number. Large weights occupy 
		#more space than low weights, so they will be more likely 
		#to be selected.
		indices = np.searchsorted(cumulative_sum, np.random.uniform(low=0.0, high=1.0, size=N))      
			
	elif(method == 'residual'):
		indices = np.zeros(N, dtype=np.int32)
		# take int(N*w) copies of each weight
		num_copies = (N*np.asarray(weights)).astype(int)
		k = 0
		for i in range(N):
			for _ in range(num_copies[i]): # make n copies
				indices[k] = i
				k += 1
		#multinormial resample
		residual = weights - num_copies     # get fractional part
		residual /= sum(residual)     # normalize
		cumulative_sum = np.cumsum(residual)
		cumulative_sum[-1] = 1. # ensures sum is exactly one
		indices[k:N] = np.searchsorted(cumulative_sum, np.random.random(N-k))
	

        #Create a new set of particles by randomly choosing particles 
        #from the current set according to their weights.
        particles[:, 0:2] = particles[indices, 0:2] #resample according to indices
        # print self.particles[:][0:1]
        # print self.particles[indices][0:1]  
        weights[:] = weights[indices]
        #Normalize the new set of particles
        weights /= np.sum(weights) 

	return particles, weights


def predict(velocity, move_confidence, max_vel, particles):
	"""Predict the position of the point in the next frame.
	Move the particles based on how the real system is predicted to behave.

	The position of the point at the next time step is predicted using the 
	estimated velocity along X and Y axis and adding Gaussian noise sampled 
	from a distribution with MEAN=0.0 and STD=std. It is a linear model.
	
	@param velocity the velocity of the object. It is a list (vector of 2 or 3 coords,
		depending on where we use it.)
	
	@param move_confidence: depending on the confidence we preserve the detector/tracker 
		with, the one responsible for the velocity vector, there is an extra moving step 
		that takes place here. The particles move once more using another way.

	@param std the standard deviation of the gaussian distribution used to add noise
	"""
	N = len(particles)

	if velocity == []  or  velocity is None:
		velocity = [0,0]
	
	velocity_x = velocity[0]
	velocity_y = velocity[1]

	# The maximum velocity results in having a tool to 
	# add randomness to the code 
	max_v = max_vel
	# x_std = np.random.random_integers(0, max_v)
	# y_std = np.random.random_integers(0, max_v)

	x_std = abs(np.random.normal(0, max_v))
	y_std = abs(np.random.normal(0, max_v))

	#To predict the position of the point at the next step we take the
	#previous position and we add the estimated speed and Gaussian noise
	particles[:, 0] += ((move_confidence * velocity_x) + np.random.normal(0, x_std, (N))) #predict the X coord
	# self.particles[:,0] = self.particles[:, 0].astype(int)

	particles[:, 1] += ((move_confidence * velocity_y) + np.random.normal(0, y_std, (N))) #predict the Y coord
	# self.particles[:, 1] = self.particles[:, 1].astype(int)
	return particles


def estimate_center(particles):    
	"""
	Estimate the position of the center point given the particle weights.

	This function get the mean and associated with the point to estimate.
	@return get the x_mean, y_mean
	"""
	#Using the weighted average of the particles
	#gives an estimation of the position of the point
	x_mean = np.average(particles[:, 0], weights=weights, axis=0)
	y_mean = np.average(particles[:, 1], weights=weights, axis=0)

	center = [x_mean, y_mean]
	return [x_mean, y_mean]


def track_point_3D(particles, center_point, velocity, move_conf, max_vel, resample_method, sigma=0.25):
	
	particles = predict(velocity, move_conf, max_vel, particles)

	est_bbox = estimate_center(particles)

	weights = update(particles, center_point, sigma)
	particles, weights = resample(resample_method, particles, weights)
				
	return est_bbox, particles, weights


def update(particles, pf_center, sigma):

	dists = list()
	probs = list()
	for i in range(len(particles)):
		dist = np.linalg.norm(pf_center - particles[i, 0:2])
		dists.append(dist)

		prob = Gaussian(dist, 0.2, 0)
		probs.append(prob)

	print dists

	print '\n\n'

	# print probs

	weights = np.asarray(probs)
	weights[weights<1e-4] = 0
		
	weights /= sum(weights)   #Normalise the weights
	weights = np.round(weights, decimals=5)   # Round the weights to the 3rd decimal digit
	
	print '\n\n'
	print weights

	return weights


def Gaussian(mu, sigma, x):
	
	# calculates the probability of x for 1-dim Gaussian with mean mu and var. sigma
	return exp(- ((mu - x) ** 2) / (sigma ** 2) / 2.0) / sqrt(2.0 * pi * (sigma ** 2))


import matplotlib.pyplot as plt
import time

if __name__ == "__main__":
	N = 100
	particles = np.empty((N,3))
	weights = np.array([1.0/N]*N)

	particles[:, 0] = np.random.uniform(0, 4.5, size=N)
	particles[:, 0] = [np.int(x) for x in particles[:,0]]

	particles[:, 1] = np.random.uniform(0, 5.5, size=N)
	particles[:, 1] = [np.int(x) for x in particles[:,1]]

	particles[:, 2] = np.random.uniform(0, 2.0, size=N)
	particles[:, 2] = [np.int(x) for x in particles[:,2]]

	print particles

	pf_centers = [[2.40, 2.10], [2.38, 2.1], [2.36, 2.13], [2.38, 2.15],
				  [2.34, 2.10], [2.25, 1.9], [2.28, 1.86], [2.30, 1.80],
				  [2.19, 1.91], [2.20, 1.9]]
	
	for pf_center in pf_centers:
		est_center, particles, weights = track_point_3D(particles, pf_center, [0,0], 0.9, 0.2, 'multinomal', 0.25)

		x = particles[:,0]
		x = np.round(x, 5)
		y = particles[:,1]
		y = np.round(y, 5)
		
		print est_center

		plt.scatter(x, y)
		plt.axis([-20, 20, -20, 20])
		plt.show()


		start = time.time()
		while(time.time() - start < 2.0):
			pass