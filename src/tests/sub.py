#!/usr/bin/env python

import rospy
from iot_humans_track.msg import int_array2d

def callback(msg):
    print msg.data
    # print msg.elems
    # print msg.data

if __name__ == "__main__":
    rospy.init_node('sub', anonymous=True)
    sub_node = rospy.Subscriber('hog', int_array2d, callback, queue_size=10) 
    rospy.spin()