#!/usr/bin/env python

# USAGE
# python real_time_object_detection.py --prototxt MobileNetSSD_deploy.prototxt.txt --model MobileNetSSD_deploy.caffemodel

# import the necessary packages
import numpy as np
import imutils
import time
import cv2
import sys

from pub_detector import PubDetector
# from config import SSD_PATH, SSD_VIDEO_ENCODING, SSD_OUTPUT
SSD_PATH = "Documents/Project_INSIGHT/mobile_net_ssd/"
SSD_OUTPUT = "Documents/Project_INSIGHT/output/ssd/"
SSD_VIDEO_ENCODING = ".avi"

#TODO Delete this
VIDEO = 'Documents/Project_INSIGHT/data/Datasets/Videos/ShoppingMallPortugal/EnterExitCrossingPaths1cor.mpg'


class MobileNetSSD(PubDetector):
    '''
    Use the ssd algorithm to detect humans 
    '''
    # initialize the list of class labels MobileNet SSD was trained to
    # detect, then generate a set of bounding box colors for each class
    
    def __init__(self, mobile_net_path):
        super(MobileNetSSD, self).__init__()

        self.labels = ["background", "aeroplane", "bicycle", "bird", "boat",
                       "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                       "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                       "sofa", "train", "tvmonitor"]

        self.colors = np.random.uniform(0, 255, size=(len(self.labels), 3))

        # load our serialized model from disk
        # print("[INFO] loading model...")
        self.net = cv2.dnn.readNetFromCaffe(mobile_net_path + 'MobileNetSSD_deploy.prototxt.txt',
                                            mobile_net_path + 'MobileNetSSD_deploy.caffemodel')

    def ssd_detection_frame(self, frame, ssd_confidence, display=False):
        '''
        The ssd detection algorithm by PyImgSearch a bit modified

        @param ssd_confidence: the confidence for the bboxes
        @param display: set to True to diplay the frames.
        @return bboxes
        '''

        if display:
            start = time.time()
        frame_bboxes = []
        # frame = imutils.resize(frame, width=400)

        # grab the frame dimensions and convert it to a blob
        (h, w) = frame.shape[:2]
        # These data are prepared from the MobileNetSSD Model.
        blob = cv2.dnn.blobFromImage(cv2.resize(
            frame, (300, 300)),	0.007843, (300, 300), 127.5)

        # pass the blob through the network and obtain the detections and
        # predictions
        self.net.setInput(blob)
        detections = self.net.forward()

        # loop over the detections
        for i in np.arange(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with
            # the prediction
            confidence = detections[0, 0, i, 2]

            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence > ssd_confidence:
                # extract the index of the class label from the
                # `detections`, then compute the (x, y)-coordinates of
                # the bounding box for the object
                idx = int(detections[0, 0, i, 1])
                if self.labels[idx] != 'person':
                    continue

                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                frame_bboxes.append([startX, startY, endX-startX, endY-startY])

                if display is True:
                    # draw the prediction on the frame
                    label = "{}: {:.2f}%".format(self.labels[idx],
                                                 confidence * 100)
                    cv2.rectangle(frame, (startX, startY), (endX, endY),
                                  self.colors[idx], 2)
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    cv2.putText(frame, label, (startX, y),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[idx], 2)

        if display is True:
            end = time.time()
            print('Total time for frame:  {} s'.format(float(end-start)))

            cv2.imshow('MobileNetSSD human detection', frame)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                sys.exit(1)

            # do a bit of cleanup
            cv2.destroyAllWindows()

        return frame_bboxes

    def ssd_detection_video(self, video, ssd_confidence, display=False, output_video_name=None):
        '''
        The ssd detection algorithm by PyImgSearch a bit modified

        @param ssd_confidence: the confidence for the bboxes
        @param display: set to True to diplay the frames.
        @return bboxes
        '''

        # initialize the video stream, allow the cammera sensor to warmup,
        # and initialize the FPS counter
        # print("[INFO] starting video stream...")
        vcap = cv2.VideoCapture(video)

        writer = None
        frame_counter = 0
        bboxes = []
        # loop over the frames from the video stream
        while True:
            # grab the frame from the threaded video stream and resize it
            # to have a maximum width of 400 pixels
            ok, frame = vcap.read()
            if not ok:
                break
            start = time.time()
            frame_bboxes = []
            frame = imutils.resize(frame, width=400)

            # grab the frame dimensions and convert it to a blob
            (h, w) = frame.shape[:2]
            # These data are prepared from the MobileNetSSD Model.
            blob = cv2.dnn.blobFromImage(cv2.resize(
                frame, (300, 300)),	0.007843, (300, 300), 127.5)

            # pass the blob through the network and obtain the detections and
            # predictions
            self.net.setInput(blob)
            detections = self.net.forward()

            # loop over the detections
            for i in np.arange(0, detections.shape[2]):
                # extract the confidence (i.e., probability) associated with
                # the prediction
                confidence = detections[0, 0, i, 2]

                # filter out weak detections by ensuring the `confidence` is
                # greater than the minimum confidence
                if confidence > ssd_confidence:
                    # extract the index of the class label from the
                    # `detections`, then compute the (x, y)-coordinates of
                    # the bounding box for the object
                    idx = int(detections[0, 0, i, 1])
                    if self.labels[idx] != 'person':
                        continue

                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")

                    frame_bboxes.append([startX, startY, endX, endY])

                    if display is True or output_video_name is not None:
                        # draw the prediction on the frame
                        label = "{}: {:.2f}%".format(self.labels[idx],
                                                     confidence * 100)
                        cv2.rectangle(frame, (startX, startY), (endX, endY),
                                      self.colors[idx], 2)
                        y = startY - 15 if startY - 15 > 15 else startY + 15
                        cv2.putText(frame, label, (startX, y),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colors[idx], 2)

            bboxes.append(frame_bboxes)

            end = time.time()
            print('Total time for frame {} :  {} s'.format(
                frame_counter, float(end-start)))
            frame_counter += 1

            if display is True or output_video_name is not None:
                cv2.imshow('MobileNetSSD human detection', frame)
                if cv2.waitKey(1) & 0xFF == ord("q"):
                    break

            # check if the video writer is None
            if writer is None and output_video_name is not None:
                # initialize our video writer
                fourcc = cv2.VideoWriter_fourcc(*"MJPG")
                writer = cv2.VideoWriter(
                    output_video_name, fourcc, 30, (frame.shape[1], frame.shape[0]), True)

            if output_video_name is not None:
                # write the output frame to disk
                writer.write(frame)

        # do a bit of cleanup
        if writer is not None:
            writer.release()
        cv2.destroyAllWindows()
        vcap.release()

        return bboxes


if __name__ == "__main__":
    ssd = MobileNetSSD(SSD_PATH)
    ssd.ssd_detection_video(VIDEO, 0.4, True)


# # construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-p", "--prototxt", required=True,
# 	help="path to Caffe 'deploy' prototxt file")
# ap.add_argument("-m", "--model", required=True,
# 	help="path to Caffe pre-trained model")
# ap.add_argument("-c", "--confidence", type=float, default=0.2,
# 	help="minimum probability to filter weak detections")
# args = vars(ap.parse_args())

# # initialize the list of class labels MobileNet SSD was trained to
# # detect, then generate a set of bounding box colors for each class
# CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
# 	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
# 	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
# 	"sofa", "train", "tvmonitor"]
# COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# # load our serialized model from disk
# print("[INFO] loading model...")
# net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# # initialize the video stream, allow the cammera sensor to warmup,
# # and initialize the FPS counter
# print("[INFO] starting video stream...")
# vs = VideoStream(src=0).start()
# time.sleep(2.0)
# fps = FPS().start()

# # loop over the frames from the video stream
# while True:
# 	# grab the frame from the threaded video stream and resize it
# 	# to have a maximum width of 400 pixels
# 	frame = vs.read()
# 	frame = imutils.resize(frame, width=400)

# 	# grab the frame dimensions and convert it to a blob
# 	(h, w) = frame.shape[:2]
# 	blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)),
# 		0.007843, (300, 300), 127.5)

# 	# pass the blob through the network and obtain the detections and
# 	# predictions
# 	net.setInput(blob)
# 	detections = net.forward()

# 	# loop over the detections
# 	for i in np.arange(0, detections.shape[2]):
# 		# extract the confidence (i.e., probability) associated with
# 		# the prediction
# 		confidence = detections[0, 0, i, 2]

# 		# filter out weak detections by ensuring the `confidence` is
# 		# greater than the minimum confidence
# 		if confidence > args["confidence"]:
# 			# extract the index of the class label from the
# 			# `detections`, then compute the (x, y)-coordinates of
# 			# the bounding box for the object
# 			idx = int(detections[0, 0, i, 1])
# 			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
# 			(startX, startY, endX, endY) = box.astype("int")

# 			# draw the prediction on the frame
# 			label = "{}: {:.2f}%".format(CLASSES[idx],
# 				confidence * 100)
# 			cv2.rectangle(frame, (startX, startY), (endX, endY),
# 				COLORS[idx], 2)
# 			y = startY - 15 if startY - 15 > 15 else startY + 15
# 			cv2.putText(frame, label, (startX, y),
# 				cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

# 	# show the output frame
# 	cv2.imshow("Frame", frame)
# 	key = cv2.waitKey(1) & 0xFF

# 	# if the `q` key was pressed, break from the loop
# 	if key == ord("q"):
# 		break

# 	# update the FPS counter
# 	fps.update()

# # stop the timer and display FPS information
# fps.stop()
# print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
# print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# # do a bit of cleanup
# cv2.destroyAllWindows()
# vs.stop()
