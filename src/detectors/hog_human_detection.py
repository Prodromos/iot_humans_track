#!/usr/bin/env python

import cv2
import time
import fast_nms

from detectors.pub_detector import PubDetector

class HOGHumanDetection(PubDetector):
    '''
    This class is used for the detection of people using HOG Descriptor. 
    1)  In the __init__ function the initialisation of the dectriptor is made
        using the default descriptor provided by OpenCV.
    2)  In the HOG_classifier function the people are detected and the bboxes 
        in which they were detected are returned.
    '''
    def __init__(self):        
        super(HOGHumanDetection, self).__init__()

        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    def hog_detection_frame(self, frame, winstride=(8,8), padding=(16,16), scale=1.2, nms_threshold=0.35, frame_size=(384,288)):
        '''
        @param frame: the current frame
        @param winstride: the desired winstride tuple for the HOG descriptor
        @param padding: the desired padding tuple for the HOG descriptor
        @param scale: the desirable scale factor for the detector
        @param nms_threshold: the overlap threshold for the NMS Algorithm
        @param frame_size: the frame size

        @return bboxes: the bboxes that were found
        '''
        if frame_size != (384, 288):
            frame = cv2.resize(frame,frame_size) # Downscale to improve frame rate        
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY) # Haar-cascade classifier needs a grayscale image
        bboxes, weights = self.hog.detectMultiScale(gray_frame, winStride=winstride, padding=padding, scale=scale)
        # for i in enumerate(weights):  TODO
        #     if weights[i] < 0.55:
        #         bboxes.remove(bboxes[i])
        bboxes = fast_nms.non_max_suppression_fast(bboxes, nms_threshold)
        return bboxes

    def hog_detection_video(self, video_name, winstride=(8,8), padding=(16,16), scale=1.2, nms_threshold=0.35, frame_size=(384,288)):
        '''
        Returns the total bboxes that were detected from the whole video
        
        @param video_name: the name of the video examined
        @param winstride: the desired winstride tuple for the HOG descriptor
        @param padding: the desired padding tuple for the HOG descriptor
        @param scale: the desirable scale factor for the detector
        @param frame_size: the size of the frames. By default, according to the
                            CAVIAR dataset each frame is 384x288 pixels
        
        @return bboxes: the bboxes for the entire video
        '''
        bboxes = []
        
        vc = cv2.VideoCapture(video_name)
        while True:       
            fr_read, frame = vc.read()
            if not fr_read:
                break
            else:
                bboxes.append(self.HOG_detection_frame(frame, winstride, padding, scale, nms_threshold, frame_size))
        return bboxes




# if __name__ == "__main__":
    # print (cv2.__version__)
    # hog = cv2.HOGDescriptor()
    # hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    # cap = cv2.VideoCapture("Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/OneLeaveShopReenter1cor.mpg")

    # while True:
    #     r, frame = cap.read()

    #     if r:
    #         start_time = time.time()
    #         frame = cv2.resize(frame,(640,360)) # Downscale to improve frame rate
    #         gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY) # HOG needs a grayscale image

    #         rects, weights = hog.detectMultiScale(gray_frame, winStride=(8,8), padding=(8,8), scale=1.2 )

    #         picked_rects = fast_nms.non_max_suppression_fast(rects, 0.3)

    #         # Measure elapsed time for detections
    #         end_time = time.time()
    #         print("Elapsed time:", end_time-start_time)
    #         print(rects)
    # #         for i, (x, y, w, h) in enumerate(picked_rects):
    # # #            if weights[i] < 0.65:
    # # #                continue
    # #             cv2.rectangle(frame, (x,y), (x+w,y+h),(0,255,0),2)

    # #         cv2.imshow("preview", frame)
    #     k = cv2.waitKey(1)
    #     if k & 0xFF == ord("q"): # Exit condition
    #         break