#!/usr/bin/env python

import rospy
from iot_humans_track.msg import lint16

class PubDetector(object):
    '''
    This class is being inherited by the Detectors that are being used in 
    this application. It initialises a Publisher and publishes
    the data. For every detector initialised,
    a corresponding object of this class will be instantiated.
    '''
    def __init__(self):
        pass

    def pub_init(self, nd_name, topic, queue_size):
        self.node = rospy.Publisher (topic, lint16, queue_size=queue_size) 
        rospy.init_node (nd_name, anonymous=True)  

    def publish(self, data):
        self.node.publish(data)


if __name__ == "__main__":
    # print 'hello'
    # bboxes = int_array2d()
    # print bboxes.data
    # b = [[1,2,3,4],[2,3,4,5], [3,4,5,6,7,8,7,7]]
    # bboxes.data = b
    # print bboxes.data
    pass   