#!/usr/bin/env python
import cv2
import time 
from detectors.pub_detector import PubDetector

class CascadeHumanDetection(PubDetector):
    '''
    This class is used for the detection of people using the cascade classifiers (Haar, LBP, etc).
    The class has an instance variable (person_cascade) and two functions are declared.
    1) The __init__ function in order to initialise the Classifier. The Classifier's PATH 
       and the CLASSIFIER_FILE are required (e.g. 'opencv-3.4.4/data/haarcascades/', 'haarcascade_fullbody.xml')

       CAUTION: For the PATH use at the end the '/' character.      
    2) The function detects the bboxes of the objects in a single frame.    
    '''
    def __init__(self, cascade_path, cascade_name):
        super(CascadeHumanDetection, self).__init__()       
        self.person_cascade = cv2.CascadeClassifier(cascade_path + cascade_name)

    
    def cascade_detection_frame(self, frame, scale, frame_size=(384,288)):
        # if frame_size != (384, 288):
        #     frame = cv2.resize(frame,frame_size) # Downscale to improve frame rate        
        # else:
        #     h, w = frame.shape[0:2]
        #     if frame_size == (384, 288)  and  [w,h] != [384, 288]:
        #         frame = cv2.resize(frame, frame_size)
                
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY) # Haar-cascade classifier needs a grayscale image
        bboxes = self.person_cascade.detectMultiScale(gray_frame, scaleFactor=scale)
        return bboxes

    def cascade_detection_video(self, video_name, scale=1.1, frame_size=(384,288)):
        '''
        Returns the total bboxes that were detected from the whole video

        @param video_name: the name of the video examined
        @param scale: the desirable scale factor for the detector
        @param frame_size: the size of the frames. By default, according to the
                           CAVIAR dataset each frame is 384x288 pixels
        '''
        bboxes = []
        
        vc = cv2.VideoCapture(video_name)
        while True:       
            fr_read, frame = vc.read()
            if not fr_read:
                break
            else:
                bboxes.append(self.cascade_detection_frame(frame, scale, frame_size))
        return bboxes






#from datasets_eval import CaviarDataset as caviar 

# CAVIAR_XML_FILES = 'Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/TwoLeaveShop2cor.xml'
# VIDEOS_PATH = 'Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/'
# VIDEOS_ENCODING = '.mpg'
# XML_PATH = VIDEOS_PATH

# person_cascade = cv2.CascadeClassifier('opencv-3.4.4/data/haarcascades/haarcascade_fullbody.xml')
# cap = cv2.VideoCapture('Documents/Project_INSIGHT/Datasets/Videos/ShoppingMallPortugal/TwoLeaveShop2cor.mpg')

# frame_cntr = 0      # Number of the current frame
# v = caviar()
# bboxes_dataset = v.xml_parser_gt_bboxes(CAVIAR_XML_FILES)


# temp_var1 = v.eval_files_list(VIDEOS_PATH, VIDEOS_ENCODING, XML_PATH)

# while False:
#     r, frame = cap.read() 
#     if r:
#         start_time = time.time()
#         #frame = cv2.resize(frame,(640,360)) # Downscale to improve frame rate
#         gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY) # Haar-cascade classifier needs a grayscale image
#         rects = person_cascade.detectMultiScale(gray_frame, scaleFactor=1.1)
        

#         end_time = time.time()
#         print("Elapsed Time:",end_time-start_time)
#         for (x, y, w, h) in rects:
#             cv2.rectangle(frame, (x,y), (x+w,y+h),(255,0,0),2)
#         cv2.imshow("detection", frame)

#         if frame_cntr >= len(bboxes_dataset):
#             break
#         for (xc,yc,w,h) in bboxes_dataset[frame_cntr]:
#             if (xc, yc, w, h) == (0,0,0,0):
#                 continue
#             xtl = int(float(xc) - float(w)/2.0)
#             ytl = int(float(yc) - float(h)/2.0)
#             cv2.rectangle(frame, (xtl,ytl), (xtl + int(w), ytl + int(h)),(0,255,0),2)
#         cv2.imshow("detection", frame)
        
#         frame_cntr +=1

#     k = cv2.waitKey(1)
#     if k & 0xFF == ord("q"): # Exit condition
# 	break
