#!/usr/bin/env python
# import the necessary packages
import numpy as np

import time
import cv2
import os

from pub_detector import PubDetector
# from config import YOLO_PATH, YOLO_CFG, YOLO_WEIGHTS
#TODO Delete this.
VIDEO_NAME = 'Documents/Project_INSIGHT/data/Datasets/Videos/ShoppingMallPortugal/EnterExitCrossingPaths1front.mpg'
YOLO_PATH = "Documents/Project_INSIGHT/yolo-object-detection/yolo-coco/"
YOLO_WEIGHTS = "yolov3.weights"
YOLO_CFG = "yolov3.cfg"
YOLO_OUTPUT = "Documents/Project_INSIGHT/output/yolo/"
YOLO_ENCODING = ".avi"


class YOLODetectionV3(PubDetector):
    '''
    This class has been created for the implementation of the YOLO_v3
    detector. It consists of 2 functions, the init where some essential
    variables are being defined and the yolo_people_detection for the 
    calculation of the bounding boxes of people in each frame.
    '''

    def __init__(self, yolo_path, weights, cfg):
        '''
        @param yolo_path: the path to the yolo weights, coco-names and cfg files
        @param weights: the name of the file that contains the weights
        @param cfg: the name of the file for the yolo cfg

        @param output_video_name: of we want the operation to be saved in a file enter
        the path and the name of the video
        '''
        super(YOLODetectionV3, self).__init__()

        self.labelsPath = os.path.sep.join([yolo_path, "coco.names"])
        self.weightsPath = os.path.sep.join([yolo_path, weights])
        self.configPath = os.path.sep.join([yolo_path, cfg])
        self.net = cv2.dnn.readNetFromDarknet(self.configPath, self.weightsPath)

        # load the COCO class labels our YOLO model was trained on
        self.labels = open(self.labelsPath).read().strip().split("\n")

        # load our YOLO object detector trained on COCO dataset (80 classes)
        # and determine only the *output* layer names that we need from YOLO
        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i[0] - 1]
                   for i in self.net.getUnconnectedOutLayers()]

    def yolo_detection_frame(self, frame, yolo_confidence, yolo_threshold, blob_size=(416, 416), display=False):
        '''
        This function is the implementation of the OpenCV version of YOLO_v3
        detector. 

        @param yolo_confidence: the confidence with which we want to filter
        out weak predictions

        @param yolo_threshold: the threshold for the NMS algorithms

        @param blob_size: the input size of the blob into the CNN. By default is 
                          set to (416,416)

        @param diplsay: by default is set to False. If we want to view the
                        frames simultaneously with the calculation of the bboxes 
                        set to True

        @return bboxes: the bboxes of the video for all the frames
        '''

        # initialize the video stream, pointer to output video file, and
        (W, H) = (None, None)

        frame_bboxes = []

        # if the frame dimensions are empty, grab them
        if W is None or H is None:
            (H, W) = frame.shape[:2]

        # construct a blob from the input frame and then perform a forward
        # pass of the YOLO object detector, giving us our bounding boxes
        # and associated probabilities
        blob = cv2.dnn.blobFromImage(
            frame, 1 / 255.0, blob_size, swapRB=True, crop=False)
        self.net.setInput(blob)

        if display:
            start = time.time()
        layerOutputs = self.net.forward(self.ln)
        # end = time.time()

        # initialize our lists of detected bounding boxes, confidences,
        # and class IDs, respectively
        boxes = []
        confidences = []
        classIDs = []

        # loop over each of the layer outputs
        for output in layerOutputs:
            # loop over each of the detections
            for detection in output:
                # extract the class ID and confidence (i.e., probability)
                # of the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)

                # We want the Human Detection part only
                if self.labels[classID] != 'person':
                    continue
                confidence = scores[classID]

                # filter out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if confidence > yolo_confidence:
                    # scale the bounding box coordinates back relative to
                    # the size of the image, keeping in mind that YOLO
                    # actually returns the center (x, y)-coordinates of
                    # the bounding box followed by the boxes' width and
                    # height
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top
                    # and and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    # update our list of bounding box coordinates,
                    # confidences, and class IDs
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

            # apply non-maxima suppression to suppress weak, overlapping
            # bounding boxes
            idxs = cv2.dnn.NMSBoxes(
                boxes, confidences, yolo_confidence, yolo_threshold)

            # ensure at least one detection exists
            if len(idxs) > 0:
                # loop over the indexes we are keeping
                for i in idxs.flatten():
                    # extract the bounding box coordinates
                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])

                    frame_bboxes.append([x, y, w, h])

                    if display is True:
                        # draw a bounding box rectangle and label on the frame
                        color = (255, 0, 0)
                        cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                        text = "{}: {:.4f}".format(self.labels[classIDs[i]], confidences[i])
                        cv2.putText(frame, text, (x, y - 5),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

            if display is True:
                end = time.time()
                print('Total time for frame :  {} s'.format(float(end-start)))

                cv2.imshow('YOLO_v3 human detection', frame)
                if cv2.waitKey(1) & 0xFF == ord("q"):
                    break

                cv2.destroyAllWindows()

        return frame_bboxes

    def yolo_detection_video(self, video, yolo_confidence, yolo_threshold, blob_size=(416,416), display=False, output_video_name=None):
        '''
        This function is the implementation of the OpenCV version of YOLO_v3
        detector. 

        @param yolo_confidence: the confidence with which we want to filter
        out weak predictions

        @param yolo_threshold: the threshold for the NMS algorithms

        @param diplsay: by default is set to False. If we want to view the
        frames simultaneously with the calculation of the bboxes set to True

        @return bboxes: the bboxes of the video for all the frames
        '''

        # load the COCO class labels our YOLO model was trained on
        labels = open(self.labelsPath).read().strip().split("\n")

        # initialize the video stream, pointer to output video file, and
        # frame dimensions
        vs = cv2.VideoCapture(video)
        (W, H) = (None, None)
        writer = None
        # try to determine the total number of frames in the video file
        try:
            prop = cv2.CAP_PROP_FRAME_COUNT
            total = int(vs.get(prop))

        # an error occurred while trying to determine the total
        # number of frames in the video file
        except:
            print("[INFO] could not determine # of frames in video")
            print("[INFO] no approx. completion time can be provided")
            total = -1

        # loop over frames from the video file stream
        frame_counter = 0
        bboxes = []
        while True:
            # read the next frame from the file
            (ok, frame) = vs.read()
            frame_bboxes = []

            # if the frame was not grabbed, then we have reached the end
            # of the stream
            if not ok:
                break

            # if the frame dimensions are empty, grab them
            if W is None or H is None:
                (H, W) = frame.shape[:2]

            # construct a blob from the input frame and then perform a forward
            # pass of the YOLO object detector, giving us our bounding boxes
            # and associated probabilities
            blob = cv2.dnn.blobFromImage(
                frame, 1 / 255.0, blob_size, swapRB=True, crop=False)
            self.net.setInput(blob)
            start = time.time()
            layerOutputs = self.net.forward(self.ln)
            # end = time.time()

            # initialize our lists of detected bounding boxes, confidences,
            # and class IDs, respectively
            boxes = []
            confidences = []
            classIDs = []

            # loop over each of the layer outputs
            for output in layerOutputs:
                # loop over each of the detections
                for detection in output:
                    # extract the class ID and confidence (i.e., probability)
                    # of the current object detection
                    scores = detection[5:]
                    classID = np.argmax(scores)

                    # We want the Human Detection part only
                    if labels[classID] != 'person':
                        continue
                    confidence = scores[classID]

                    # filter out weak predictions by ensuring the detected
                    # probability is greater than the minimum probability
                    if confidence > yolo_confidence:
                        # scale the bounding box coordinates back relative to
                        # the size of the image, keeping in mind that YOLO
                        # actually returns the center (x, y)-coordinates of
                        # the bounding box followed by the boxes' width and
                        # height
                        box = detection[0:4] * np.array([W, H, W, H])
                        (centerX, centerY, width, height) = box.astype("int")

                        # use the center (x, y)-coordinates to derive the top
                        # and and left corner of the bounding box
                        x = int(centerX - (width / 2))
                        y = int(centerY - (height / 2))

                        # update our list of bounding box coordinates,
                        # confidences, and class IDs
                        boxes.append([x, y, int(width), int(height)])
                        confidences.append(float(confidence))
                        classIDs.append(classID)

            # apply non-maxima suppression to suppress weak, overlapping
            # bounding boxes
            idxs = cv2.dnn.NMSBoxes(
                boxes, confidences, yolo_confidence, yolo_threshold)

            # ensure at least one detection exists
            if len(idxs) > 0:
                # loop over the indexes we are keeping
                for i in idxs.flatten():
                    # extract the bounding box coordinates
                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])

                    frame_bboxes.append([x, y, w, h])

                    if display is True or output_video_name is not None:
                        # draw a bounding box rectangle and label on the frame
                        color = (255, 0, 0)
                        cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                        text = "{}: {:.4f}".format(
                            labels[classIDs[i]],	confidences[i])
                        cv2.putText(frame, text, (x, y - 5),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

            print frame_bboxes
            if display is True or output_video_name is not None:
                cv2.imshow('YOLO_v3 human detection', frame)
                if cv2.waitKey(1) & 0xFF == ord("q"):
                    break

            # check if the video writer is None
            if writer is None and output_video_name is not None:
                # initialize our video writer
                fourcc = cv2.VideoWriter_fourcc(*"MJPG")
                writer = cv2.VideoWriter(
                    output_video_name, fourcc, 30, (frame.shape[1], frame.shape[0]), True)

                # write the output frame to disk
            if output_video_name is not None:
                writer.write(frame)

            bboxes.append(frame_bboxes)

            end = time.time()
            print('Total time for frame {} :  {} s'.format(
                frame_counter, float(end-start)))
            frame_counter += 1

        if writer is not None:
            writer.release()
        vs.release()
        cv2.destroyAllWindows()

        return bboxes


if __name__ == "__main__":
    # pass
    yolo = YOLODetectionV3(YOLO_PATH, YOLO_WEIGHTS, YOLO_CFG)
    yolo.yolo_detection_video(VIDEO_NAME, 0.5, 0.3, blob_size=(288,288), display=True)
