#!/usr/bin/env python

import argparse

# Change the working directory accordingly
import os
import getpass

#CONSTANTS used in the Project
DEBUG = False

CHECK_FLAG_THRESHOLD = 7

CAVIAR_VIDEOS_PATH = 'Documents/Project_INSIGHT/data/Datasets/Videos/ShoppingMallPortugal/'
CAVIAR_VIDEOS_ENCODING = '.mpg'
XML_PATH = 'Documents/Project_INSIGHT/data/Datasets/Videos/ShoppingMallPortugal/'

SSD_PATH = "Documents/Project_INSIGHT/mobile_net_ssd/"
SSD_OUTPUT = "Documents/Project_INSIGHT/output/ssd/"
SSD_VIDEO_ENCODING = ".avi"

CASCADE_HAAR_CLASSIFIER_PATH = 'opencv-3.4.4/data/haarcascades/'
CASCADE_LBP_CLASSIFIER_PATH = 'opencv-3.4.4/data/lbpcascades/'

CASCADE_HAAR_FULL_BODY_FILE = 'haarcascade_fullbody.xml'
CASCADE_HAAR_UPPER_BODY_FILE = 'haarcascade_upperbody.xml'
CASCADE_HAAR_LOWER_BODY_FILE = 'haarcascade_lowerbody.xml'
CASCADE_LBP_FILE = 'haarcascade_fullbody.xml'

YOLO_PATH = "Documents/Project_INSIGHT/yolo-object-detection/yolo-coco/"
YOLO_WEIGHTS = "yolov3.weights"
YOLO_CFG = "yolov3.cfg"
YOLO_OUTPUT = "Documents/Project_INSIGHT/output/yolo/"
YOLO_ENCODING = ".avi"


CSV_DATA_PATH = 'Documents/Project_INSIGHT/data/evaluations/pf_eval/'
CSV_DATA_HAAR = 'eval_2_haar'
CSV_DATA_HOG = 'eval_2_hog'
CSV_DATA_SSD = 'eval_2_ssd'
CSV_DATA_YOLO = 'eval_2_yolo'
CSV_END = '.csv'



####################################################################################
######################  DETECTOR PARAMETERS   ######################################
#            scale
HAAR_PARAMS = 1.2

#             winstride   padding   scale   nms_thres
HOG_PARAMS = [  (8,8),      (8,8),   1.2,      0.4]

#              conf    display 
SSD_PARAMS = [  0.4,    False]

#               conf  thres    blob      display
YOLO_PARAMS = [ 0.5,  0.3,  (320 , 320),  False]
 
####################################################################################
####################################################################################



def assign_trackers_detectors():
    ap = argparse.ArgumentParser()
    ap.add_argument("-r", "--haar", type=str, required=False,
        help="name of the tracker assigned to haar detector")
    ap.add_argument("-g", "--hog", type=str, required=False,
        help="name of the tracker assigned to hog detector")
    ap.add_argument("-s", "--ssd", type=str, required=False,
        help="name of the tracker assigned to ssd detector")
    ap.add_argument("-y", "--yolo", type=str, required=False,
        help="name of the tracker assigned to yolo detector")
    ap.add_argument("-u", "--huang", type=str, required=False,
        help="name of the Particle Filter Huang")
    ap.add_argument("-d", "--deepgaze", type=str, required=False,
        help="name of the Particle Filter Deepgaze")
        
    args = vars(ap.parse_args())

    return args


#TODO Find a better place to place the function. Maybe a file named detector will do the job just fine
def detectors_parameters_insert(haar_params=None, hog_params=None, ssd_params=None, yolo_params=None):
    '''
    The implementation of this function is to create a list of dictionaries 
    that contain the necessary parameters for all the detectors

    @param haar_params: a list with the haar parameters
    @param hog_params: a list with the hog parameters
    @param ssd_params: a list with the ssd parameters 
    @param yolo_params: a list with the yolo parameters

    @return detectors_params: a list of dicts with the parameters of the selected detectors
    '''
    detectors_parameters = []
    if haar_params is not None:
        haar_dict = {'name': 'haar', 'scale': haar_params}
        detectors_parameters.append(haar_dict)
    
    if hog_params is not None:
        hog_dict = {'name': 'hog', 'winstride': hog_params[0], 'padding': hog_params[1],        \
                    'scale': hog_params[2], 'threshold': hog_params[3]}
        detectors_parameters.append(hog_dict)
    
    if ssd_params is not None:
        ssd_dict = {'name': 'ssd', 'confidence': ssd_params[0], 'display': ssd_params[1]}
        detectors_parameters.append(ssd_dict)
    
    if yolo_params is not None:
        yolo_dict = {'name': 'yolo', 'confidence': yolo_params[0], 'threshold': yolo_params[1], \
                    'blob_size': yolo_params[2], 'display': yolo_params[3]}
        detectors_parameters.append(yolo_dict)

    return detectors_parameters


if __name__ == "__main__":
    pass