#!/usr/bin/env python

import sys
import os
import cv2
from random import randint
import numpy as np
from sub_tracker import SubTracker
# Set video to load
VIDEO_PATH = 'Documents/Project_INSIGHT/data/Datasets/Videos/ShoppingMallPortugal/EnterExitCrossingPaths1cor.mpg'

trackerTypes = ['BOOSTING', 'MIL', 'KCF', 'TLD',
                'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']


class MultiTracker(SubTracker):

    def __init__(self):
        super(MultiTracker, self).__init__()
        self.multi_tracker = cv2.MultiTracker_create()
        self.tracker_name = None

    def __tracker_select(self, bboxes):
        ''' 
        Select a tracker based on the tracker's name

        @param tracker_type: a string with the name of the tracker we want to use
        @return tracker: an object of the tracker
        '''
        trackers = list()
        for i in range(len(bboxes)):
                
            if self.tracker_name == 'BOOSTING':
                trackers.append(cv2.TrackerBoosting_create())
            elif self.tracker_name == 'MIL':
                trackers.append(cv2.TrackerMIL_create())
            elif self.tracker_name == 'KCF':
                trackers.append(cv2.TrackerKCF_create())
            elif self.tracker_name == 'TLD':
                trackers.append(cv2.TrackerTLD_create())
            elif self.tracker_name == 'MEDIANFLOW':
                trackers.append(cv2.TrackerMedianFlow_create())
            elif self.tracker_name == 'GOTURN':
                trackers.append(cv2.TrackerGOTURN_create())
            elif self.tracker_name == 'MOSSE':
                trackers.append(cv2.TrackerMOSSE_create())
            elif self.tracker_name == 'CSRT':
                trackers.append(cv2.TrackerCSRT_create())
            else:
                ValueError('Incorrect tracker name')
                print('Available trackers are:')
                for t in trackerTypes:
                    print(t)
                return False
        if len(trackers) == 0:
            ValueError('No Bounding boxes were selected for tracking. Exiting...') 
        return trackers  


    def tracker_select(self, tracker_type):
        ''' 
        Select a tracker based on the tracker's name

        @param tracker_type: a string with the name of the tracker we want to use
        @return tracker: an object of the tracker
        '''

        if tracker_type == 'BOOSTING'   or  tracker_type == 'MIL'     or   \
           tracker_type == 'KCF'        or  tracker_type == 'TLD'     or   \
           tracker_type == 'MEDIANFLOW' or  tracker_type == 'GOTURN'  or   \
           tracker_type == 'MOSSE'      or  tracker_type == 'CSRT':
        
            self.tracker_name = tracker_type

        else:
            ValueError('Incorrect tracker name')
            print('Available trackers are:')
            for t in trackerTypes:
                print(t)
            return False
        return True

    def tracker_add(self, frame, bboxes):
        '''
        Use the bboxes provided at the frame and a tracker for all the
        bboxes

        @param frame: the img
        @param bboxes: the bboxes in the frame
        '''
        trackers = self.__tracker_select(bboxes)
        for i, bbox in enumerate(bboxes):
            self.multi_tracker.add(trackers[i], frame, tuple(bbox))
        return None

    def tracker_update(self, frame):
        '''
        Update the tracker for the current frame
        @param frame: the current frame
        @return bool, bboxes: True if success, the bboxes located in the frame
        '''
        ok, bboxes = self.multi_tracker.update(frame)
        # print 'Inside tracker_update {}'.format(bboxes)
        
        bboxes = np.asarray(bboxes)
        for i in range(len(bboxes)):
            bboxes[i] = [np.uint16(x) for _,x in enumerate(bboxes[i])]  

        return ok, bboxes






def createTrackerByName(tracker_type):
    if tracker_type == 'BOOSTING':
        tracker = cv2.TrackerBoosting_create()
    elif tracker_type == 'MIL':
        tracker = cv2.TrackerMIL_create()
    elif tracker_type == 'KCF':
        tracker = cv2.TrackerKCF_create()
    elif tracker_type == 'TLD':
        tracker = cv2.TrackerTLD_create()
    elif tracker_type == 'MEDIANFLOW':
        tracker = cv2.TrackerMedianFlow_create()
    elif tracker_type == 'GOTURN':
        tracker = cv2.TrackerGOTURN_create()
    elif tracker_type == 'MOSSE':
        tracker = cv2.TrackerMOSSE_create()
    elif tracker_type == 'CSRT':
        tracker = cv2.TrackerCSRT_create()
    else:
        ValueError('Incorrect tracker name')
        print('Available trackers are:')
        for t in trackerTypes:
            print(t)
        return False
    return tracker



def main():
    pass
    # Create a video capture object to read videos
    vcap = cv2.VideoCapture(VIDEO_PATH)
    # Read first frame
    success, frame = vcap.read()

    # quit if unable to read the video file
    if not success:
        print('Failed to read video')
        sys.exit(1)

    # Select boxes
    bboxes = []
    colors = []

    # OpenCV's selectROI function doesn't work for selecting multiple objects in Python
    # So we will call this function in a loop till we are done selecting all objects
    k=0
    while True:
        # draw bounding boxes over objects
        # selectROI's default behaviour is to draw box starting from the center
        # when fromCenter is set to false, you can draw box starting from top left corner
        k += 1
        bbox = cv2.selectROI(frame, False)
        bbox = list(bbox)
        # bbox[2] = bbox[2]+bbox[0]
        # bbox[3] = bbox[3]+bbox[1]
        bboxes.append(bbox)
        colors.append((randint(0, 255), randint(0, 255), randint(0, 255)))
        print("Press q to quit selecting boxes and start tracking")
        print("Press any other key to select next object")
        if (k == 2):  # q is pressed
            break
    print('Selected bounding boxes {}'.format(bboxes))

    # Specify the tracker type
    trackerType = trackerTypes[5]

    # Create MultiTracker object
    multiTracker = cv2.MultiTracker_create()
    print 'OK'
    # Initialize MultiTracker
    for bbox in bboxes:
        multiTracker.add(createTrackerByName(trackerType), frame, tuple(bbox))
    
    success, boxes = multiTracker.update(frame)
    print boxes
    # multiTracker.add(createTrackerByName(trackerType), frame, tuple(bboxes))

    # Process video and track objects
    while vcap.isOpened():
        success, frame = vcap.read()
        if not success:
            break

        # get updated location of objects in subsequent frames
        success, bboxes = multiTracker.update(frame)
        # draw tracked objects
        print bboxes
        for i, newbox in enumerate(bboxes):
            p1 = (np.uint16(newbox[0]), np.uint16(newbox[1]))
            p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
            # p2 = (np.uint16(newbox[2]), np.uint16(newbox[3]))
            cv2.rectangle(frame, p1, p2, colors[i], 2, 1)

        # show frame
        cv2.imshow('MultiTracker', frame)
        # print success, boxes

        # quit on ESC button
        if cv2.waitKey(1) & 0xFF == 27:  # Esc pressed
            vcap.release()
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    main()
