#!/usr/bin/env python
import argparse

from config import *
from multi_obj_track import MultiTracker
from tracker_manager import TrackerManager
from detectors.cascade_human_detection import CascadeHumanDetection
from detectors.hog_human_detection import HOGHumanDetection
from detectors.mobile_net_ssd import MobileNetSSD
from detectors.yolo_v3 import YOLODetectionV3

# # Particle Filters 
# from particle_filters.pf_deepgaze import DeepgazePF
# from particle_filters.pf_huang import HuangPF
import os
import getpass

PF_EVAL = [ 'Haar_CSRT/accept_error_40 pixels/', 
            'Haar_TLD/accept_error_40 pixels/',
            'SSD_CSRT/accept_error_40 pixels/',
            'SSD_TLD/accept_error_40 pixels/' ]

PF_PARAM_PATHS = [  'pf_N_100_w_0.4_multinomal/',
                    'pf_N_150_w_0.4_multinomal/',
                    'pf_N_100_w_0.4_residual/',
                    'pf_N_150_w_0.4_residual/' ]



def sc_system_init():
    '''
    This function plays an important role at startup. 
    1) It parses the arguments that were inserted with the keyboard. 
    2) It initialises the objects of the detectors that were chosen
    3) It initialises the objects of the multi_trackers that were chosen
    4) It initialises the particle filters list
    
    TODO Add more ways to utilize PARTICLE FILTERS. MORE COMBINATIONS WITH TRACKERS, etc.

    If a multi_tracker is set to 'None' from the input then the multi_tracker_obj is set to None 

    e.g. --haar None  --hog 'GOTURN  --yolo None
        Here the system will use 3 detectors and 1 tracker that corresponds to hog detector.
        The other objs are set to None and then they are appended to the track_list.

    @return track_list: a list of lists that contains:  * name of the detector
                                                        * detector_obj
                                                        * name of the tracker
                                                        * multi_tracker obj
    
    @return pf_list: a list of lists that contains:     * name of the detector
                                                        * detector_obj
                                                        * name of the particle filter
                                                        * multi_tracker obj

    '''
    
    track_list = list()
    pf_list = list()
    args = assign_trackers_detectors()
    for name, value in args.iteritems():
        if name == 'haar'  and  value != None:
            haar_obj = CascadeHumanDetection(CASCADE_HAAR_CLASSIFIER_PATH, CASCADE_HAAR_FULL_BODY_FILE)
            multi_haar = None
            if value != 'None': 
                multi_haar = MultiTracker()
                multi_haar.tracker_select(value)
                tracker_name = value
                print 'New Pair Selected:  ' + name.upper()  + '  &  ' + str(value)
            else:
                print 'New Detector Selected:  ' + name.upper()
                tracker_name = None 
            haar_list = [name, haar_obj, tracker_name, multi_haar]
            track_list.append(haar_list)

        elif name == 'hog'  and  value != None:
            hog_obj = HOGHumanDetection()
            multi_hog = None
            if value != 'None':
                multi_hog = MultiTracker()
                multi_hog.tracker_select(value)
                tracker_name = value
                print 'New Pair Selected:  ' + name.upper()  + '  &  ' + str(value)
            else:
                print 'New Detector Selected:  ' + name.upper() 
                tracker_name = None
            hog_list = [name, hog_obj, tracker_name, multi_hog]
            track_list.append(hog_list)

        elif name == 'ssd'  and  value != None:
            ssd_obj = MobileNetSSD(SSD_PATH)
            multi_ssd = None
            if value != 'None':
                multi_ssd = MultiTracker()
                multi_ssd.tracker_select(value)
                tracker_name = value
                print 'New Pair Selected:  ' + name.upper()  + '  &  ' + str(value)
            else:
                print 'New Detector Selected:  ' + name.upper() 
                tracker_name = None
            ssd_list = [name, ssd_obj, tracker_name, multi_ssd]
            track_list.append(ssd_list)

        elif name == 'yolo'  and  value != None:
            yolo_obj = YOLODetectionV3(YOLO_PATH, YOLO_WEIGHTS, YOLO_CFG)
            multi_yolo = None
            if value != 'None':
                multi_yolo = MultiTracker()
                multi_yolo.tracker_select(value)
                print 'New Pair Selected:  ' + name.upper()  + '  &  ' + str(value)
                tracker_name = value
            else:
                print 'New Detector Selected:  ' + name.upper()
                tracker_name = None 
            yolo_list = [name, yolo_obj, tracker_name, multi_yolo]
            track_list.append(yolo_list)

        elif (name == 'huang'  or  name == 'deepgaze')  and  value != None:
            if value == 'haar':
                value_obj = CascadeHumanDetection(CASCADE_HAAR_CLASSIFIER_PATH, CASCADE_HAAR_FULL_BODY_FILE)
                pf_list.append([value, value_obj, name, None]) 
            elif value == 'hog':
                value_obj = hog_obj = HOGHumanDetection()
                pf_list.append([value, value_obj, name, None])
            elif value == 'ssd':
                value_obj = MobileNetSSD(SSD_PATH)
                pf_list.append([value, value_obj, name, None])
            elif value == 'yolo':
                value_obj = YOLODetectionV3(YOLO_PATH, YOLO_WEIGHTS, YOLO_CFG)
                pf_list.append([value, value_obj, name, None])
        
    # To avoid compatiblity problems later, always need list inside a list    
    if len (track_list) == 0:
        ValueError('None Detector or Tracker selected. Exiting...')
    if len(track_list) < 2:
        track_list = list(track_list)
    
    print track_list  
    print pf_list
    return track_list, pf_list


def main_sc ():
    USERNAME = getpass.getuser()
    # os.chdir ('/home/' + USERNAME + '/' + CSV_DATA_PATH + pf_path + pf_eval_path) 
    print os.getcwd() 
    # path = '/home/' + USERNAME + '/' + CSV_DATA_PATH + pf_path + pf_eval_path
    os.chdir('/home/' + USERNAME)
    # pf_N = [100, 150]
    # pf_resampling = ['multinomal', 'residual']

    # i=0
    # for N in pf_N:
    #     for pf_res in pf_resampling:
    #         main_func(N, pf_res, PF_PARAM_PATHS[i])
    #         i += 1
    pf_path = ''
    haar_params = None
    hog_params = None
    ssd_params = None
    yolo_params = None

    track_list, pf_list = sc_system_init()

    for det in track_list:
        
        if det[0] == 'haar':
            haar_params = 1.2

        elif det[0] == 'hog':       
            #             winstride   padding   scale   nms_thres
            hog_params = [  (8,8),      (8,8),   1.2,      0.4]
        
        elif det[0] == 'ssd':
            #              conf    display 
           ssd_params = [  0.4,    False]
        
        elif det[0] == 'yolo':
            #               conf  thres    blob      display
            yolo_params = [ 0.5,  0.3,  (320 , 320),  False]


    for det in pf_list:
        
        if det[0] == 'haar':
            haar_params = 1.2
        
        elif det[0] == 'hog':       
            #             winstride   padding   scale   nms_thres
            hog_params = [  (8,8),      (8,8),   1.2,      0.4]
        
        elif det[0] == 'ssd':
            #              conf    display 
           ssd_params = [  0.4,    False]
        
        elif det[0] == 'yolo':
            #               conf  thres    blob      display
            yolo_params = [ 0.5,  0.3,  (320 , 320),  False]


    for dn, _, tn, _ in track_list:
        if dn == 'haar':
            if tn == 'CSRT':
                pf_path = PF_EVAL[0]
            elif tn == 'TLD':
                pf_path = PF_EVAL[1]
        if dn == 'ssd':
            if tn == 'CSRT':
                pf_path = PF_EVAL[2]
            elif tn == 'TLD':
                pf_path = PF_EVAL[3]


    detectors_params = detectors_parameters_insert(haar_params, hog_params, ssd_params, yolo_params)

    #                        num_particles
    # huang_pf_params = {'huang': 50}
    # #                        particles, std, noise_prob, resample_method, noise, mask, velocity
    # deepgaze_pf_params =  {'deepgaze': [400, 8, 0.15, 'multinomal', (-300,300), (5,2), [0,0]]}
    # huang_pf_params.update(deepgaze_pf_params)

    #          move_conf, acc_error, resample_method,   N ,   w_thres
    pf_params = [0.9,        2,     'residual',    100,    0.3]
    tracker_mngr = TrackerManager(track_list, detectors_params, pf_params, 20)
    tracker_mngr.max_dist_radius = 40
    tracker_mngr.track_mode_select('sc_pf')
    # tracker_mngr.pf_list.append('My Particle Filter')
    tracker_mngr.set_colors()
    
    # args: frame_size , iou_threshold, skip_frames, display
    tracker_mngr.track_caviar((384,288), 0.20, 13, 3, True, display=True, output=True)



if __name__ == "__main__":
    main_sc()
