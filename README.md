# Project INSIGHT: "Detection and tracking of human presence in indoor environments using multiple RGB cameras and Particle Filters"

## In order to install and run the Thesis Project there are some requirements:
- OpenCV 3.4.4
- ROS Kinetic
- Copy weights for the DNN models in /home or change the location from the source code



## For the 2D project
- Initialize a roscore 
- exec the command with the a detector and a tracker, e.g. SSD detector and the CSRT tracker: 

	`rosrun iot_humans_track main.py --ssd CSRT`


More info you can find at the config.py file.

## For the 3D Project and the VIPT_2014 Dataset
- Initialie a roscore 
- exec: `rosrun rviz rviz for visualization`

For using the right number of cameras: 
- Change the CAMS_2_USE in the mc_config.py to a number 1-4
- From the launch file mc_system.launch use the .py files depending on the number of cameras you will use, comment otherwise.
- Exec `rosrun iot_humans_track mc_system.launch`




